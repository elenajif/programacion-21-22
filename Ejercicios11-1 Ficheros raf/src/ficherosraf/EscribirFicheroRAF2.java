package ficherosraf;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class EscribirFicheroRAF2 {

	public static void main(String[] args) {
		try {
			// 1.- abrir el fichero en acceso RAF
			RandomAccessFile f = new RandomAccessFile("datos", "rw");
			// 2.- nos posicionamos al final del fichero
			f.seek(f.length());
			// 3.- escribimos una cadena de texto
			f.writeBytes("Esto es una cadena de texto");
			// 4.- cerramos el fichero
			f.close();
			System.out.println("Fichero actualizado correctamente");
		} catch (FileNotFoundException e) {
			System.out.println("No existe el archivo");
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
		}
	}

}
