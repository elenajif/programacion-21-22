package ficherosraf;

import java.io.EOFException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class LeerRAFByteAByte {

	public static void main(String[] args) {
		try {
			//1.- abrir el acceso al archivo
			RandomAccessFile f = new RandomAccessFile("datos1.txt","rw");
			//2.- recorrer el archivo caracter a caracter
			char letra;
			boolean finFichero=false;
			
			do {
				try {
					letra =(char) f.readByte();
					if (letra =='b') {
						//1.- desplazar el puntero uno hacia atr�s
						f.seek(f.getFilePointer()-1);
						//2.- escribo la B
						f.writeByte('B');
					}
				} catch (EOFException e) {
					// 3.- fin fichero
					System.out.println("Fin fichero");
					finFichero=true;
					f.close();
				}		
			} while (finFichero==false);
			
		}catch (IOException e) {
			System.out.println("Error de entrada de datos");
		}

	}

}
