package ficherosraf;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;

public class LeerYEscribirEnRAF {

	public static void main(String[] args) {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			// abrir el acceso al fichero
			RandomAccessFile f = new RandomAccessFile("datos2.txt", "rw");
			f.seek(f.length());
			String respuesta = "";
			do {
				System.out.print("Nombre= ");
				String nombre = in.readLine();
				// grabar el nombre en el archivo
				f.writeUTF(nombre);
				System.out.println("�Deseas continuar (si/no)? ");
				respuesta = in.readLine();
			} while (respuesta.equalsIgnoreCase("si"));

			// cerrar el archivo
			f.close();

			// leer el archivo para mostrar los datos
			RandomAccessFile f2 = new RandomAccessFile("datos2.txt", "rw");
			String nombre = "";
			boolean finFichero = false;
			do {
				try {
					nombre = f2.readUTF();
					System.out.println(nombre);
				} catch (EOFException e) {
					System.out.println("fin fichero");
					finFichero = true;
					f2.close();
				}
			} while (!finFichero);

		} catch (IOException e) {
			System.out.println("Error de entrada salida");
		}

	}

}
