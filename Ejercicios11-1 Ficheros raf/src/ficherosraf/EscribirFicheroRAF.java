package ficherosraf;

import java.io.IOException;
import java.io.RandomAccessFile;

public class EscribirFicheroRAF {
	// RAF
	// Random Access File
	// no guarda texto plano, guarda registros
	// permite acceder a una posicion determinada
	// Me puedo desplazar por los registros
	// puedo leer y escribir a la vez
	// modo
	// r -> read
	// w -> write
	// metodo escribir (writeInt, writeLong, writeDouble, writeBytes)
	// getFilePointer devuelve la posicion actual donde se va a realizar la
	// operacion
	// seek colocar el fichero en una posicion determinada
	// length tama�o archivo

	public static void main(String[] args) {
		try {
			// 1.- abrir el fichero en acceso RAF
			RandomAccessFile f = new RandomAccessFile("datos.txt", "rw");
			// 2.- nos posicionamos al final del fichero
			f.seek(f.length());
			// 3.- escribimos una cadena de texto
			f.writeBytes("Esto es un texto");
			// 4.- cerramos el fichero
			f.close();
			System.out.println("Fichero actualizado correctamente");
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
		}
	}

}
