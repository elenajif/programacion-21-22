package ejercicio10;

import java.util.Scanner;

public class Ejercicio10 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		char[] caracteres = new char[10];

		// relleno vector caracteres
		for (int i = 0; i < caracteres.length; i++) {
			System.out.println("Dame un caracter");
			caracteres[i] = input.nextLine().charAt(0);
		}
		// muestro contenido del array
		for (int i = 0; i < caracteres.length; i++) {
			System.out.print(caracteres[i] + " ");
		}
		// ordenar
		ordenar(caracteres);
		System.out.println();
		// mostrar ordenado
		for (int i = 0; i < caracteres.length; i++) {
			System.out.print(caracteres[i] + " ");
		}
		input.close();
	}

	private static void ordenar(char[] array) {
		// cada vez que me situo en una celda
		// recorro con el segundo bucle las celdas siguientes
		// en busca del valor mas peque�o, que ire colocando en la celda actual
		// asi sucesivamente a lo largo de todas las celdas
		for (int i = 0; i < array.length - 1; i++) {
			for (int j = i + 1; j < array.length; j++) {
				if (array[j] < array[i]) {
					char aux = array[i];
					array[i] = array[j];
					array[j] = aux;
				}
			}
		}
	}

}
