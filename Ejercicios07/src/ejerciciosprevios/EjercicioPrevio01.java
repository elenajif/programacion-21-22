package ejerciciosprevios;

public class EjercicioPrevio01 {

	public static void main(String[] args) {
		// creo un vector de int de tama�o 5 llamado faltas
		// tipodatos[] nombre = new tipodatos[tama�o]
		int[] faltas = new int[5];
		// creo un vector de doubles de tama�o 3 llamado notas
		double[] notas = new double[3];
		// declaro un vector de String llamado nombres
		String[] nombres;
		// lo inicializo con tama�o 4
		nombres = new String[4];
		//creo un vector de String llamado paises con 4 valores
		String[] paises = {"Espa�a","Fracia","B�lgica","Italia"};
		//el indice del vector (posicion) comienza en 0
		//mostrar una componente del vector paises
		System.out.println("La posicion 0 contiene "+paises[0]);
		System.out.println("La posicion 1 contiene "+paises[1]);
		System.out.println("La posicion 2 contiene "+paises[2]);
		System.out.println("La posicion 3 contiene "+paises[3]);
		
	}

}
