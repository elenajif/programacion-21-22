package ejerciciosprevios;

public class EjercicioPrevio05 {
	
	public static void main(String[] args) {
		int[] miVector = { 12, 34, 23, 11, 23, 45, 67, 43, 55, 41 };
		int suma = 0;
		for (int i = 0; i < miVector.length; i++) {
			suma += miVector[i];
			// suma=suma+miVector[i]
		}
		System.out.println("La suma del vector es " + suma);
	}

}
