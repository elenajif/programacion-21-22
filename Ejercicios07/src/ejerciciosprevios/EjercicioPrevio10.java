package ejerciciosprevios;

public class EjercicioPrevio10 {

	public static void main(String[] args) {
		int[] miVector = { 12, 34, 23, 11, 23, 45, 67, 43, 55, 41 };
		System.out.println("Muestro miVector");
		for (int i = 0; i < miVector.length; i++) {
			System.out.println(miVector[i]);
		}

		int[] miVector1;
		// igualar vectores no lleva []
		miVector1 = miVector;
		System.out.println("Muestro miVector1");
		for (int i = 0; i < miVector1.length; i++) {
			System.out.println(miVector1[i]);
		}
	}

}
