package ejerciciosprevios;

import java.util.Scanner;

public class EjercicioPrevio03 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		// creamos un vector de String llamado notas de tama�o 4
		double[] notas = new double[4];

		// pedimos datos por teclado
		for (int i = 0; i < notas.length; i++) {
			System.out.println("Dame un numero double");
			notas[i] = input.nextDouble();
		}

		// mostramos los datos
		for (int i = 0; i < notas.length; i++) {
			System.out.println(notas[i]);
		}

		input.close();
	}

}
