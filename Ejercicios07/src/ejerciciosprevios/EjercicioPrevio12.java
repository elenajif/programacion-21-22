package ejerciciosprevios;

import java.util.Scanner;

public class EjercicioPrevio12 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int[] miVector = new int[5];
		for (int i = 0; i < miVector.length; i++) {
			System.out.println("Dame la componente " + i + " del vector");
			miVector[i] = input.nextInt();
		}

		// en los metodos en parametros y valores de devolucion los vectores llevan []
		// en los metodos en el return no lleva []
		// en el main en las llamadas no se ponen []

		// metodo suma recibe un vector y calcula su suma
		int suma = sumaVector(miVector);
		System.out.println("La suma del vector es " + suma);
		// metodo promedio recibe un vector y calcula su promedio
		double promedio = promedioVector(miVector);
		System.out.println("La media del vector es " + promedio);
		// metodo maximo recibe un vector y calcula su maximo
		int max = maximoVector(miVector);
		System.out.println("El maximo del vector es " + max);
		// metodo minimo recibe un vector y calcula su minimo
		int min = minimoVector(miVector);
		System.out.println("El minimo del vector es " + min);

		input.close();
	}

	public static int sumaVector(int[] vector) {
		int suma = 0;
		for (int i = 0; i < vector.length; i++) {
			suma += vector[i];
		}
		return suma;
	}

	public static int maximoVector(int[] vector) {
		int max = vector[0];
		for (int i = 0; i < vector.length; i++) {
			if (vector[i] > max) {
				max = vector[i];
			}
		}
		return max;
	}

	public static int minimoVector(int[] vector) {
		int min = vector[0];
		for (int i = 0; i < vector.length; i++) {
			if (vector[i] < min) {
				min = vector[i];
			}
		}
		return min;
	}

	public static double promedioVector(int[] vector) {
		int suma = 0;
		for (int i = 0; i < vector.length; i++) {
			suma += vector[i];
			// suma=suma+miVector[i]
		}
		double promedio = (double) suma / (double) vector.length;
		return promedio;
	}
}
