package ejerciciosprevios;

import java.util.Scanner;

public class EjercicioPrevio07 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int[] miVector = new int[10];
		for (int i = 0; i < miVector.length; i++) {
			System.out.println("Dame la componente " + i + " del vector");
			miVector[i] = input.nextInt();
		}
		int max = miVector[0];
		for (int i = 0; i < miVector.length; i++) {
			if (miVector[i] > max) {
				max = miVector[i];
			}
		}
		System.out.println("El maximo del vector es " + max);
		input.close();
	}
}
