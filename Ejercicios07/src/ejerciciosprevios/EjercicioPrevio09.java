package ejerciciosprevios;

import java.util.Scanner;

public class EjercicioPrevio09 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int[] miVector = new int[10];
		for (int i = 0; i < miVector.length; i++) {
			System.out.println("Dame la componente " + i + " del vector");
			miVector[i] = input.nextInt();
		}
		int suma = 0;
		for (int i = 0; i < miVector.length; i++) {
			suma += miVector[i];
			// suma=suma+miVector[i]
		}
		System.out.println("La suma del vector es " + suma);
		double promedio = (double) suma / (double) miVector.length;
		System.out.println("La media del vector es " + promedio);
		input.close();
	}
}
