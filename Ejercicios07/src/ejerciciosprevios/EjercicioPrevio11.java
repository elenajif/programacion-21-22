package ejerciciosprevios;

import java.util.Scanner;

public class EjercicioPrevio11 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int[] miVector = new int[5];
		for (int i = 0; i < miVector.length; i++) {
			System.out.println("Dame la componente " + i + " del vector");
			miVector[i] = input.nextInt();
		}

		// en los metodos en parametros y valores de devolucion los vectores llevan []
		// en los metodos en el return no lleva []
		// en el main en las llamadas no se ponen []
		
		// metodo suma recibe un vector y calcula su suma
		sumaVector(miVector);
		// metodo promedio recibe un vector y calcula su promedio
		promedioVector(miVector);
		// metodo maximo recibe un vector y calcula su maximo
		maximoVector(miVector);
		// metodo minimo recibe un vector y calcula su minimo
		minimoVector(miVector);

		input.close();
	}

	public static void sumaVector(int[] vector) {
		int suma = 0;
		for (int i = 0; i < vector.length; i++) {
			suma += vector[i];
		}
		System.out.println("La suma del vector es " + suma);
	}
	
	public static void maximoVector(int[] vector) {
		int max = vector[0];
		for (int i = 0; i < vector.length; i++) {
			if (vector[i] > max) {
				max = vector[i];
			}
		}
		System.out.println("El maximo del vector es " + max);
	}
	
	public static void minimoVector(int[] vector) {
		int min = vector[0];
		for (int i = 0; i < vector.length; i++) {
			if (vector[i] < min) {
				min = vector[i];
			}
		}
		System.out.println("El minimo del vector es " + min);
	}
	
	public static void promedioVector(int[] vector) {
		int suma = 0;
		for (int i = 0; i < vector.length; i++) {
			suma += vector[i];
			// suma=suma+miVector[i]
		}
		double promedio = (double) suma / (double) vector.length;
		System.out.println("La media del vector es " + promedio);
	}

}
