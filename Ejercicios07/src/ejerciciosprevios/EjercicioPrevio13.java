package ejerciciosprevios;

import java.util.Scanner;

public class EjercicioPrevio13 {

	public static void main(String[] args) {
		// llamamos a leerDatos
		double[] miVector = leerDatos();
		// llamamos a sumarDatos
		sumarDatos(miVector);
	}

	// metodo para leer un vector de doubles leerDatos
	static double[] leerDatos() {
		// para indicar que va a devolver un vector de double si se ponen []
		Scanner input = new Scanner(System.in);
		System.out.println("Dame el numero de datos que quieres introducir");
		int numero = input.nextInt();
		double[] vectorDoubles = new double[numero];
		for (int i = 0; i < vectorDoubles.length; i++) {
			System.out.println("Dame la componente " + i + " del vector: ");
			vectorDoubles[i] = input.nextDouble();
		}
		input.close();
		return vectorDoubles;
		// cuando devuelvo un vector no lleva []
	}

	// metodo para calcular la suma de los datos sumarDatos que reciba el vector
	static void sumarDatos(double[] miVector) {
		// en los parametros si recibo un vector si lleva []
		double suma = 0;
		for (int i = 0; i < miVector.length; i++) {
			suma += miVector[i];
		}
		System.out.println("La suma es " + suma);
	}

}
