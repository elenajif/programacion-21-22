package ejerciciosprevios;

import java.util.Scanner;

public class EjercicioPrevio04 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		// creamos un vector de String llamado colores de tama�o 4
		String[] colores = new String[4];

		// pedimos datos por teclado
		for (int i = 0; i < colores.length; i++) {
			System.out.println("Dame un color");
			colores[i] = input.nextLine();
		}

		// mostramos los datos
		for (int i = 0; i < colores.length; i++) {
			System.out.println(colores[i]);
		}

		input.close();
	}

}
