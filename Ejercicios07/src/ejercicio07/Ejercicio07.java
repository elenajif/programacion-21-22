package ejercicio07;

import java.util.Scanner;

public class Ejercicio07 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		// pido el tama�o del array
		System.out.println("Dame el tama�o del array");
		int tamano = input.nextInt();
		input.nextLine();
		// creo el array
		String[] cadenas = new String[tamano];
		// pido las cadenas
		for (int i = 0; i < cadenas.length; i++) {
			System.out.println("Introduce una cadena");
			cadenas[i] = input.nextLine();
		}
		// indico desplazamiento
		desplazar(cadenas, 2);

		for (int i = 0; i < cadenas.length; i++) {
			System.out.println(cadenas[i]);
		}

		input.close();
	}

	private static void desplazar(String[] cadenas, int desplazamiento) {
		desplazamiento = desplazamiento % cadenas.length;
		// el siguiente bucle se repite tantas veces como el desplazamiento
		for (int i = 0; i < desplazamiento; i++) {
			String aux;
			aux = cadenas[cadenas.length - 1];
			// empiezo desde el final del array
			// moviendo el penultimo elemento a la casilla del 1
			for (int j = cadenas.length - 1; j > 0; j--) {
				cadenas[j] = cadenas[j - 1];
			}
			cadenas[0] = aux;
		}
	}

}
