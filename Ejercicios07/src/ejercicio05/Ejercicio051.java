package ejercicio05;

import java.util.Scanner;

public class Ejercicio051 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int[] numeros = new int[10];

		for (int i = 0; i < numeros.length; i++) {
			System.out.println("Introduce un numero");
			numeros[i] = input.nextInt();
		}

		mostrarEstadisticas(numeros);

		input.close();
	}

	static void mostrarEstadisticas(int[] array) {
		int bigger = 0;
		int smaller = 999999999;
		int countN = 0;
		int sumaN = 0;
		int countP = 0;
		int sumaP = 0;
		int countT = 0;
		int sumaT = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i] > bigger) {
				bigger = array[i];
			}
			if (array[i] < smaller) {
				smaller = array[i];
			}
			if (array[i] > 0) {
				sumaP += array[i];
				countP++;
			} else if (array[i] < 0) {
				array[i] = -array[i];
				sumaN += array[i];
				countN++;
			}
			sumaT += array[i];
			countT++;
		}

		System.out.println("El numero mas grande es " + bigger);
		System.out.println("El numero mas peque�o es " + smaller);
		if (countP != 0) {
			System.out.println("La media de positivos es " + ((double) sumaP / (double) countP));
		} else {
			System.out.println("No hay numeros positivos.");
		}
		if (countN != 0) {
			System.out.println("La media de negativos es " + (-((double) sumaN / (double) countN)));
		} else {
			System.out.println("No hay numeros negativos.");
		}
		System.out.println("La media total es " + (double) (sumaT / countT));

	}
}
