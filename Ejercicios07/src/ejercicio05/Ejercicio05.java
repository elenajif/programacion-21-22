package ejercicio05;

import java.util.Scanner;

public class Ejercicio05 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int[] numeros = new int[10];

		for (int i = 0; i < numeros.length; i++) {
			System.out.println("Introduce un numero");
			numeros[i] = input.nextInt();
		}

		mostrarEstadisticas(numeros);

		input.close();
	}

	private static void mostrarEstadisticas(int[] numeros) {
		int mayor = 0;
		int menor = 0;
		int contadorPositivos = 0;
		int sumaPositivos = 0;
		int contadorNegativos = 0;
		int sumaNegativos = 0;

		for (int i = 0; i < numeros.length; i++) {
			// comprueba cual es el mayor y el menor
			if (i == 0) {
				mayor = numeros[i];
				menor = numeros[i];
			} else if (numeros[i] > mayor) {
				mayor = numeros[i];
			} else if (numeros[i] < menor) {
				menor = numeros[i];
			}
			// cuento los numeros y sumo los valores
			if (numeros[i] >= 0) {
				contadorPositivos++;
				sumaPositivos += numeros[i];
			} else {
				contadorNegativos++;
				sumaNegativos += numeros[i];
			}
		}
		// muestro los resultados
		System.out.println("El mayor es " + mayor);
		System.out.println("El menor es " + menor);
		System.out.println("Media de positivos " + ((double) sumaPositivos / contadorPositivos));
		System.out.println("Media de negativos " + ((double) sumaNegativos / contadorNegativos));
		System.out.println("Media total " + ((double) (sumaPositivos + sumaNegativos) / numeros.length));
	}

}
