package ejercicio12;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		// pido el lado
		System.out.println("Introduce el lado del array");
		int lado = input.nextInt();
		// creo matriz con lado lado (2x2 3x3...)
		int[][] matriz = new int[lado][lado];

		// metodo que rellena y muestra
		// le paso matriz y scanner
		rellenarMatriz(matriz, input);
		System.out.println("Mostrar datos matriz modificada");
		matriz = intercambiarFilasColumnas(matriz);
		// mostrar la matriz
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				System.out.print(matriz[i][j] + " ");
			}
			System.out.println();
		}
		input.close();
	}

	// metodo void que rellenar e imprime la matriz
	// recibe una matriz int[][] matriz
	// recibe un scanner Scanner input
	private static void rellenarMatriz(int[][] matriz, Scanner input) {
		// pido los datos
		System.out.println("Pedimos datos matriz");
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				System.out.println("Introduce un numero");
				matriz[i][j] = input.nextInt();
			}
		}
		// los muestro
		System.out.println("Mostramos datos matriz");
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				System.out.print(matriz[i][j] + " ");
			}
			System.out.println();
		}
	}

	// metodo que devuelve una matriz despues de intercambiar
	// recibe una matriz int[][] matriz
	private static int[][] intercambiarFilasColumnas(int[][] matriz) {
		int[][] matrizModificada = new int[matriz.length][matriz[0].length];
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				matrizModificada[j][i] = matriz[i][j];
			}
		}
		return matrizModificada;
	}

}
