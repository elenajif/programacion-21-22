package ejercicio04;

import java.util.Scanner;

import ejercicio03.Ejercicio03;

public class Ejercicio04 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		// Pido el tama�o
		System.out.println("Introduce el tama�o del array");
		int tamano = input.nextInt();
		// creo el vector del tama�o que hemos introducido
		char[] letras = new char[tamano];
		// rellenamos (Math.random()*(fin-inicio+1)) + inicio
		// inicio 'a'
		// fin 'z'
		for (int i = 0; i < letras.length; i++) {
			letras[i] = (char) ((Math.random() * ('z' - 'a' + 1)) + 'a');
			System.out.print(letras[i] + " ");
		}

		System.out.println();
		//el metodo sustituyeVocales esta en Ejercicio03 y es public
		letras = Ejercicio03.sustituyeVocales(letras);
		for (int i = 0; i < letras.length; i++) {
			System.out.print(letras[i] + " ");
		}

		input.close();

	}

}
