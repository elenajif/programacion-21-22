package ejercicio15;

import java.util.Scanner;

public class Ejercicio15 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		// pedimos numero de filas y columnas
		System.out.println("Dame el numero de filas");
		int filas = input.nextInt();
		System.out.println("Dame el numero de columnas");
		int columnas = input.nextInt();
		// creamos la matriz
		int[][] matriz = new int[filas][columnas];
		// rellenamos matriz con numeros aleatorios
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				matriz[i][j] = (int) (Math.random() * 100) - 50;
			}
		}
		// mostramos la matriz
		System.out.println("Mostramos matriz");
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				System.out.print(matriz[i][j] + " ");
			}
			System.out.println();
		}
		// llamamos al metodo ordenar(matriz)
		ordenar(matriz);
		// mostramos la matriz ordenada
		System.out.println("Mostramos matriz ordenada");
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				System.out.print(matriz[i][j] + " ");
			}
			System.out.println();
		}
		input.close();
	}

	private static void ordenar(int[][] matriz) {
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				for (int k = 0; k < matriz.length; k++) {
					for (int l = 0; l < matriz[k].length; l++) {
						// si encuentro uno menor lo cambio
						if (matriz[i][j] < matriz[k][l]) {
							int aux = matriz[i][j];
							matriz[i][j] = matriz[k][l];
							matriz[k][l] = aux;
						}
					}
				}
			}
		}
	}

}
