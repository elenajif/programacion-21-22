package ejercicio14;

import java.util.Scanner;

public class Ejercicio14 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String[][] matriz = new String[2][5];

		// pido los datos
		// guardo los nombres en la fila 0
		// y las edades en la fila 1
		for (int i = 0; i < 5; i++) {
			System.out.println("Introduce un nombre");
			matriz[0][i] = input.nextLine();

			System.out.println("Introduce una edad");
			matriz[1][i] = input.nextLine();
		}

		// recorro los 5 registros de alumnos
		for (int i = 0; i < 5; i++) {
			System.out.println("Nombre " + matriz[0][i]);
			System.out.println("Edad " + matriz[1][i]);
		}

		input.close();
	}

}
