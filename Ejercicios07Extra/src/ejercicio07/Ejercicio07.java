package ejercicio07;

import java.util.Scanner;

public class Ejercicio07 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Dame el tama�o del vector");
		int tam = input.nextInt();

		// creamos los arrays
		int[] array1 = new int[tam];
		int[] array2;

		// rellenamos el array1
		Metodos07.rellenarNumAleatorioArray(array1, 10, 100);

		// apuntamos el array2 al array1
		array2 = array1;

		// creamos un nuevo array usando array1, tendra una nueva direccion
		array1 = new int[tam];

		// lo volvemos arellenar, al crear un array nuevo no tiene datos
		Metodos07.rellenarNumAleatorioArray(array1, 10, 100);

		// creamos array3 multiplicando
		int array3[] = Metodos07.multiplicador(array1, array2);

		// mostramos contenido arrays
		System.out.println("array1");
		Metodos07.mostrarArray(array1);
		System.out.println("array2");
		Metodos07.mostrarArray(array2);
		System.out.println("array3");
		Metodos07.mostrarArray(array3);

		input.close();

	}

}
