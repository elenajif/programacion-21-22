package ejercicio07;

public class Metodos07 {
	public static void rellenarNumAleatorioArray(int[] vector, int a, int b) {
		for (int i = 0; i < vector.length; i++) {
			vector[i] = ((int) Math.floor(Math.random() * (a - b) + b));
		}
	}

	public static void mostrarArray(int[] vector) {
		for (int i = 0; i < vector.length; i++) {
			System.out.println("En el indice " + i + " esta el valor " + vector[i]);
		}
	}

	public static int[] multiplicador(int[] vector1, int[] vector2) {
		int[] vector3 = new int[vector1.length];
		for (int i = 0; i < vector1.length; i++) {
			vector3[i] = vector1[i] * vector2[i];
		}
		return vector3;
	}

}
