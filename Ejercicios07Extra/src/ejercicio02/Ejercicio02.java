package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Dame el tama�o del vector");
		int tam = input.nextInt();
		int[] vectorNum = new int[tam];

		Metodos02.rellenarNumAleatorioArray(vectorNum, 0, 9);
		Metodos02.mostrarArray(vectorNum);
		
		input.close();
	}

}