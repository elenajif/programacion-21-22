package ejercicio02;

public class Metodos02 {
	public static void rellenarNumAleatorioArray(int[] vector, int a, int b) {
		for (int i = 0; i < vector.length; i++) {
			// Generamos un n�mero entre los parametros pasados
			vector[i] = ((int) Math.floor(Math.random() * (a - b) + b));
		}
	}

	public static void mostrarArray(int[] vector) {
		for (int i = 0; i < vector.length; i++) {
			System.out.println("En el indice " + i + " esta el valor " + vector[i]);
		}
	}

}
