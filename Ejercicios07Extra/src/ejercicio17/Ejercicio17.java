package ejercicio17;

public class Ejercicio17 {

	public static void main(String[] args) {
		// array a ordenar
		int[] vectorNumeros = { 5, 6, 2, 10, 1 };

		// llamada funcion
		Metodos17.ordenacionBurbuja(vectorNumeros);

		// mostrar el contenido
		for (int i = 0; i < vectorNumeros.length; i++) {
			System.out.println(vectorNumeros[i]);
		}
	}
}
