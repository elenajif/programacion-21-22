package ejercicio17;

public class Metodos17 {
	// Ordena un array de enteros por el metodo de la burbuja
	public static void ordenacionBurbuja(int[] vector) {
		int cuentaintercambios = 0; // Variable que cuenta los intercambios que hacemos
		for (boolean ordenado = false; !ordenado;) { // hasta que no haya mas intercambios no sales
			for (int i = 0; i < vector.length - 1; i++) {
				if (vector[i] > vector[i + 1]) {
					// intercambio
					int variableauxiliar = vector[i];
					vector[i] = vector[i + 1];
					vector[i + 1] = variableauxiliar;
					// indico que ha habido un intercambio mas
					cuentaintercambios++;
				}
			}
			// Si no hay intercambios, el array esta ordenado
			if (cuentaintercambios == 0) {
				ordenado = true;
			}
			cuentaintercambios = 0; // Reinicio la variable
		}
	}

}
