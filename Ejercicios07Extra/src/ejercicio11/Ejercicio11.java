package ejercicio11;

import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		// Introducimos un numero
		System.out.println("Dame un numero");
		String numero=input.nextLine();
		//aprovechamos los String para averiguar la longitud del numero
		//para crear un array y pasarlo a digitos
		int[] digitos=Metodos11.convierteNumeroArray(numero, numero.length());
		
		//llamamos al metodo que mostrara el resultado de si es o no capicua
		if (Metodos11.esCapicua(digitos)) {
			System.out.println("El numero "+numero+ " es capicua" );
		} else {
			System.out.println("El numero "+numero+ " no es capicua");
		}

		input.close();
	}

}
