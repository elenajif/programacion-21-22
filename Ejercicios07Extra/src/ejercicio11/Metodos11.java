package ejercicio11;

import java.util.Arrays;

public class Metodos11 {
	public static int[] convierteNumeroArray(String numero, int longitud) {
		int[] digitos = new int[longitud];
		for (int i = 0; i < digitos.length; i++) {
			digitos[i] = Character.getNumericValue(numero.charAt(i));
		}
		return digitos;
	}

	public static boolean esCapicua(int[] vector) {
		int[] listaPrueba = new int[vector.length];
		for (int i = 0, j = 1; j <= vector.length; i++, j++) {
			listaPrueba[i] = vector[vector.length - j];
		}
		
		if (Arrays.equals(vector, listaPrueba)) {
			return true;
		}
		return false;
	}

}
