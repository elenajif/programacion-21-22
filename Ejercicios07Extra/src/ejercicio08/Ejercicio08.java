package ejercicio08;

import java.util.Scanner;

public class Ejercicio08 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Dame el tama�o del vector");
		int tan = input.nextInt();
		int[] num = new int[tan];

		int ultimoDigito;
		do {
			System.out.println("Introduce un numero entre 0 y 9");
			ultimoDigito = input.nextInt();
		} while (!(ultimoDigito >= 0 && ultimoDigito <= 9));

		// rellenamos el array
		Metodos08.rellenarNumAleatorioArray(num, 1, 300);
		// creamos un array que contenga los numeros terminados en el num especificado
		int[] terminadosEn = Metodos08.numTerminadosEn(num, ultimoDigito);
		// mostramos el resultado
		Metodos08.mostrarArrayTerminadosEn(terminadosEn);

		input.close();
	}

}
