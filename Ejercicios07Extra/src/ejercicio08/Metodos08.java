package ejercicio08;

public class Metodos08 {
	public static void rellenarNumAleatorioArray(int[] vector, int a, int b) {
		for (int i = 0; i < vector.length; i++) {
			vector[i] = ((int) Math.floor(Math.random() * (a - b) + b));
		}
	}

	public static void mostrarArrayTerminadosEn(int[] vector) {
		for (int i = 0; i < vector.length; i++) {
			// no incluimos las posiciones que tienen un 0
			if (vector[i] != 0) {
				System.out.println("El numero " + vector[i] + " acaba en el numero deseado");
			}
		}
	}

	public static int[] numTerminadosEn(int[] vectorNum, int ultimoNumero) {
		// Array que almacenar� los numeros terminados en el numero pedido
		int[] terminadosEn = new int[vectorNum.length];
		int numeroFinal = 0;
		for (int i = 0; i < terminadosEn.length; i++) {
			// obtenemos las unidades usando el resto
			numeroFinal = vectorNum[i] % 10;
			// si el numero obtenido es el buscado, lo a�adimos
			if (numeroFinal == ultimoNumero) {
				terminadosEn[i] = vectorNum[i];
			}
		}
		return terminadosEn;
	}

}
