package ejercicio12;

public class Metodos12 {
	public static void invertirVector() {
		int[] vector1 = { 1, 2, 3, 4, 5 };
		int[] vector2 = new int[vector1.length];

		for (int i = (vector1.length - 1), j = 0; i >= 0; i--, j++) {
			vector2[j] = vector1[i];
			System.out.println(vector2[j]);
		}
	}
}
