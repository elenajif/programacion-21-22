package ejercicio10;

public class Ejercicio10 {

	public static void main(String[] args) {
		//tama�o del array
		final int TAMANO=10;
		//creamos los arrays
		String[] nombres=new String[TAMANO];
		double[] notas=new double[TAMANO];
		//rellenamos el array de una vez
		Metodos10.rellenarArrays(notas, nombres);
		//devuelve las notas con palabras
		String[] resultado=Metodos10.anadeResultado(notas);
		//mostramos el resultado
		Metodos10.mostrarArrays(nombres, notas, resultado);

	}

}
