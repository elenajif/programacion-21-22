package ejercicio05;

import java.util.Scanner;

public class Ejercicio05 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		char[] mayusculas = new char[26];
		for (int i = 65, j = 0; i <= 90; i++, j++) {
			mayusculas[j] = (char) i;
		}

		String cadena = "";
		int eleccion = -1;

		do {
			System.out.println("Elige un indice entre 0 y " + (mayusculas.length - 1));
			eleccion = input.nextInt();

			if (!(eleccion >= -1 && eleccion <= mayusculas.length - 1)) {
				System.out.println("Error, inserta otro n�mero");
			} else {
				if (eleccion != -1) {
					cadena += mayusculas[eleccion];
				}
			}

		} while (eleccion != -1);

		System.out.println(cadena);

		input.close();
	}

}
