package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		final int TAMANIO=10;
		int vectorNum[] = new int[TAMANIO];
		rellenarArray(vectorNum);
		mostrarArray(vectorNum);
	}
	
	static void rellenarArray(int[] vector) {
		Scanner input = new Scanner(System.in);
		for (int i=0;i<vector.length;i++) {
			System.out.println("Introduce un numero ");
			vector[i]=input.nextInt();
		}
		input.close();
	}
	
	static void mostrarArray(int[] vector) {
		for (int i=0;i<vector.length;i++) {
			System.out.println("Indice: "+i+" Valor: "+vector[i]);
		}
	}

}
