package ejercicio11bis;

import java.util.Scanner;

public class Ejercicio11 {
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.println("Introduce un n�mero y te digo si es capicua");
		String numero = input.nextLine();
		capicua(numero);
		input.close();
	}

	static void capicua(String numero) {
		String num = "";
		for (int i = (numero.length() - 1); i >= 0; i--) {
			num += numero.charAt(i);
		}
		if (num.equals(numero)) {
			System.out.println("Es capicua");
		} else {
			System.out.println("NO es capicua");
		}
	}
}