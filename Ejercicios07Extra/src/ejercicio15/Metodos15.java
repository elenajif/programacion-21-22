package ejercicio15;

import java.util.Arrays;

public class Metodos15 {
	public static void compruebaVectores() {

		// Creo el array
		int[] vectorNum1 = new int[10];
		int[] vectorNum2 = new int[10];

		// Comparo el contenido de ambos
		if (Arrays.equals(vectorNum1, vectorNum2)) {
			System.out.println("Son iguales");
		} else {
			System.out.println("No son iguales");
		}
	}
}
