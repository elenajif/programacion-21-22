package ejercicio09;

public class Metodos09 {
	public static char letraNIF(int res) {
		// definimos el array de char
		char[] letrasNIF = { 'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 
				'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V',
				'H', 'L', 'C', 'K', 'E' };
		// devolvemos el valor en la posicion del array
		return letrasNIF[res];
	}
}
