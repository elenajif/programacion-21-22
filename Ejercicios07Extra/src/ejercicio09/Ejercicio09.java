package ejercicio09;

import java.util.Scanner;

public class Ejercicio09 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		// Declaramos como constante el numero por el que dividir
		final int DIVISOR = 23;
		// insertamos el dni
		System.out.println("Dame los numeros del DNI");
		int dni = input.nextInt();
		// sacamos el resto
		int res = dni % DIVISOR;
		// llamamos al metodo
		char letra = Metodos09.letraNIF(res);
		// mostramos dni completo
		System.out.println("Tu DNI es " + dni + letra);
		input.close();
	}

}
