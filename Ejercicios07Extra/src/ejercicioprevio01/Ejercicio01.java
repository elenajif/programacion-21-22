package ejercicioprevio01;

import java.util.Scanner;

public class Ejercicio01 {
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		//creo vector llamando al metodo rellenarVector
		//el vector se rellena en la clase Metodos01
		String[] frases = Metodos01.rellenarVector();
		//llamo a visualizarPalabras que recibe un vector
		//usaremos un split para separar
		Metodos01.visualizarPalabras(frases);
		input.close();
	}

}
