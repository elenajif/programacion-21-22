package ejercicio19;

public class Metodos19 {
	// Genera un numero aleatorio entre el minimo y el maximo, includo el maximo
	// y el minimo
	public static int generaNumeroAleatorio(int minimo, int maximo) {
		int num = (int) Math.floor(Math.random() * (maximo - minimo + 1) + (minimo));
		return num;
	}
}
