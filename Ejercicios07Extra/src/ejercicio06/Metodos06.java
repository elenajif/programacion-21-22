package ejercicio06;

import java.util.Scanner;

public class Metodos06 {
	public static void caracteres() {
		Scanner input = new Scanner(System.in);
		// pido la frase
		System.out.println("Dame una frase");
		String frase = input.nextLine();
		// creamos un array de caracteres
		char[] caracteres = new char[frase.length()];
		// recorremos la frase y lo metemos en el array
		for (int i = 0; i < frase.length(); i++) {
			caracteres[i] = frase.charAt(i);
			System.out.println(caracteres[i]);
		}

		input.close();
	}

}
