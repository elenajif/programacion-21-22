package ejercicios05extra;

import java.util.Scanner;

public class Ejercicio03 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce la altura de la escalera");
		int altura = input.nextInt();

		for (int i = 1; i <= altura; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print("*");
			}
			System.out.println();
		}

		input.close();
	}

}
