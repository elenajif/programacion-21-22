package ejercicios05extra;

public class Ejercicio06 {

	public static void main(String[] args) {
		for(int i = 0; i < 4 ; i++){// 4 l�neas
            for(int j =0 ;j <= i; j++){// 4 columnas
                System.out.print(" ");// El �rea en blanco es un tri�ngulo con 4 filas y 4 columnas
            }
            for(int k = 0 ;k < (3-i)*2+1 ;k++){// Con el aumento de i, el cambio de k es 6 + 1,4 + 1,2 + 1,0 + 1
                // Es decir, adem�s de imprimir 1 espacio en blanco, hay 7 * en la primera l�nea, 5 * en la segunda l�nea, y as� sucesivamente.
                System.out.print("*");
            }
            System.out.println();
        }
	}

}
