package ejercicios05extra;

import java.util.Scanner;

public class Ejercicio05 {

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		System.out.print("Introduzca el lado del cuadrado: ");
		int lado = teclado.nextInt();
		teclado.close();

		System.out.println();
		for (int fila = 1; fila <= lado; fila++) {
			for (int columna = 1; columna <= lado; columna++) {
				if (fila == 1 || fila == lado || columna == 1 || columna == lado) {
					System.out.print("*");
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
		teclado.close();
	}

}
