package ficherossecuenciales;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class LecturaEscrituraFicheroSecuencial {

	public static void main(String[] args) throws IOException {
		// 1.- abrir el fichero para leer y el segundo fichero para escribir
		BufferedReader origen = new BufferedReader(new FileReader("datos.txt"));
		PrintWriter destino = new PrintWriter(new FileWriter("datosMayusculas.txt"));

		// 2.- leo del primero y escribo en el segundo en mayusculas
		String linea = "";
		linea = origen.readLine();
		while (linea != null) {
			destino.println(linea.toUpperCase());
			linea = origen.readLine();
		}

		// 3.- Cerrar los archivos
		origen.close();
		destino.close();

		// 4.- Visualizar el archivo origen
		System.out.println("Visualizo fichero origen");
		visualizarArchivo("datos.txt");

		// 5.- Visualizar el archivo destino
		System.out.println("Visualizo fichero destino");
		visualizarArchivo("datosMayusculas.txt");

	}

	public static void visualizarArchivo(String nombreArchivo) {
		try {
			// 1.- abro para lectura
			BufferedReader origen = new BufferedReader(new FileReader(nombreArchivo));
			// 2.- Recorro linea a linea hasta null
			String linea = "";
			linea = origen.readLine();
			while (linea != null) {
				System.out.println(linea);
				linea = origen.readLine();
			}
			// 3.- cierro fichero
			origen.close();
		} catch (FileNotFoundException e) {
			System.out.println("El fichero no existe");
			// cierro aplicacion
			System.exit(0);
		} catch (IOException e) {
			System.out.println("fichero no accesible");
			System.exit(0);
		}
	}

}
