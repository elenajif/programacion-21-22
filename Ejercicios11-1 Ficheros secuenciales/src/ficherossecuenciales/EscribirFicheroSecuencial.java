package ficherossecuenciales;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class EscribirFicheroSecuencial {
	// creamos una variable para guardar el nombre del archivo
	public static String archivo = "datos.txt";

	public static void main(String[] args) {
		try {
			// 1.- abrir el fichero para escribir
			PrintWriter f = new PrintWriter(new FileWriter(archivo, true));
			Scanner in = new Scanner(System.in);
			String linea="";
			//true escribir� al final
			//false escribir� al principio, escribe encima
			
			// 2.- escribo en el fichero
			System.out.println("Dame el texto que quieres guardar en el fichero");
			linea = in.nextLine();
			f.print("\n"+linea);
			
			// 3.- cierro el fichero
			f.close();
			in.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
