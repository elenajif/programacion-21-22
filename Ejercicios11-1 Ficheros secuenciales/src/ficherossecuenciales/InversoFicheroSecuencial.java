package ficherossecuenciales;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class InversoFicheroSecuencial {

	public static final String ARCHIVO = "datos.txt";
	public static final String ARCHIVO2 = "inverso.txt";

	public static void main(String[] args) throws IOException {
		// 1.- abrir el archivo para leer y crear un ArrayList
		BufferedReader origen = new BufferedReader(new FileReader(ARCHIVO));
		ArrayList<String> v = new ArrayList<String>();
		// 2.- Leo el archivo y escribo en el vector
		String linea = "";
		linea = origen.readLine();
		while (linea != null) {
			v.add(linea);
			linea = origen.readLine();
		}
		// 3.- Cierro el archivo
		origen.close();
		// 4.- Abro el segundo archivo para escribir
		PrintWriter destino = new PrintWriter(new FileWriter(ARCHIVO2, false));
		// 5.- Leo el vector del rev�s y voy escribiendo
		for (int i = v.size() - 1; i >= 0; i--) {
			destino.println(v.get(i));
		}

		// 6.- Cierro el archivo
		destino.close();

		// 7.- Visualizo primer archivo
		System.out.println("Visualizar archivo origen");
		visualizarArchivo(ARCHIVO);

		// 8.- Visualizo segundo archivo
		System.out.println("Visualizar archivo destino");
		visualizarArchivo(ARCHIVO2);

	}

	public static void visualizarArchivo(String nombreArchivo) {
		try {
			// 1.- abro para lectura
			BufferedReader origen = new BufferedReader(new FileReader(nombreArchivo));
			// 2.- Recorro linea a linea hasta null
			String linea = "";
			linea = origen.readLine();
			while (linea != null) {
				System.out.println(linea);
				linea = origen.readLine();
			}
			// 3.- cierro fichero
			origen.close();
		} catch (FileNotFoundException e) {
			System.out.println("El fichero no existe");
			// cierro aplicacion
			System.exit(0);
		} catch (IOException e) {
			System.out.println("fichero no accesible");
			System.exit(0);
		}
	}

}
