package inversoPoo;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Principal {

	public static void main(String[] args) throws IOException {
		// variables
		String nombreArchivo="datos.txt";
		
		//crear  objeto de clase Archivo
		//uso el constructor que tiene el nombre del archivo
		Archivo unArchivo = new Archivo(nombreArchivo);
		
		//crear archivo invertido
		//uso el objeto que tiene el nombre del archivo
		unArchivo.crearArchivoInvertido();
		
		//visualizar archivo origen (metodo static)
		//llamo partiendo de la clase, no del objeto
		System.out.println("Mostrar origen");
		//llamo usando nombre de variable
		Archivo.visualizarArchivo(nombreArchivo);
		
		//visualizar archivo destino (metodo static)
		System.out.println("Mostrar destino");
		//llamo sin usar nombre de variable
		Archivo.visualizarArchivo("nombreInvertido.txt");

	}

}
