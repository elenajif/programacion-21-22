package clases;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class Biblioteca implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final int DIAS_PRESTAMO = 15;

	private ArrayList<Articulos> articulos;
	private ArrayList<Socios> socios;
	private ArrayList<Prestamos> prestamos;

	public Biblioteca() {
		articulos = new ArrayList<Articulos>();
		socios = new ArrayList<Socios>();
		prestamos = new ArrayList<Prestamos>();
	}

	public void altaSocio(String nombre) {
		socios.add(new Socios(socios.size() + 1, nombre, LocalDate.now()));
	}

	// libro
	public void altaArticulo(String isbn, String titulo, String editorial, String autor) {
		articulos.add(new Libros(isbn, titulo, editorial, autor));
		Collections.sort(articulos);
	}

	// revista
	public void altaArticulo(String isbn, String titulo, String editorial, boolean online) {
		articulos.add(new Revistas(isbn, titulo, editorial, online));
		Collections.sort(articulos);
	}

	public void listarArticulos() {
		System.out.println("Articulos: ");
		for (Articulos articulos : articulos) {
			System.out.println(articulos);
		}
	}

	public void crearPrestamoSocio(int idSocio) {
		if (socioExiste(idSocio)) {
			prestamos.add(new Prestamos(prestamos.size() + 1, LocalDate.now(), LocalDate.now().plusDays(DIAS_PRESTAMO),
					devuelveSocio(idSocio)));
		} else {
			System.out.println("El socio no existe");
		}
	}

	public void mostrarPrestamosSocio(int idSocio) {
		for (Prestamos prestamo : prestamos) {
			if (prestamo.getSocio().equals(devuelveSocio(idSocio))) {
				System.out.println(prestamo);
			}
		}
	}

	public boolean socioExiste(int idSocio) {
		for (Socios socio : socios) {
			if (socio.getIdSocio() == idSocio) {
				return true;
			}
		}
		return false;
	}

	public Socios devuelveSocio(int idSocio) {
		for (Socios socio : socios) {
			if (socio.getIdSocio() == idSocio) {
				return socio;
			}
		}
		return null;
	}

	public void introducirArticuloPrestamo(int idPrestamo, String isbn) {
		if (prestamoExiste(idPrestamo)) {
			if (articuloExiste(isbn) && !devuelvePrestamo(idPrestamo).comprobarArticulos(isbn)) {
				devuelvePrestamo(idPrestamo).listaArticulos.add(devuelveArticulo(isbn));
			} else {
				System.out.println("El articulo no existe");
			}
		} else {
			System.out.println("El prestamo no existe");
		}
	}

	public boolean prestamoExiste(int idPrestamo) {
		for (Prestamos prestamo : prestamos) {
			if (prestamo.getIdPrestamo() == idPrestamo) {
				return true;
			}
		}
		return false;
	}

	public Prestamos devuelvePrestamo(int idPrestamo) {
		for (Prestamos prestamo : prestamos) {
			if (prestamo.getIdPrestamo() == idPrestamo) {
				return prestamo;
			}
		}
		return null;
	}

	public boolean articuloExiste(String isbn) {
		for (Articulos articulo : articulos) {
			if (articulo.getIsbn().equals(isbn)) {
				return true;
			}
		}
		return false;
	}

	public Articulos devuelveArticulo(String isbn) {
		for (Articulos articulo : articulos) {
			if (articulo.getIsbn().equals(isbn)) {
				return articulo;
			}
		}
		return null;
	}

	public void listarPrestamos() {
		for (Prestamos prestamo: prestamos) {
			System.out.println(prestamo);
		}
	}
	
	//cargar datos
	public void guardarDatos() {
		try  {
			ObjectOutputStream escritor = new ObjectOutputStream(new 
					FileOutputStream(new File("src/datos.dat")));
			escritor.writeObject(prestamos);
			escritor.writeObject(articulos);
			escritor.writeObject(socios);
			escritor.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//guardar datos
	@SuppressWarnings("unchecked")
	public void cargarDatos() {
		try {
			ObjectInputStream escritor = new ObjectInputStream(new
					FileInputStream (new File("src/datos.dat")));
			prestamos=(ArrayList<Prestamos>) escritor.readObject();		
			articulos=(ArrayList<Articulos>) escritor.readObject();
			socios=(ArrayList<Socios>) escritor.readObject();
			escritor.close();			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
}
