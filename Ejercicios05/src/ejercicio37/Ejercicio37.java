package ejercicio37;

public class Ejercicio37 {

	public static void main(String[] args) {
		// obtener los numeros perfectos hasta el 1000
		for (int i = 1; i <= 1000; i++) {
			// declaramos una variable que avanza igual que la i
			int numero = i;

			// para controlar si es perfecto, debo sumar sus divisores
			int sumaDivisores = 0;

			for (int j = 1; j < numero; j++) {
				if (numero % j == 0) {
					// si es divisor, lo guardo en suma de divisores
					// acumulamos -> variable = variable+nuevoValor
					sumaDivisores = sumaDivisores + j;
				}
			}
			// si la suma de divisores es igual al numero -> perfecto
			if (sumaDivisores == numero) {
				System.out.println(numero);
			}
		}
	}

}
