package ejercicio41;

import java.util.Scanner;

public class Ejercicio41 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce una altura");
		int altura = input.nextInt();
		
		//La i es la altura
		for(int i = 1; i <= altura; i++){
			
			//Calcular el numero de espacios, en funcion de la altura
			int numEspacios = altura - i; 
			for(int j = numEspacios; j > 0 ; j--){
				System.out.print(" ");
			}
			
			//calcular el numero de asteriscos, en funcion de la altura
			int numAsteriscos = i * 2 - 1;
			for(int k = 0; k < numAsteriscos; k++){
				System.out.print("*");
			}
			
			System.out.println();
		}
		
		
		input.close();
	}

}
