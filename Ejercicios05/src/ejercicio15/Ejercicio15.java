package ejercicio15;

import java.util.Scanner;

public class Ejercicio15 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		int numeroLeido; // numero
		int contadorNumerosLeidos = 0; // contador
		int sumaTotal = 0; // acumulador

		do {
			System.out.println("Introduce numeros enteros positivos o negativos");
			System.out.println("(0 para salir)");
			//leemos el numero
			numeroLeido=input.nextInt();
			//aumentamos contador
			contadorNumerosLeidos++;
			//acumulamos suma
			sumaTotal=sumaTotal+numeroLeido;			
		} while (numeroLeido != 0);
		
		System.out.println("La cantidad de numeros leidos es "+contadorNumerosLeidos);
		System.out.println("La suma de sus valores es "+sumaTotal);

		input.close();

	}

}
