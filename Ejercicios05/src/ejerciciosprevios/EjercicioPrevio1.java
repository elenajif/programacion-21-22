package ejerciciosprevios;

public class EjercicioPrevio1 {

	public static void main(String[] args) {
		// bucle for
		// for(comienzo;final;incremento)
		// la variable i se declara dentro del for
		// comenzando por la letra i
		System.out.println("Primer bucle");
		for (int i = 0; i < 5; i++) {
			System.out.println("Esto es una frase");
		}
		System.out.println("Segundo bucle");
		for (int i = 0; i < 5; i++) {
			System.out.println("Esto es la frase " + i);
		}
		System.out.println("Tercer bucle");
		for (int i = 1; i < 7; i++) {
			System.out.println("Esto es la frase " + i);
		}
		System.out.println("Cuarto bucle");
		for (int i = 5; i > 0; i--) {
			System.out.println("Esto es la frase " + i);
		}
		System.out.println("Quinto bucle");
		for (int i = 0; i < 10; i += 2) {
			System.out.println("Esto es la frase " + i);
		}
		// �mbito de las variables
		// una variable declarada dentro del main, ser� v�lida para todo el main
		// una variable declarada dentro de un for, solo ser� v�lida en ese for
	}

}
