package ejerciciosprevios;

import java.util.Scanner;

public class EjercicioPrevio2 {

	public static void main(String[] args) {
		//bucle for pidiendo el numero de veces que se repite
		
		//pedimos el dato que va a ser el numero de vueltas
		Scanner entrada = new Scanner(System.in);
		System.out.println("Cuantas vueltas quieres que de el for?");
		int numero = entrada.nextInt();
		
		//hacemos el for hasta el numero que hemos pedido
		for (int i = 0; i < numero; i++) {
			System.out.println("Vuelta numero " + i);
		}
		
		entrada.close();
	}

}
