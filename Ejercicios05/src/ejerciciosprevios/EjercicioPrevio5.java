package ejerciciosprevios;

import java.util.Scanner;

public class EjercicioPrevio5 {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		System.out.println("Dame el valor de cantidad (menor de 10)");
		int cantidad=entrada.nextInt();

		do {
			System.out.println("La cantidad es " + cantidad);
			cantidad = cantidad + 1;
		} while (cantidad < 10);
		entrada.close();
	}

}
