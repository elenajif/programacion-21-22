package ejercicio09;

public class Ejercicio09 {

	public static void main(String[] args) {
		System.out.println("Voy a generar un numero aleatorio");
		double aleatorio;
		// genero un numero aleatorio entre 0 y 100
		aleatorio = Math.random() * 101;
		// lo convierto a int
		int enteroAleatorio = (int) aleatorio;

		System.out.println("Aleatorio double " + aleatorio);
		System.out.println("Aleatorio entero " + enteroAleatorio);

		// muestro los numeros desde ese n� aleatorio hasta -100 de 7 en 7
		for (int i = enteroAleatorio; i >= -100; i = i - 7) {
			System.out.println(i);
		}

	}

}
