package ejercicio16;

import java.util.Scanner;

public class Ejercicio16 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Dame la cantidad de sueldos a pedir");
		int cantidadSueldos = input.nextInt();

		// uso float valor literal
		float mayor = 0.0F;

		for (int i = 0; i < cantidadSueldos; i++) {
			System.out.println("Dame el sueldo " + (i + 1) + ":");
			float sueldo = input.nextFloat();

			// si es el primer sueldo, ese es el mayor
			if (sueldo > mayor) {
				mayor = sueldo;
			}
		}
		
		System.out.println("El mayor sueldo es "+mayor);

		input.close();

	}

}
