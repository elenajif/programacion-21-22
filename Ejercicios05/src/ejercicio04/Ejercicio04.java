package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Dame un numero mayor");
		int mayor = input.nextInt();
		System.out.println("Dame un numero menor");
		int menor = input.nextInt();

		// varias lineas
		for (int i = mayor; i >= menor; i--) {
			System.out.println(i);
		}

		// una linea
		for (int i = mayor; i >= menor; i--) {
			System.out.print(i + " ");
		}

		input.close();

	}

}
