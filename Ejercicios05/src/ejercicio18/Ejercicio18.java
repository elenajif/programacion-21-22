package ejercicio18;

import java.util.Scanner;

public class Ejercicio18 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce el tama�o del lado");
		int lado = input.nextInt();

		//recordatorio, cuando se usa mas de un for
		//el contador es i,j,k ...
		for (int i = 0; i < lado; i++) {
			for (int j = 0; j < lado; j++) {
					System.out.print("x ");
			}
			System.out.print("\n");
			//es lo mismo que System.out.println();
		}

		input.close();

	}

}
