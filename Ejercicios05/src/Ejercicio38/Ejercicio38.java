package Ejercicio38;

import java.util.Scanner;

public class Ejercicio38 {

	public static void main(String[] args) {
		// Math.random() -> puedo generar un numero aleatorio
		// *100 estar� entre 0 y 100
		// si lo casteo sera entero (int)
		// Math.round -> redondeo
		// obtener un numero aleatorio entre 1 y 100
		Scanner input = new Scanner(System.in);

		int aleatorio = (int) (Math.round(Math.random() * 100));
		int numero;

		do {
			System.out.println("Adivina el numero");
			numero = input.nextInt();
			if (numero > aleatorio) {
				System.out.println("El numero que has introducido es mayor");
			} else if (numero < aleatorio) {
				System.out.println("El numero que has introducido es menor");
			} else {
				System.out.println("Has acertado");
			}
		} while (numero != aleatorio);

		input.close();
	}

}
