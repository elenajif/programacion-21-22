package ejercicio13;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Dame la base");
		int base = input.nextInt();
		System.out.println("Dame el exponente");
		int exponente = input.nextInt();

		int resultado = 1;
		// los acumuladores que suman se inicializan a 0
		// los acumuladores que multiplican se inicializan a 1

		// bucle con la operacion
		for (int i = 0; i < exponente; i++) {
			resultado = resultado * base;
		}
		
		System.out.println("La potencia de "+base+" elevado a "+exponente+" es "+resultado);

		input.close();

	}

}
