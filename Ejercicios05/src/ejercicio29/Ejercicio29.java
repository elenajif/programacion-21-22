package ejercicio29;

import java.util.Scanner;

public class Ejercicio29 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		// cadena
		String cadena;
		// contador para las vocales
		int contadorVocales = 0;
		// acumulador para contar todos los caracteres
		int contadorTotalCaracteres = 0;
		// cadena total
		String cadenaTotal = "";

		do {
			// pido cadena
			System.out.println("Introduce una cadena");
			cadena = input.nextLine().toLowerCase();
			// uso toLowerCase para reducir el codigo de comparar vocales

			// voy sumando caracteres acumulando
			contadorTotalCaracteres = contadorTotalCaracteres + cadena.length();

			// compruebo las vocales para contar en contadorVocales
			//comparar letras (char) ==' '
			for (int i= 0;i<cadena.length();i++) {
				char letra=cadena.charAt(i);			
				if (letra=='a' || letra=='e' || letra=='i'||letra=='o'||letra=='u') {
					contadorVocales++;
				}
			}		
			//concateno cadena leida
			cadenaTotal=cadenaTotal+cadena;

		} while (!cadena.equals("fin"));
		//comparar cadenas .equals(" ")

		System.out.println("Cantidad vocales "+contadorVocales);
		System.out.println("Porcentaje de vocales "+
				((double)contadorVocales/contadorTotalCaracteres*100)+"%");
		System.out.println("Cadena total "+cadenaTotal);

		input.close();

	}

}
