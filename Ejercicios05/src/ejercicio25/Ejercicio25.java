package ejercicio25;

import java.util.Scanner;

public class Ejercicio25 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		char respuesta;
		double centigrados;
		
		do {
			System.out.println("Introduce grados centigrados");
			centigrados=input.nextDouble();
			//acabo de leer un numero 
			//antes de leer el caracter tengo que limpiar el buffer
			input.nextLine();
			System.out.println("Kelvin "+(centigrados+273));
			System.out.println("Repetimos (S/N)");
			respuesta=input.nextLine().charAt(0);			
		} while (respuesta=='S' || respuesta=='s');
		
		input.close();

	}

}
