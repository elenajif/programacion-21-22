package ejercicio05;

import java.util.Scanner;

public class Ejercicio05 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce el inicio de la serie");
		int inicio = input.nextInt();
		System.out.println("Introduce el fin de la serie");
		int fin = input.nextInt();
		System.out.println("Introduce el salto");
		int salto = input.nextInt();

		if (salto > 0) {
			if (inicio > fin) {
				for (int i = inicio; i >= fin; i = i - salto) {
					System.out.println(i);
				}
			} else {
				for (int i = inicio; i <= fin; i = i + salto) {
					System.out.println(i);
				}
			}
		} else {
			System.out.println("El salto debe ser mayor a 1");
		}

		input.close();

	}

}
