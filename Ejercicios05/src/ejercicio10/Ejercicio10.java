package ejercicio10;

import java.util.Scanner;

public class Ejercicio10 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		// creo la variable resultado que guardara el total de la suma
		double resultado = 0;

		for (int i = 0; i < 10; i++) {
			System.out.println("Introduce un decimal");
			double numero = input.nextDouble();
			// Almaceno en resultado lo que ya contenia
			// y lo sumo al numero leido
			resultado = resultado + numero;
			//resultado es un acumulador
		}
		
		System.out.println("La suma es "+resultado);
		input.close();
	}

}
