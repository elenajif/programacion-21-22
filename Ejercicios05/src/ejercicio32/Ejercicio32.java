package ejercicio32;

import java.util.Scanner;

public class Ejercicio32 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Dame la altura de la escalera");
		int altura = input.nextInt();

		// se repite hasta la altura
		// por cada nivel muestro tantos numeros como el valor del nivel
		for (int i = 1; i <= altura; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(j);
			}
			System.out.println();
		}

		input.close();

	}

}
