package ejercicio36;

import java.util.Scanner;

public class Ejercicio36 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce una serie de palabras separadas por 1 espacio");
		String cadena = input.nextLine();

		int contadorEspacios = 0;

		for (int i = 0; i < cadena.length(); i++) {
			if (cadena.charAt(i) == ' ') {
				contadorEspacios++;
			}
		}

		System.out.println("Hay " + (contadorEspacios + 1) + " palabras");

		input.close();

	}

}
