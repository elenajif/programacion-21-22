package ejercicio35;

import java.util.Scanner;

public class Ejercicio35 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce un numero");
		int limite = input.nextInt();

		// declaro una variable que contar� los primos
		int contadorPrimos = 0;

		// el for comienza en 1, contador de Primos
		for (int i = 1; i <= limite; i++) {
			// compruebo si el numero es primo
			int numero = i;
			boolean esPrimo = true;

			for (int j = 2; j < numero; j++) {
				// si encuentro algun divisor, no es primo
				if (numero % j == 0) {
					esPrimo = false;
					break;
				}
			}
			if (esPrimo) {
				contadorPrimos++;
			}
		}
		// mostramos el resultado
		System.out.println("Hay " + contadorPrimos + " numeros primos");
		input.close();
	}
}
