package ejercicio35;

import java.util.Scanner;

public class Ejercicio35Bis {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String numerosPrimos = "";
		System.out.println("Introduce numero para que te calcule los numeros primos");
		int numero = input.nextInt();
		int contadorPrimos = 0;
		int totalPrimos = 0;
		for (int i = 1; i <= numero; i++) {
			for (int j = 1; j <= i; j++) {
				if (i % j == 0) {
					contadorPrimos++;
				}
			}
			if (contadorPrimos == 2) {
				totalPrimos++;
				numerosPrimos += i + " ";
			}
			contadorPrimos = 0;
		}
		System.out.println("Los n�meros primos son: " + numerosPrimos + "y el total son " + totalPrimos);
		input.close();
	}

}