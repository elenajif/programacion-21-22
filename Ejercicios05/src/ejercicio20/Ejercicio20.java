package ejercicio20;

import java.util.Scanner;

public class Ejercicio20 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce un caracter");
		char caracter = input.nextLine().charAt(0);
		System.out.println("Introduce una cadena");
		String cadena = input.nextLine();

		// creamos un contador que se debe inicializar a 0
		int contadorCaracter = 0;

		// cadena.length() -> longitud de la cadena
		// comparar numeros -> if (numero==3)
		// comparar char -> if (cadena.charAt(i)=='a')
		// comparar String -> if (cadena.equals("hola"))
		for (int i = 0; i < cadena.length(); i++) {
			if (cadena.charAt(i) == caracter) {
				contadorCaracter++;
			}
		}
		System.out.println("La cantidad de veces que aparece el caracter " + caracter 
				+ " es " + contadorCaracter);

		input.close();

	}

}
