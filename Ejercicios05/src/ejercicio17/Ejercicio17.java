package ejercicio17;

import java.util.Scanner;

public class Ejercicio17 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		// declaro una variable que guardara si hay o no negativos
		boolean hayNegativos = false;
		int numero;
		for (int i = 0; i < 10; i++) {
			System.out.println("Introduce un numero");
			numero = input.nextInt();
			// revisar si es negativo
			if (numero < 0) {
				hayNegativos = true;
			}
		}
		// comprobar al finalizar el bucle si hay negativos
		// cuando es una variable boolean
		// se puede poner un if sin tener que comparar con true
		// hayNegativos==true es lo mismo que poner hayNegativos
		if (hayNegativos) {
			System.out.println("Hay negativos");
		} else {
			System.out.println("No hay negativos");
		}
		input.close();
	}

}
