package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {

	final static String MENSAJE = "Fin del programa";

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce un numero");
		int numero = input.nextInt();

		int contador = numero;
		// muestro las lineas separadas
		do {
			System.out.println(contador);
			contador--;
		} while (contador > 0);

		// muestro las lineas juntas pero con un espacio
		contador = numero;
		do {
			System.out.print(contador + " ");
			contador--;
		} while (contador > 0);

		input.close();
		System.out.println("");
		System.out.println(MENSAJE);
	}

}
