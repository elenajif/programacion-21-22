package ejercicio19;

import java.util.Scanner;

public class Ejercicio19 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		// asumo que no hay multiplos
		boolean hayMultiplos = false;
		int numero;

		for (int i = 0; i < 5; i++) {
			System.out.println("Introduce un numero");
			numero = input.nextInt();
			// compruebo si es multiplo y si lo es, cambio el valor del booleano
			if (numero % 3 == 0) {
				hayMultiplos = true;
			}
		}
		// al terminar muestro si hay o no multiplos
		if (hayMultiplos) {
			System.out.println("Hay multiplos");
		} else {
			System.out.println("No hay multiplos");
		}

		input.close();

	}

}
