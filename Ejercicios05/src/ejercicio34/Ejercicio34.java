package ejercicio34;

import java.util.Scanner;

public class Ejercicio34 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce un numero");
		int numero = input.nextInt();

		// para calcular si es primo, compruebo si tiene algun divisor

		// creo una variable que se guarda si es primo o no
		boolean esPrimo = true;
		for (int i = 2; i < numero; i++) {
			// si encuentro algun divisor, no es primo
			if (numero % i == 0) {
				esPrimo = false;
				break; // en el momento que encuentra un divisor, salgo del bucle
			}
		}

		// muestro si la variable es true o false indicando si es primo o no
		//poner esPrimo==true -> esPrimo
		if (esPrimo) {
			System.out.println("El numero es primo");
		} else {
			System.out.println("El numero no es primo");
		}

		input.close();

	}

}
