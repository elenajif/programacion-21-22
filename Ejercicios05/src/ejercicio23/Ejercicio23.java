package ejercicio23;

import java.util.Scanner;

public class Ejercicio23 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		// suma de las notas -> acumular la suma
		// total de notas introducidas -> acumular la cantidad
		// calcular la media -> suma / cantidad
		double sumaNotas = 0;
		int cantidadNotas = 0;

		double nota;

		do {
			System.out.println("Dame la nota del alumno");
			nota = input.nextDouble();

			if (nota >= 0) {
				sumaNotas = sumaNotas + nota;
				cantidadNotas++;
			}
		} while (nota >= 0);
		
		//muestro la media si al menos se ha introducido alguna nota
		if (cantidadNotas!=0) {
			System.out.println("La media es "+(sumaNotas/cantidadNotas));
		}

		input.close();

	}

}
