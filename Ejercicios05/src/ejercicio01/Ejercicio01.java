package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	final static String MENSAJE = "Fin del programa";

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce un numero");
		int numero = input.nextInt();

		// muestro las lineas separadas
		for (int i = 1; i <= numero; i++) {
			System.out.println(i);
		}

		// muestro las lineas juntas pero con un espacio
		for (int i = 1; i <= numero; i++) {
			System.out.print(i + " ");
		}

		input.close();
		System.out.println("");
		System.out.println(MENSAJE);
	}

}
