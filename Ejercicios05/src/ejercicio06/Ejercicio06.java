package ejercicio06;

import java.util.Scanner;

public class Ejercicio06 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce un numero");
		int numero = input.nextInt();

		int contadorCifras = 0;

		// divido mientras que no sea 0
		// cuento las veces que soy capaz de dividir entre 0
		while (numero > 0) {
			numero = numero / 10;
			contadorCifras++;
		}
		
		//contadorCifras es un contador

		System.out.println("La cantidad de cifras es " + contadorCifras);

		input.close();

	}

}
