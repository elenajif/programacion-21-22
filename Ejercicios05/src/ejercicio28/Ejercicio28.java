package ejercicio28;

import java.util.Scanner;

public class Ejercicio28 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		int numLeido;
		int contadorNumLeidos = 0;
		int mayor = 0;
		int menor = 999999999;

		do {
			// pido dato a leer
			System.out.println("Introduce numeros (termina con el 0)");
			numLeido = input.nextInt();
			// aumento contador de datos leidos
			contadorNumLeidos++;
			// compruebo si es mayor
			if (numLeido != 0 && numLeido > mayor) {
				mayor = numLeido;
			}
			// compruebo si es menor
			if (numLeido != 0 && numLeido < menor) {
				menor = numLeido;
			}

		} while (numLeido != 0);

		System.out.println("La cantidad de numeros leidos es " + (contadorNumLeidos - 1));
		System.out.println("El mayor es " + mayor);
		System.out.println("El menor es " + menor);
		input.close();

	}

}
