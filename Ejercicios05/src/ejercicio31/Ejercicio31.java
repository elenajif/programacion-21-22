package ejercicio31;

import java.util.Scanner;

public class Ejercicio31 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce un numero");
		int numero = input.nextInt();

		// para calcular si es perfecto debemos sumar todos sus divisores
		// creo una variable sumaDivisores -> acumulador
		// variable = variable + nuevoValor
		// acumulador de suma =0
		// acumulador de producto = 1
		int sumaDivisores = 0;

		// bucle para comprobar los divisores
		// un numero es divisor de 2 -> numero%2==0
		// un numero es divisor de 3 -> numero%3==0
		//el bucle no puede comenzar en 0 
		for (int i = 1; i < numero; i++) {
			if (numero % i == 0) {
				// es un divisor, acumulo la suma
				sumaDivisores = sumaDivisores + i;
			}
		}

		// si la suma de divisiones es igual al numero -> es perfecto
		if (sumaDivisores == numero) {
			System.out.println("El numero es perfecto");
		} else {
			System.out.println("El numero no es perfecto");
		}

		input.close();

	}

}
