package ejercicio12;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int opcion = 0;
		// al menos debe entrar una vez en el menu -> do while
		do {
			System.out.println("1.- opcion1");
			System.out.println("2.- opcion2");
			System.out.println("3.- salir");
			opcion = input.nextInt();
			switch (opcion) {
			case 1:
				System.out.println("Opcion1");
				break;
			case 2:
				System.out.println("Opcion2");
				break;
			case 3:
				System.out.println("Adios");
				break;
			}
		} while (opcion != 3);
		input.close();

	}

}
