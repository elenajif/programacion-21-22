package ejercicio07;

import java.util.Scanner;

public class Ejercicio07 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce un numero");
		int numero = input.nextInt();

		System.out.println("Tabla del numero " + numero + "\n");

		// con el \n incorporo un intro

		for (int i = 1; i <= 10; i++) {
			System.out.println(numero + " x " + i + " = " + (i * numero));
		}

		input.close();
	}

}
