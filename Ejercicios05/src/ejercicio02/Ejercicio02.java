package ejercicio02;

public class Ejercicio02 {

	final static String MENSAJE = "Fin del programa";

	public static void main(String[] args) {

		int contador = 100;

		// varias lineas
		while (contador > 0) {
			System.out.println(contador);
			contador--;
		}

		contador = 100;
		// una linea
		while (contador > 0) {
			System.out.print(contador + " ");
			contador--;
		}

		System.out.println("");
		System.out.println(MENSAJE);
	}

}
