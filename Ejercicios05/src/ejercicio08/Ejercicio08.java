package ejercicio08;

public class Ejercicio08 {

	public static void main(String[] args) {
		// con int
		// tengo que castear la i para que muestre un char
		// (char)i
		System.out.println("Forma1");
		for (int i = 65; i < 91; i++) {
			System.out.println((char) i);
			if (i == 'N') {
				System.out.println("�");
			}
		}

		// con char
		System.out.println("Forma2");
		for (char i = 'A'; i <= 'Z'; i++) {
			System.out.println(i);
			if (i == 'N') {
				System.out.println("�");
			}
		}

	}

}
