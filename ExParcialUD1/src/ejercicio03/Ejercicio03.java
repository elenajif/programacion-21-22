package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {

	// declaro dos constantes
	static final String NOMBRE_USUARIO = "juan";
	static final String CODIGO_DESCUENTO = "12342";

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		String nombre;
		String codigo;
		
		System.out.println("Dame el nombre de usuario");
		nombre=entrada.nextLine();
		System.out.println("Dame el codigo de descuento");
		codigo=entrada.nextLine();
		
		//coincidencia completa -> equals
		if (nombre.equals(NOMBRE_USUARIO) && codigo.equals(CODIGO_DESCUENTO)) {
			System.out.println("El descuento es v�lido");
		} else {
			System.out.println("Nombre de usuario o descuento no v�lido");
		}

		entrada.close();

	}

}
