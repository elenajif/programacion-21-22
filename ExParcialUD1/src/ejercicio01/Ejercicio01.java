package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		// declaro Scanner para poder pedir datos por pantalla
		Scanner entrada = new Scanner(System.in);
		// declaro tres variables para las tres notas
		double n1;
		double n2;
		double n3;

		// pido las notas por teclado
		System.out.println("Dame una nota");
		// leo el dato por teclado
		n1 = entrada.nextDouble();
		// repito con las otras dos notas
		System.out.println("Dame otra nota");
		n2 = entrada.nextDouble();
		System.out.println("Dame una tercera nota");
		n3 = entrada.nextDouble();

		// CTRL+MAYUS+F -> tabular

		System.out.println("La nota media es " + (n1 * 0.15 + n2 * 0.15 + n3 * 0.7));

		// cierro el Scanner
		entrada.close();
	}

}
