package programa;

import java.util.ArrayList;

import figuras.Circulo;
import figuras.Cuadrado;
import figuras.Figura;
import figuras.Rectangulo;
import figuras.Triangulo;

public class EjercicioFiguras {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //ME creo las figuras
        Triangulo triangulo = new Triangulo(0,0,5,3);
        Cuadrado cuadrado = new Cuadrado(0,0,2);
        Rectangulo rectangulo = new Rectangulo(0,0,5,3);
        Circulo circulo = new Circulo(0,0,3);
        
        
        //Creo el ArrayList de figuras
        ArrayList<Figura> listafiguras =new ArrayList<Figura>();
        
        //introduzco las figuras en el arraylist
        listafiguras.add(triangulo);
        listafiguras.add(cuadrado);
        listafiguras.add(rectangulo);
        listafiguras.add(circulo);
        
        
        System.out.println("MUEVO LAS FIGURAS");
        //Movemos las figuras a la coordenada (1,1)
        for (int i=0;i<listafiguras.size();i++) {
            //obtenemos la figura del ArrayList
            Figura fig = listafiguras.get(i);
            //y la movemos
            fig.mover(i,i);
        }
        
        
        System.out.println("\n\nMUESTRO LAS COORDENADAS DE LAS FIGURAS");
        //Recorremos el ArrayList para ver las coordenadas
        for (int i=0;i<listafiguras.size();i++) {
            //obtenemos la figura del ArrayList
            Figura fig = listafiguras.get(i);
            
            System.out.println("ORIGEN DE FIGURA => ("+fig.getX()+","+fig.getY()+")");
            
        }
        
        
        
        System.out.println("\n\nMUESTRO EL AREA DE LAS FIGURAS");
        //Recorremos el ArrayList para calcular las areas de las figuras
        for (int i=0;i<listafiguras.size();i++) {
            //obtenemos la figura del ArrayList
            Figura fig =  listafiguras.get(i);
            
            System.out.println("AREA DE FIGURA => "+fig.getArea());
            
        }
        
        
        System.out.println("\n\nDIBUJO LAS FIGURAS");
        //Recorremos el ArrayList para dibujar las figuras
        for (int i=0;i<listafiguras.size();i++) {
            //obtenemos la figura del ArrayList
            Figura fig =  listafiguras.get(i);
            
            fig.dibujar();
            
        }
        
    }
    
}
