
package figuras;


public abstract class Figura {
    
    //Creo los atributos de las coordenadas
    private int x, y;
    
    /**
     * Constructor de la clase Figura
     * 
     * @param x int que representa la coordenada x
     * @param y int que representa la coordenada y
     */        
    public Figura(int x, int y){
        this.x = x;
        this.y = y;
    }
    
    /**
     * Defino el metodo abstracto getArea. Este metodo debera ser definido
     * en las clases que hereden de Figura
     * 
     * @return float que representa el area de una figura 
     */
    public abstract float getArea();
    
    
    /**
     * 
     * @return 
     */
    public abstract void dibujar();
    
    /**
     * Metodo que modifica las coordenadas de una figura
     * 
     * @param x int que representa a la coordenada x
     * @param y int que representa a la coordenada y
     */
    public void mover(int x, int y){
        this.x = x;
        this.y = y;
    }

    
    /**
     * Metodo get que retorna el valor de la coordenada x
     * 
     * @return int que representa la coordenada x de una figura 
     */
    public int getX() {
        return x;
    }

    
    /**
     * Metodo que retorna el valor de la coordenada y
     * 
     * @return int que representa la coordenada y de una figura 
     */
    public int getY() {
        return y;
    }
    
    
}
