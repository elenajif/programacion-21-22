
package figuras;

public class Circulo extends Figura{
    
    //variables de la clase radio
    private double radio;
    
    
    /**
     * Constructor de la clase circulo
     * 
     * @param x int que represeenta la coordenada x de una figura
     * @param y int que representa la coordenada y de una figura
     * @param radio  double que representa el radio de un circulo
     */
    public Circulo(int x, int y, double radio){
        super(x,y);
        this.radio = radio;
    }

    
    /**
     * metodo que retorna el area de un circulo
     * 
     * @return float que representa el area de un circulo 
     */
    @Override
    public float getArea() {
        return (float) (Math.PI * Math.pow(radio, 2));
    }

    
    /**
     * Metodo que dibuja una figura
     */
    @Override
    public void dibujar() {
        System.out.println("Dibujo un circulo");
    }
    
    
    
    
}
