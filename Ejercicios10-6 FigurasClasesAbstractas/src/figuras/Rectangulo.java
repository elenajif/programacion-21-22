
package figuras;

public class Rectangulo extends Figura{
    
    //Creo las variables de la clase
    private int largo, alto;
    
    
    /**
     * Constructor de la clase Rectangulo
     * 
     * @param x int que representa la coordenada x de la figura
     * @param y int que representa la coordenada y de la figura
     * @param largo int que representa el largo de unrectangulo
     * @param alto int que represetna el largo de un rectangulo
     */
    public Rectangulo(int x, int y, int largo, int alto){
        super(x,y);
        this.largo = largo;
        this.alto = alto;
    }

    
    /**
     * metodo que retorna el area de un rectangulo
     * 
     * @return float que representa el area de un rectangulo 
     */
    @Override
    public float getArea() {
        return this.largo * this.alto;
    }
    
    
    
    /**
     * Metodo que dibuja una figura
     */
    @Override
    public void dibujar() {
        System.out.println("Dibujo un Rectangulo");
    }
    
    
}
