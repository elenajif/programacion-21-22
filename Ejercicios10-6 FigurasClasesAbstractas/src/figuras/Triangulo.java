
package figuras;

public class Triangulo extends Figura{
    
    //variables de la clase triangulo
    private float base, altura;
    
    
    /**
     * Constructor de la clase Triangulo
     * 
     * @param x int que representa la coordenada x de una fogura
     * @param y int que representa la coordenada y de una fogura
     * @param base int que representa la base de un triangulo
     * @param altura int que representa la altura de un triangulo
     */
    public Triangulo(int x, int y, int base, int altura){
        super(x,y);
        this.base = base;
        this.altura = altura;
    }

    
    /**
     * metodo que retorna el area de un triangulo
     * 
     * @return float que representa el area de un triangulo 
     */
    @Override
    public float getArea() {
        return (this.base * this.altura) / 2;
    }

    
    /**
     * Metodo que dibuja una figura
     */
    @Override
    public void dibujar() {
        System.out.println("Dibujo un triangulo");
    }
    
    
}
