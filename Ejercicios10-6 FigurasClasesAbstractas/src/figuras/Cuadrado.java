
package figuras;

public class Cuadrado extends Figura{
    
    
    //Creo las variables de la clase Cuadrado
    private int lado;
    
    
    /**
     * Constructor de la clase Rectangulo
     * 
     * @param x int que representa la coordenada x de la figura
     * @param y int que representa la coordenada y de la figura
     * @param largo int que representa el largo de unrectangulo
     * @param alto int que represetna el largo de un rectangulo
     */
    public Cuadrado(int x, int y, int lado){
        super(x,y);
        this.lado = lado;
    }

    
    
    /**
     * metodo que retorna el area de un cuadrado
     * 
     * @return float que representa el area de un cuadrado 
     */
    @Override
    public float getArea() {
        return this.lado * this.lado;
    }

    
    /**
     * Metodo que dibuja una figura
     */
    @Override
    public void dibujar() {
        System.out.println("Dibujo un cuadrado");
    }
    
    
    
    
}
