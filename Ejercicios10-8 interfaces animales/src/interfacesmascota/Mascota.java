package interfacesmascota;

import clasesmascota.Animal;

public interface Mascota {
	//todos los metodos son abstractos
	String getCodigo();
	void hazRuido();
	void come(String comida);
	void peleaCon(Animal contrincante);
}
