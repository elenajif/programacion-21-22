package ejercicio04;

public class Ejercicio04 {

	public static void main(String[] args) {
		byte var1 = 25;
		short var2 = 33;
		int var3 = 2500;
		long var4 = 3000;
		float var5 = 5.2f;
		double var6 = 25.3;
		char var7 = 'c';
		String var8 = "hola";
		boolean var9 = true;

		System.out.println("var1 es byte " + var1);
		System.out.println("var2 es short " + var2);
		System.out.println("var3 es int " + var3);
		System.out.println("var4 es long " + var4);
		System.out.println("var5 es float " + var5);
		System.out.println("var6 es double " + var6);
		System.out.println("var7 es char " + var7);
		System.out.println("var8 es String " + var8);
		System.out.println("var9 es boolean " + var9);

	}

}
