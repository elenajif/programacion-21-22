package ejercicio07;

public class Ejercicio07 {

	public static void main(String[] args) {
		// Declarar numero1 entero y darle un valor
		int numero1 = 5;
		// Mostrar numero1
		System.out.println(numero1);
		// Declarar numero2 entero, numero2 ser� numero1 + un numero entero cualquiera
		int numero2;
		numero2 = numero1 + 7;
		// Mostrar numero2
		System.out.println(numero2);
		// Declarar numero3 entero, numero3 ser� numero1 � un numero entero cualquiera
		int numero3;
		numero3 = numero1 - 4;
		// Mostrar numero3
		System.out.println(numero3);
		// Declarar numero4 entero y darle un valor
		int numero4 = 25;
		// Mostrar numero4
		System.out.println(numero4);
		// Incrementar con el operador += un numero entero cualquiera
		numero4 += 5;
		// Mostrar numero4
		System.out.println(numero4);
		// Declarar numero5 entero y darle un valor
		int numero5 = 10;
		// Mostrar numero5
		System.out.println(numero5);
		// Decrementar con el operador -= un numero entero cualquiera
		numero5 -= 3;
		// Mostrar numero5
		System.out.println(numero5);
		// Declarar numero6 entero y darle un valor
		int numero6 = 12;
		// Mostrar numero6
		System.out.println(numero6);
		// Multiplicar con el operador *= un numero entero cualquiera
		numero6 *= 2;
		// Mostrar numero6
		System.out.println(numero6);
		// Declarar numero7 entero y darle un valor
		int numero7 = 30;
		// Mostrar numero7
		System.out.println(numero7);
		// Dividir con el operador /= un numero entero cualquiera
		numero7 /= 5;
		// Mostrar numero7
		System.out.println(numero7);
		// Declarar numero8 entero y darle un valor
		int numero8 = 125;
		// Mostrar numero8
		System.out.println(numero8);
		// Aumentar su valor con operador ++
		numero8++;
		// Mostrar numero8
		System.out.println(numero8);
		// Declarar numero9 entero y darle un valor
		int numero9 = 230;
		// Mostrar numero9
		System.out.println(numero9);
		// Disminuir su valor con operador --
		numero9--;
		// Mostrar numero9
		System.out.println(numero9);

	}

}
