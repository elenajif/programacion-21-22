package ejerciciosprevios;

public class EjercicioIncrementales1 {

	public static void main(String[] args) {
		int numero1 = 1;
		int numero2 = numero1++;
		// al empezar numero1=1
		// numero1=2 numero2=1
		// primero se ha hecho la operacion numero2=numero1 -> 1
		// segundo se ha hecho la operacion numero1++ -> 2
		System.out.println("numero1 " + numero1);
		System.out.println("numero2 " + numero2);

		int numero3 = 1;
		int numero4 = --numero3;
		// al empezar numero3=1
		// numero3=0 numero4=0
		// primero se ha hecho la operacion --numero3 -> 0
		// segundo se ha hecho la operacion numero4=--numero3 -> 0
		System.out.println("numero3 " + numero3);
		System.out.println("numero4 " + numero4);

		int numero5 = 2;
		int numero6 = ++numero5;
		// al empezar numero5=2
		// numero5=3 numero6=3
		// primero se ha hecho la operacion ++numero5 -> 3
		// segundo se ha hecho la operacion numero6=++numero5 -> 3
		System.out.println("numero5 " + numero5);
		System.out.println("numero6 " + numero6);
		
		int numero7=6;
		int numero8=numero7--;
		//al empezar numero7=6
		//numero7=5 numero8=6
		//primero se ha hecho la operacion numero8=numero7 -> 6
		//segundo se ha hecho la operaacion numero8=numero7-- -> 5
		System.out.println("numero7 " + numero7);
		System.out.println("numero8 " + numero8);
		
		//variable++ variable-- incrementa o decrementa despues
		//++variable --variable incrementa o decrementa antes
	}

}
