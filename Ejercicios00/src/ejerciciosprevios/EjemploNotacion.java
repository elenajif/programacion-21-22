package ejerciciosprevios;

public class EjemploNotacion {

	public static void main(String[] args) {
		// probamos las notaciones

		// declaramos una variable de tipo entero
		// int nombreVariable = valor;

		// recordamos, las variables comienzan por minúscula

		int numero = 3;
		int numeroEntero = 4;

		// mostramos por pantalla
		// syso -> CTRL+ESPACIO+ENTER

		System.out.println(numero);
		System.out.println(numeroEntero);

		// tabular -> CTRL+MAYUS+F

		// declaramos una variable tipo cadena
		// String nombreVariable = "valor";

		String cadena = "hola";
		String otraCadena = "hola caracola";

		// mostramos por pantalla

		System.out.println(cadena);
		System.out.println(otraCadena);

		// declaramos una variable de tipo decimal
		// double nombreVariable = valor;

		double numero1 = 3.25;
		double numeroDecimal = 5.23897;

		// mostramos por pantalla

		System.out.println(numero1);
		System.out.println(numeroDecimal);

		// declaramos una constante
		// final double NOMBREVARIABLE=valor;

		final double PI = 3.1416;

		// mostramos por pantalla

		System.out.println(PI);
	}

}
