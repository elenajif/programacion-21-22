package ejerciciosprevios;

public class EjercicioCasteo {

	public static void main(String[] args) {
		int numero=12;
		System.out.println("numero "+numero); //12
		double numero2=12;
		System.out.println("numero2 "+numero2); //12.0
		int numero3=123456;
		System.out.println("numero3 "+numero3);
		byte numero4=(byte)numero3;
		System.out.println("numero4 "+numero4);
		int numero5=25;
		System.out.println("numero5 "+numero5);
		byte numero6=(byte)numero5;
		System.out.println("numero6 "+numero6);
		int numero7=25252525;
		System.out.println("numero7 "+numero7);
		short numero8=(short)numero7;
		System.out.println("numero8 "+numero8);
		byte numero9=125;
		System.out.println("numero9 "+numero9);
		int numero10=numero9;
		System.out.println("numero10 "+numero10);
		//de cajon peque�o a cajon grande no hay problema
		//de cajon grande a cajon peque�o (cuidado)
		
		char letraA = 65; //caracter ascii A
		System.out.println("letraA "+letraA);
		int valorA=letraA;
		System.out.println("valorA "+valorA);
		int valorB='B';//66 caracter ascii B
		System.out.println("valorB "+valorB);
		char letraB = (char)valorB;
		System.out.println("letraB "+letraB);
		
		

	}

}
