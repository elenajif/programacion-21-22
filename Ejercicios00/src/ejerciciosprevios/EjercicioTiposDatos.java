package ejerciciosprevios;

public class EjercicioTiposDatos {

	// declaro una constante
	static final int MI_CONSTANTE = 25;
	static final double PI = 3.1416;

	public static void main(String[] args) {
		// declaro una variable entera
		int numero1;

		// declaro una variable double
		double numero2;

		// declaro un caracter
		char letra = 'c';

		// declaro un booleano
		boolean miVariable;

		// declaro un String
		String cadena = "hola";
		
		//declaro un float literal
		float miNumero=5.6F;
		float miNumero1=15.1458F;

		// inicializo la variable numero1
		numero1 = 3;
		System.out.println(numero1);

		// inicializo la variable numero2
		numero2 = 3.22;
		System.out.println(numero2);

		// muestro la variable char
		System.out.println(letra);

		// inicializo la variable booleana
		miVariable = true;
		System.out.println(miVariable);

		// muestro el valor del String
		System.out.println(cadena);
		
		//muestro el valor de las constantes
		System.out.println(MI_CONSTANTE);
		System.out.println(PI);
		
		//muestro el valor float
		System.out.println(miNumero);
		System.out.println(miNumero1);
		
		//salto de linea
		System.out.println("Esto es una frase sin salto de linea");
		System.out.println("");
		System.out.println("Esto es otra frase \n con salto de linea");

	}

}
