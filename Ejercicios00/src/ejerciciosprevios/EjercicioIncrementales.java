package ejerciciosprevios;

public class EjercicioIncrementales {

	public static void main(String[] args) {
		int numero1 = 3;
		System.out.println(numero1); // 3
		numero1++; // numero1=numero1+1;
		System.out.println(numero1); // 4
		numero1--; // numero1=numero1-1
		System.out.println(numero1); // 3

	}

}
