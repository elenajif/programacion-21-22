package ejerciciosprevios;

public class EjercicioOperadores {

	public static void main(String[] args) {
		// declaro un variable
		int numero1 = 45;
		System.out.println(numero1); // 45
		// creo una segunda variable usando numero1 y restando un valor
		int numero2 = numero1 - 10;
		System.out.println(numero2); // 35
		// declaro dos variables int
		int x = 3;
		int y = 5;
		// cambio el valor de numero1 restandole 15
		numero1 = numero1 - 15;
		System.out.println(numero1); // 30
		// cambio el valor de numero2 sumando 3
		numero2 = numero2 + 3;
		System.out.println(numero2);// 38

		// operaciones con x e y
		System.out.println("operaciones");
		int suma = x + y;
		System.out.println(suma);
		int resta = x - y;
		System.out.println(resta);
		int multiplica = x * y;
		System.out.println(multiplica);
		double divide = (double) x / (double) y;
		System.out.println(divide);
		int resto = x % y;
		System.out.println(resto);

		System.out.println("Operadores asignacion");
		int puntuacionA = 10;
		int bonus = 5;
		System.out.println(puntuacionA); // 10
		System.out.println(bonus); // 5
		puntuacionA = puntuacionA + bonus;
		System.out.println(puntuacionA); // 15
		puntuacionA = puntuacionA - bonus;
		System.out.println(puntuacionA);// 10
		int extra = 2;
		puntuacionA = puntuacionA * extra;
		System.out.println(puntuacionA);// 20
		int cactus = 2;
		puntuacionA = puntuacionA / cactus;
		System.out.println(puntuacionA);// 10

		System.out.println("Operadores asignacion notacion reducida");
		System.out.println(puntuacionA); // 10
		System.out.println(bonus); // 5
		puntuacionA += bonus;
		// puntuacionA=puntuacionA+bonus;
		System.out.println(puntuacionA); // 15
		puntuacionA -= bonus;
		// puntuacionA=puntuacionA-bonus;
		System.out.println(puntuacionA);// 10
		puntuacionA *= extra;
		// puntuacionA=puntuacionA*extra;
		System.out.println(puntuacionA);// 20
		puntuacionA /= cactus;
		// puntuacionA=puntuacionA/cactus;
		System.out.println(puntuacionA);// 10

	}

}
