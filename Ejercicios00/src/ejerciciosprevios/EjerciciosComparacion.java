package ejerciciosprevios;

public class EjerciciosComparacion {

	public static void main(String[] args) {
		int numero=3;
		//= asignacion -> dar un valor
		//== comparacion -> compara un valor
		System.out.println("Parte1");
		System.out.println(numero==3);//true
		System.out.println(numero>5);//false
		System.out.println(numero>=3);//true
		System.out.println(numero<=6);//true
		System.out.println(numero<2);//false
		System.out.println(numero!=25);//true
		
		int var1=5;
		int var2=6;
		System.out.println("Parte2");
		System.out.println((var1==5)&&(var2==3));//true && false -> false
		System.out.println((var1<=15)&&(var2>=2));//true && true -> true
		System.out.println((var1<3)||(var2>4));//false || true -> true
		System.out.println((var1!=4)||(var2>25));//true || false -> true

	}

}
