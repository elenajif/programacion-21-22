package ejerciciosprevios;

public class Clase1 {

	public static void main(String[] args) {
		// ejercicio de prueba para mostrar el funcionamiento de
		// print y println
		// syso -> CTRL+ESPACIO -> intro
		System.out.println("Mensaje usando println");
		System.out.print("Mensaje usando print");
		System.out.print("Segundo mensaje usando print (no salta linea)");
		
	}

}
