package ejerciciosprevios;

public class EstoEsUnaClase {
	
	double PI;
	double NUMERO_PI;
	
	//constantes todo en may�sculas y barra baja si tienen m�s de una palabra

	//metodo main
	public static void main(String[] args) {
		// notacion Upper Camel Case

	}
	
	//metodo llamado suma
	public void suma() {
		// notacion lower Camel Case
		//variable llamada numero
		int numero=0;
		//variable llamada numeroGrande
		int numeroGrande=8;
		System.out.println(numero);
		System.out.println(numeroGrande);
	}
	
	//metodo llamado sumaNumeros
	public void sumaNumeros() {
		//notacion lower Camel Case
	}

}
