package ejerciciosprevios;

public class EjerciciosCondicional {

	public static void main(String[] args) {
		int num1=56;
		int num2=-63;
		System.out.println(num1);
		System.out.println(num2);
		System.out.println(num2>num1); //false
		System.out.println(num2>num1?"num1 es menor":"num1 es mayor");
		num1=3;
		System.out.println(num1>5?"num1 es mayor de 5":"num1 es menor o igual a 5");

	}

}
