package ejercicio06;

public class Ejercicio06 {

	public static void main(String[] args) {
		// 1 euro = 0.91 libras esterlinas
		// 1 euro = 1.17 d�lares
		double euros=120.25;
		double libras;
		double dolares;

		System.out.println("Euros "+euros);

		//transformo a libras
		libras=0.91*euros;
		System.out.println("Libras "+libras);
		//transformo a dolares
		dolares=1.17*euros;
		System.out.println("Dolares "+dolares);

		//mas corto pero sin guardar el valor para usarlo a posteriori
		System.out.println("Libras "+euros*0.91+" Dolares "+euros*1.17);
	}

}
