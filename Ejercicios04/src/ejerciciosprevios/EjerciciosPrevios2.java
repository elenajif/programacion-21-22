package ejerciciosprevios;

import java.util.Scanner;

public class EjerciciosPrevios2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Dame tu nombre");
		String nombre = input.nextLine();

		switch (nombre) {
		case "Elena":
			System.out.println("Hola Elena");
			break;
		case "Maria":
			System.out.println("Hola Maria");
			break;
		case "Raquel":
			System.out.println("Hola Raquel");
			break;
		default:
			System.out.println("No te conozco");

		}

		input.close();
	}

}
