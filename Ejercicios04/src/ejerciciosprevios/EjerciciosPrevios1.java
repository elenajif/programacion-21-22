package ejerciciosprevios;

import java.util.Scanner;

public class EjerciciosPrevios1 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Dame un numero");
		int numero = input.nextInt();

		switch (numero) {
		case 1:
			System.out.println("El valor de numero es 1");
			break;
		case 2:
			System.out.println("El valor de numero es 2");
			break;
		case 3:
			System.out.println("El valor de numero es 3");
			break;
		default:
			System.out.println("El valor elegido no es ni 1 ni 2 ni 3");
		}

		input.close();

	}

}
