package ejerciciosprevios;

import java.util.Scanner;

public class EjerciciosPrevios3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Dame tu nombre");
		String nombre = input.nextLine();

		switch (nombre) {
		case "Elena":
		case "Maria":
		case "Raquel":
			System.out.println("Hola Bienvenido");
			break;
		default:
			System.out.println("No te conozco");

		}

		input.close();
	}

}
