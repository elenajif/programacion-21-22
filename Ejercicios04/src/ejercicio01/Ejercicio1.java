package ejercicio01;

import java.util.Scanner;

public class Ejercicio1 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce una letra");
		char letra =input.nextLine().charAt(0);
		
		switch (letra) {
		case 'a':
			System.out.println("es la vocal a");
			break;
		case 'e':
			System.out.println("es la vocal e");
			break;
		case 'i':
			System.out.println("es la vocal i");
			break;
		case 'o':
			System.out.println("es la vocal o");
			break;
		case 'u':
			System.out.println("es la vocal u");
			break;
		default:
			System.out.println("no es una vocal");
		}
		
		input.close();

	}

}
