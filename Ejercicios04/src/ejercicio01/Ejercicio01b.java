package ejercicio01;

import java.util.Scanner;

public class Ejercicio01b {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce una letra");
		char letra = input.nextLine().charAt(0);

		switch (letra) {
		case 'a':
			System.out.println("es la vocal a");
		case 'e':
			System.out.println("es la vocal e");
		case 'i':
			System.out.println("es la vocal i");
		case 'o':
			System.out.println("es la vocal o");
		case 'u':
			System.out.println("es la vocal u");
		default:
			System.out.println("no es una vocal");
		}
		
		System.out.println("Cuando entra en un switch sin break");
		System.out.println("muestra todo lo que est� por debajo de la opci�n elegida");
		System.out.println("cuidado");

		input.close();

	}

}
