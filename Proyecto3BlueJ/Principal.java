
public class Principal
{
    public static void main(String[] args)
    {
        //metodo no static llamado calculaSuma
        //esta en la clase Suma
        Suma sumita = new Suma();
        sumita.calculaSuma();
        //metodo no static llamado calculaSuma(x,y)
        //esta en la clase Suma2
        Suma2 sumita2 = new Suma2();
        sumita2.calculaSuma(3,4);
        //metodo no static llamado int calculaSuma(x,y)
        //esta en la clase Suma3
        Suma3 sumita3 = new Suma3();
        int suma=sumita3.calculaSuma(3,4);
        System.out.println("La suma es "+suma);
    }
}
