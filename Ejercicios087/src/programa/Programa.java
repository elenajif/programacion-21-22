package programa;

import clases.Fruteria;
import clases.Proveedor;

public class Programa {

	public static void main(String[] args) {
		System.out.println("Creamos instancia fruteria 4 frutas y 3 verduras");
		int maxFrutas=4;
		int maxVerduras=3;
		Fruteria miFruteria= new Fruteria(maxFrutas, maxVerduras);
		System.out.println("Instancia creada");
		System.out.println("Creamos 2 proveedores");
		Proveedor proveedor1 = new Proveedor("Proveedor fruta");
		System.out.println("Asigno telefono");
		proveedor1.setTelefonoContacto("976111111");
		Proveedor proveedor2=new Proveedor("Proveedor verdura");
		System.out.println("Asigno telefono");
		proveedor2.setTelefonoContacto("976222222");
		
		System.out.println("Damos de alta 4 frutas y 3 verduras");
		miFruteria.altaFruta("NNNN", "Naranja", 1.55, "Fruteria1",proveedor1);
		miFruteria.altaFruta("CCCC", "Manzana", 1.20, "Fruteria1",proveedor1);
		miFruteria.altaFruta("FFFF", "Pera", 0.75, "Fruteria1",proveedor1);
		miFruteria.altaFruta("RRRR", "Melocoton", 1.75, "Fruteria2",proveedor1);
		miFruteria.altaVerdura("BBBB", "Brocoli", 1.55, "Fruteria1",proveedor2);
		miFruteria.altaVerdura("AAAA", "Alcachofas", 1.45, "Fruteria1",proveedor2);
		miFruteria.altaVerdura("BRBR", "Borraja", 0.75, "Fruteria1",proveedor2);
		
		System.out.println("Listamos frutas");
		miFruteria.listarFrutas();
		System.out.println("Listamos verduras");
		miFruteria.listarVerduras();
		
		System.out.println("Buscamos fruta por codigo NNNN");
		System.out.println(miFruteria.buscarFruta("NNNN"));
		System.out.println("Buscamos verdura por codigo AAAA");
		System.out.println(miFruteria.buscarVerdura("AAAA"));
		
		System.out.println("Eliminamos la fruta CCCC");
		miFruteria.eliminarFruta("CCCC");
		miFruteria.listarFrutas();
		System.out.println("Eliminamos la verdura BRBR");
		miFruteria.eliminarVerdura("BRBR");
		miFruteria.listarVerduras();
		
		System.out.println("Almacenar nueva fruta");
		miFruteria.altaFruta("WWWW", "Kiwi", 1.33, "Fruteria3",proveedor1);
		System.out.println("Almacenar nueva verdura");
		miFruteria.altaVerdura("PPPP", "Puerros", 1.12, "Fruteria3",proveedor2);
		
		System.out.println("Modificamos fruta WWWW a Pera");
		miFruteria.cambiarTipoFruta("WWWW", "Pera");
		System.out.println("Modificamos verdura PPPP a Pimiento");
		miFruteria.cambiarTipoVerdura("PPPP", "Pimiento");
		
		System.out.println("Listamos frutas por fruteria");
		miFruteria.listarFrutasPorFruteria("Fruteria1");
		System.out.println("Listamos verduras por fruteria");
		miFruteria.listarVerdurasPorFruteria("Fruteria1");
		
		System.out.println("Listamos frutas por proveedor1");
		miFruteria.listarFrutasPorProveedor(proveedor1);
		System.out.println("Listamos verduras por proveedor2");
		miFruteria.listarVerdurasPorProveedor(proveedor2);

	}

}
