package practica1;

import java.util.Scanner;

public class Practica1 {

	// constantes en mayusculas y separadas con guion bajo
	// static final tipodatos
	static final String CODIGO_IMPORTE = "FTDY";
	static final String CODIGO_TRANSPORTE = "HTRG";
	static final double DESCUENTO_IMPORTE = 0.15;
	static final double DESCUENTO_TRANSPORTE = 4.95;

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		// variable para el menu
		int opcion = 0;
		do {
			System.out.println("*******************************");
			System.out.println("1.- Calcular importe compra");
			System.out.println("2.- Ordena jugadores");
			System.out.println("3.- Frecuencia vocales");
			System.out.println("4.- Dinero en cuenta");
			System.out.println("5.- Salir");
			System.out.println("*******************************");
			System.out.println("Introduce opci�n");

			opcion = input.nextInt(); // leer opcion
			input.nextLine(); // limpiar buffer

			switch (opcion) {
			case 1:
				double compra = 0;
				String descuento = "";
				System.out.println("Introduce el total de la compra");
				compra = input.nextDouble();
				// limpiar buffer
				input.nextLine();
				System.out.println("Introduce el descuento");
				descuento = input.nextLine();

				if (descuento.equals(CODIGO_IMPORTE)) {
					System.out.println("El descuento es " + descuento);
					System.out.println("El total con descuento por importe es " + compra * DESCUENTO_IMPORTE);
				} else if (descuento.equals(CODIGO_TRANSPORTE)) {
					System.out.println("El descuento es " + descuento);
					System.out.println("El total con descuento por importe es " + compra * DESCUENTO_TRANSPORTE);
				}

				break;
			case 2:
				//jugador1
				System.out.println("Introduzca el NOMBRE del primer jugador");
				String nombrePersona1=input.nextLine();
				System.out.println("Introduzca el APODO del primer jugador");
				String apodoPersona1=input.nextLine();
				System.out.println("Introduzca la puntuaci�n del primer jugador");
				int puntuaPersona1=input.nextInt();
				input.nextLine();
				
				//jugador2
				System.out.println("Introduzca el NOMBRE del segundo jugador");
				String nombrePersona2=input.nextLine();
				System.out.println("Introduzca el APODO del segundo jugador");
				String apodoPersona2=input.nextLine();
				System.out.println("Introduzca la puntuaci�n del segundo jugador");
				int puntuaPersona2=input.nextInt();
				input.nextLine();
				
				//jugador3
				System.out.println("Introduzca el NOMBRE del tercer jugador");
				String nombrePersona3=input.nextLine();
				System.out.println("Introduzca el APODO del tercer jugador");
				String apodoPersona3=input.nextLine();
				System.out.println("Introduzca la puntuaci�n del tercer jugador");
				int puntuaPersona3=input.nextInt();

				//creo los nombres completos
				String completo1=apodoPersona1.toUpperCase()+ " - "+nombrePersona1+ " ("+puntuaPersona1+")";
				String completo2=apodoPersona2.toUpperCase()+ " - "+nombrePersona2+ " ("+puntuaPersona2+")";
				String completo3=apodoPersona3.toUpperCase()+ " - "+nombrePersona3+ " ("+puntuaPersona3+")";
				
				boolean unoMenorQueDos=completo1.compareToIgnoreCase(completo2)<=0;
				boolean unoMenorQueTres=completo1.compareToIgnoreCase(completo3)<=0;
				boolean dosMenorQueUno=completo2.compareToIgnoreCase(completo1)<=0;
				boolean dosMenorQueTres=completo2.compareToIgnoreCase(completo3)<=0;
				boolean tresMenorQueUno=completo3.compareToIgnoreCase(completo1)<=0;
				boolean tresMenorQueDos=completo3.compareToIgnoreCase(completo2)<=0;
				
				if (unoMenorQueDos && dosMenorQueTres) { //1<2<3
					System.out.println(completo1);
					System.out.println(completo2);
					System.out.println(completo3);
				} else if (unoMenorQueTres && tresMenorQueDos) { //1<3<2
					System.out.println(completo1);
					System.out.println(completo3);
					System.out.println(completo2);
				} else if (dosMenorQueTres && tresMenorQueUno) { //2<3<1
					System.out.println(completo2);
					System.out.println(completo3);
					System.out.println(completo1);				
				} else if (dosMenorQueUno && unoMenorQueTres) { //2<1<3
					System.out.println(completo2);
					System.out.println(completo1);
					System.out.println(completo3);
				} else if (tresMenorQueUno && unoMenorQueDos) { //3<1<2
					System.out.println(completo3);
					System.out.println(completo1);
					System.out.println(completo2);
				} else if (tresMenorQueDos && dosMenorQueUno) { //3<2<1
					System.out.println(completo3);
					System.out.println(completo2);
					System.out.println(completo1);
				}
				break;

			case 3:
				int contadorA = 0;
				int contadorE = 0;
				int contadorI = 0;
				int contadorO = 0;
				int contadorU = 0;
				String texto;
				String muestraA = "";
				String muestraE = "";
				String muestraI = "";
				String muestraO = "";
				String muestraU = "";

				System.out.println("Dame el texto a evaluar");
				texto = input.nextLine();
				texto = texto.toLowerCase();

				for (int i = 0; i < texto.length(); i++) {
					if (texto.charAt(i) == 'a') {
						contadorA++;
						muestraA += "*";
					}
					if (texto.charAt(i) == 'e') {
						contadorE++;
						muestraE += "*";
					}
					if (texto.charAt(i) == 'i') {
						contadorI++;
						muestraI += "*";
					}
					if (texto.charAt(i) == 'o') {
						contadorO++;
						muestraO += "*";
					}
					if (texto.charAt(i) == 'u') {
						contadorU++;
						muestraU += "*";
					}
				}
				System.out.println("");
				System.out.println("a   " + contadorA + "    " + muestraA);
				System.out.println("e   " + contadorE + "    " + muestraE);
				System.out.println("i   " + contadorI + "    " + muestraI);
				System.out.println("o   " + contadorO + "    " + muestraO);
				System.out.println("u   " + contadorU + "    " + muestraU);
				break;
			case 4:
				System.out.println("Dame el saldo de la cuenta");
				double saldo = input.nextDouble();
				double saldoActual = saldo;
				double cantidad;
				do {
					System.out.println("Realiza ingreso (positivo), reintegro (negativo)");
					cantidad = input.nextDouble();
					saldoActual = saldoActual + cantidad;
					System.out.println("Queda " + (saldoActual) + " �");
				} while (saldoActual >= 0);
				System.out.println("No queda saldo");
				break;
			case 5:
				input.close();
				System.out.println("Programa terminado");
				break;
			default:
				System.out.println("La opci�n no es correcta");
			}

		} while (opcion != 5); // fin del while

	}

}
