package ejerciciointerface;

public interface InstrumentoMusical {
	void tocar();

	void afinar();

	String tipoInstrumento();
}
