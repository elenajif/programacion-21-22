package ejercicioprevio01;

import java.time.LocalDate;

public class Persona {

	/**
	 * @author Elena
	 */
	private String nombre;
	private String apellidos;
	private LocalDate fechaNacimiento;
	private boolean esHombre;
	private double altura;
	private double peso;
	
	/**
	 * Constructor con parametros nombre, apellidos
	 * @param nombre
	 * @param apellidos
	 */

	public Persona(String nombre, String apellidos) {
		this.nombre = nombre;
		this.apellidos = apellidos;
	}

	/**
	 * Constructor con parametros nombre, apellidos, fechaNacimiento, esHombre
	 * @param nombre
	 * @param apellidos
	 * @param fechaNacimiento
	 * @param esHombre
	 */
	public Persona(String nombre, String apellidos, String fechaNacimiento, boolean esHombre) {
		this(nombre, apellidos);
		this.fechaNacimiento = LocalDate.parse(fechaNacimiento);
		this.esHombre = esHombre;
	}

	public Persona(String nombre, String apellidos, String fechaNacimiento, boolean esHombre, double altura,
			double peso) {
		this(nombre, apellidos, fechaNacimiento, esHombre);
		this.peso = peso;
		this.altura = altura;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public boolean isEsHombre() {
		return esHombre;
	}

	public void setEsHombre(boolean esHombre) {
		this.esHombre = esHombre;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}
	
	// this.atributo accede a un atributo de esta clase
	// this.metodo() accede a un metodo de esta clase
	// this() accede a un constructor de esta clase

}
