package ejercicio01;

public class Vehiculo {

	// atributo que me va a permitir contar vehiculos creados
	// me interesa que sea static, accesible y global
	private static int vehiculosCreados;

	// atributos private
	private String tipo;
	private String marca;
	private float consumo;
	private float kmTotales;
	private int numRuedas;
	private boolean funciona;

	// CONSTRUCTORES SOBRECARGADOS

	// constructor sin parametros
	public Vehiculo() {
		vehiculosCreados++;
	}

	// constructor con parametros1
	public Vehiculo(String tipo, String marca) {
		this.tipo = tipo;
		this.marca = marca;
		kmTotales = 0;
		funciona = true;
		vehiculosCreados++;
	}

	// constructor con parametros2
	public Vehiculo(String tipo, String marca, float consumo, int numRuedas) {
		this.tipo = tipo;
		this.marca = marca;
		this.consumo = consumo;
		this.numRuedas = numRuedas;
		kmTotales = 0;
		funciona = true;
		vehiculosCreados++;
	}

	// setter y getter (no se creara setter para kmTotales)
	public static int getVehiculosCreados() {
		return vehiculosCreados;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public float getConsumo() {
		return consumo;
	}

	public void setConsumo(float consumo) {
		this.consumo = consumo;
	}

	public float getKmTotales() {
		return kmTotales;
	}

	public int getNumRuedas() {
		return numRuedas;
	}

	public void setNumRuedas(int numRuedas) {
		this.numRuedas = numRuedas;
	}

	public boolean isFunciona() {
		return funciona;
	}

	public void setFunciona(boolean funciona) {
		this.funciona = funciona;
	}

	// METODOS SOBRECARGADOS

	// metodo float combustibleConsumido()
	public float combustibleConsumido() {
		return (this.consumo * this.kmTotales) / 100;
	}

	// metodo float combustibleConsumido(float kmTotales)
	public float combustibleConsumido(float kmTotales) {
		return (this.consumo * kmTotales) / 100;
	}

	// metodo trucarCuentaKM()
	public void trucarCuentaKM() {
		this.kmTotales = 0;
	}

	// metodo trucarCuentaKm(float km)
	public void trucarCuentaKM(float km) {
		this.kmTotales=km;
	}
}
