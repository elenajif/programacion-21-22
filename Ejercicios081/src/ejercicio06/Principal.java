package ejercicio06;

public class Principal {

	public static void main(String[] args) {
		// sin lectura por teclado
		// Crear persona con constructor con dos parametros
		Persona personita1 = new Persona("Jaimito", "Borromeo");
		// mostrar datos
		System.out.println("Persona constructor 2 parametros");
		System.out.println("nombre "+personita1.getNombre());
		System.out.println("apellidos "+personita1.getApellidos());

		// Crear persona con constructor con cuatro parametros
		Persona personita2 = new Persona("Rafa","Nadal",("1986-06-03"),true);
		// mostrar datos
		System.out.println("Persona constructor 4 parametros");
		System.out.println("nombre "+personita2.getNombre());
		System.out.println("apellidos "+personita2.getApellidos());
		System.out.println("fecha nacimiento "+personita2.getFechaNacimiento());
		System.out.println("es hombre? "+personita2.isEsHombre());

		// Crear persona con constructor con seis parametros
		Persona personita3 = new Persona("Pantera","Rosa",("1963-12-18"),false,1.56,65);
		// mostrar datos
		System.out.println("Persona constructor 4 parametros");
		System.out.println("nombre "+personita3.getNombre());
		System.out.println("apellidos "+personita3.getApellidos());
		System.out.println("fecha nacimiento "+personita3.getFechaNacimiento());
		System.out.println("es hombre? "+personita3.isEsHombre());
		System.out.println("altura "+personita3.getAltura());
		System.out.println("peso "+personita3.getPeso());

	}

}
