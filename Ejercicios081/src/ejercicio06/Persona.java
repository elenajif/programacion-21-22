package ejercicio06;

import java.time.LocalDate;

public class Persona {

	// atributos
	private String nombre;
	private String apellidos;
	// fecha -> LocalDate (tipo dato)
	private LocalDate fechaNacimiento;
	private boolean esHombre;
	private double altura;
	private double peso;

	// constructor dos parametros
	public Persona(String nombre, String apellidos) {
		this.nombre = nombre;
		this.apellidos = apellidos;
	}

	// constructor cuatro parametros
	public Persona(String nombre, String apellidos, String fechaNacimiento, boolean esHombre) {
		// llamo al constructor de la clase Persona con dos parametros
		// nombre y apellidos
		this(nombre, apellidos);
		// recibo un parametro String
		// pero lo tengo que guardar en un LocalDate
		// tengo que parsearlo
		this.fechaNacimiento = LocalDate.parse(fechaNacimiento);
		// de este modo, recibo un String y lo guardo en un LocalDate
		this.esHombre = esHombre;
	}

	// constructor seis parametros
	public Persona(String nombre, String apellidos, String fechaNacimiento, boolean esHombre, double altura,
			double peso) {
		// llamo al constructor de la clase Persona con cuatro parametros
		// nombre, apellidos, fechaNacimiento, esHombre
		this(nombre, apellidos, fechaNacimiento, esHombre);
		this.peso = peso;
		this.altura = altura;
	}
	
	//setter y getter
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public boolean isEsHombre() {
		return esHombre;
	}

	public void setEsHombre(boolean esHombre) {
		this.esHombre = esHombre;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}
	
	// this.atributo accede a un atributo de esta clase
	// this.metodo() accede a un metodo de esta clase
	// this() accede a un constructor de esta clase

}
