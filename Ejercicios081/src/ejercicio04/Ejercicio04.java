package ejercicio04;

import java.util.Scanner;

import ejercicio01.Vehiculo;
import ejercicio02.Ejercicio02;

public class Ejercicio04 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		// creo un vector de vehiculos
		Vehiculo[] listaVehiculos = new Vehiculo[3];

		for (int i = 0; i < listaVehiculos.length; i++) {
			System.out.println("Dame la marca");
			String marca = input.nextLine();
			System.out.println("Dame el tipo");
			String tipo = input.nextLine();
			System.out.println("Dame el consumo");
			float consumo = input.nextFloat();
			System.out.println("Introduce el numero de ruedas");
			int numRuedas = input.nextInt();
			input.nextLine();
			
			listaVehiculos[i]=new Vehiculo(tipo, marca, consumo, numRuedas);
			
		}
		input.close();
		
		for (int i=0;i<listaVehiculos.length;i++) {
			Ejercicio02.mostrarDatosVehiculo(listaVehiculos[i]);
			System.out.println(listaVehiculos[i].getVehiculosCreados());
		}
	}

}
