package ejercicio02;

import java.util.Scanner;

import ejercicio01.Vehiculo;

public class Ejercicio02 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		// pedir datos (4 parametros)
		System.out.println("Datos del 1 vehiculo");
		System.out.println("Dame la marca");
		String marca = input.nextLine();
		System.out.println("Dame el tipo");
		String tipo = input.nextLine();
		System.out.println("Dame el consumo");
		float consumo = input.nextFloat();
		System.out.println("Introduce el numero de ruedas");
		int numRuedas = input.nextInt();
		input.nextLine();

		// creo vehiculo con constructor con 4 parametros
		Vehiculo vehiculo1 = new Vehiculo(tipo, marca, consumo, numRuedas);

		// pedir datos (2 parametros)
		System.out.println("Datos del 2 vehiculo");
		System.out.println("Dame la marca");
		marca = input.nextLine();
		System.out.println("Dame el tipo");
		tipo = input.nextLine();

		// creo vehiculo con constructor con 2 parametros
		Vehiculo vehiculo2 = new Vehiculo(tipo, marca);

		System.out.println("Datos del 3 vehiculo");
		
		// creo vehiculo con constructor sin parametros
		// al no tener parametros tendre que asignarlos usando un set
		Vehiculo vehiculo3 = new Vehiculo();
		
		//pido datos y usando los setter le doy valores
		//primero pido el dato
		//usando set lo asigno a vehiculo3
		System.out.println("Dame la marca");
		marca = input.nextLine();
		vehiculo3.setMarca(marca);
		System.out.println("Dame el tipo");
		tipo = input.nextLine();
		vehiculo3.setTipo(tipo);
		System.out.println("Dame el consumo");
		consumo = input.nextFloat();
		vehiculo3.setConsumo(consumo);
		System.out.println("Introduce el numero de ruedas");
		numRuedas = input.nextInt();
		vehiculo3.setNumRuedas(numRuedas);
		
		mostrarDatosVehiculo(vehiculo1);
		mostrarDatosVehiculo(vehiculo2);
		mostrarDatosVehiculo(vehiculo3);
		
		System.out.println("Cambio los km del vehiculo1");
		vehiculo1.trucarCuentaKM(57000);
		System.out.println("Estimo el combustible consumido en el vehiculo1");
		float estimacion =vehiculo1.combustibleConsumido();
		System.out.println(estimacion);
		System.out.println("Reseteo el cuentakm del vehiculo1");
		vehiculo1.trucarCuentaKM();
		System.out.println("Estimo el combustible consumido en el vehiculo1 para 40km");
		estimacion = vehiculo1.combustibleConsumido(40);
		System.out.println(estimacion);
		
		input.close();
	}
	
	public static void mostrarDatosVehiculo(Vehiculo vehiculo) {
		System.out.println();
		System.out.println("Tipo "+vehiculo.getTipo());
		System.out.println("Marca "+vehiculo.getMarca());
		System.out.println("Consumo "+vehiculo.getConsumo());
		System.out.println("Numero ruedas "+vehiculo.getNumRuedas());
		System.out.println("Funciona "+vehiculo.isFunciona());
		System.out.println("Km "+vehiculo.getKmTotales());
		System.out.println();
	}

}
