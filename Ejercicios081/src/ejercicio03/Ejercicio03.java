package ejercicio03;

import ejercicio01.Vehiculo;

public class Ejercicio03 {

	public static void main(String[] args) {
		System.out.println("Creo vehiculo1");
		// crear vehiculo con constructor sin parametros
		Vehiculo vehiculo1 = new Vehiculo();
		//mostrar Vehiculos creados (vehiculo1)
		System.out.println(vehiculo1.getVehiculosCreados());

		System.out.println("Creo vehiculo2");
		//crear vehiculo con constructor con 2 parametros
		Vehiculo vehiculo2 = new Vehiculo("moto","yamaha");
		//mostrar Vehiculos creados (vehiculo1, vehiculo2)
		System.out.println(vehiculo1.getVehiculosCreados());
		System.out.println(vehiculo2.getVehiculosCreados());
		
		System.out.println("Creo vehiculo3");
		//crear vehiculo con constructor con 4 parametros
		Vehiculo vehiculo3=new Vehiculo("coche","audi",5.6F,4);
		//mostrar Vehiculos creados (vehiculo1, vehiculo2, vehiculo3)
		System.out.println(vehiculo1.getVehiculosCreados());
		System.out.println(vehiculo2.getVehiculosCreados());
		System.out.println(vehiculo3.getVehiculosCreados());
				
		

	}

}
