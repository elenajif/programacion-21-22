package programa;

import clases.Fruteria;

public class Programa {

	public static void main(String[] args) {
		System.out.println("Creamos instancia fruteria 4 frutas y 3 verduras");
		int maxFrutas=4;
		int maxVerduras=3;
		Fruteria miFruteria= new Fruteria(maxFrutas, maxVerduras);
		System.out.println("Instancia creada");
		
		System.out.println("Damos de alta 4 frutas y 3 verduras");
		miFruteria.altaFruta("NNNN", "Naranja", 1.55, "Fruteria1");
		miFruteria.altaFruta("CCCC", "Manzana", 1.20, "Fruteria1");
		miFruteria.altaFruta("FFFF", "Pera", 0.75, "Fruteria1");
		miFruteria.altaFruta("RRRR", "Melocoton", 1.75, "Fruteria2");
		miFruteria.altaVerdura("BBBB", "Brocoli", 1.55, "Fruteria1");
		miFruteria.altaVerdura("AAAA", "Alcachofas", 1.45, "Fruteria1");
		miFruteria.altaVerdura("BRBR", "Borraja", 0.75, "Fruteria1");
		
		System.out.println("Listamos frutas");
		miFruteria.listarFrutas();
		System.out.println("Listamos verduras");
		miFruteria.listarVerduras();
		
		System.out.println("Buscamos fruta por codigo NNNN");
		System.out.println(miFruteria.buscarFruta("NNNN"));
		System.out.println("Buscamos verdura por codigo AAAA");
		System.out.println(miFruteria.buscarVerdura("AAAA"));
		
		System.out.println("Eliminamos la fruta CCCC");
		miFruteria.eliminarFruta("CCCC");
		miFruteria.listarFrutas();
		System.out.println("Eliminamos la verdura BRBR");
		miFruteria.eliminarVerdura("BRBR");
		miFruteria.listarVerduras();
		
		System.out.println("Almacenar nueva fruta");
		miFruteria.altaFruta("WWWW", "Kiwi", 1.33, "Fruteria3");
		System.out.println("Almacenar nueva verdura");
		miFruteria.altaVerdura("PPPP", "Puerros", 1.12, "Fruteria3");
		
		System.out.println("Modificamos fruta WWWW a Pera");
		miFruteria.cambiarTipoFruta("WWWW", "Pera");
		System.out.println("Modificamos verdura PPPP a Pimiento");
		miFruteria.cambiarTipoVerdura("PPPP", "Pimiento");
		
		System.out.println("Listamos frutas por fruteria");
		miFruteria.listarFrutasPorFruteria("Fruteria1");
		System.out.println("Listamos verduras por fruteria");
		miFruteria.listarVerdurasPorFruteria("Fruteria1");

	}

}
