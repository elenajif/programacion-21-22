package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("1.- Area");
		System.out.println("2.- Volumen");
		System.out.println("3.- Salir");

		int opcion = input.nextInt();

		if (opcion>=1 && opcion<=3) {
			switch (opcion) {
			case 1:
				System.out.println("Introduce el radio");
				double radio = input.nextDouble();
				if (radio < 0) {
					System.out.println("El radio es negativo");
				} else {
					System.out.println("El area es " + (Math.PI * radio * radio));
				}
				break;
			case 2:
				System.out.println("Introduce el radio");
				radio=input.nextDouble();
				if (radio<0) {
					System.out.println("El radio es negativo");
				} else {
					System.out.println("El volumen es "+(4*Math.PI*radio*radio*radio));
				}
				break;
			case 3:
				input.close();
				System.out.println("Programa terminado");
			}
		} else {
			System.out.println("La opci�n elegida no es valida");
		}

	}

}
