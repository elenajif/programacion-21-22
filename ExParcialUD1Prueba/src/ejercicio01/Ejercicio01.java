package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);

		int n1;
		int n2;
		int n3;

		System.out.println("Dame un numero");
		n1 = entrada.nextInt();
		System.out.println("Dame otro numero");
		n2 = entrada.nextInt();
		System.out.println("Dame otro numero");
		n3 = entrada.nextInt();

		if ((n1 == 10) && (n2 == 10) && (n3 == 10)) {
			System.out.println("Matricula de honor");
		} else if ((n1 >= 9) && (n2 >= 9) && (n3 >= 9)) {
			System.out.println("Beca");
		} else if ((n1 >= 8) && (n2 >= 8) && (n3 >= 8)) {
			System.out.println("Beca parcial");
		} else {
			System.out.println("Tu nota es inferior a 8, no tienes beca");
		}
		entrada.close();

	}

}
