package clases;

public class Persona {
	String nombre;
	String apellidos;
	int edad;
	String ciclo;
	
	public Persona(String nombre, String apellidos, int edad) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.edad = edad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getCiclo() {
		return ciclo;
	}

	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Persona) {
			Persona tmpPersona = (Persona) obj;
			if (this.nombre.equals(tmpPersona.nombre) 
					&& this.apellidos.equals(tmpPersona.apellidos)
					&& this.edad==tmpPersona.edad) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	
	
}
