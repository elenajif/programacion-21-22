package ejerciciosexcepciones;

public class Ejercicio4ConExcepciones {

	public static void main(String[] args) {
		try {
			int n = Integer.parseInt("M");
			System.out.println(n);
		} catch (NumberFormatException e) {
			System.out.println("Las cadenas no pueden convertirse a int");
		} finally {
			System.out.println("Esto se ejecuta siempre");
		}
	}

}
