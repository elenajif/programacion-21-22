package ejerciciosexcepciones;

public class Ejercicio9ConExcepciones {

	//excepcion creada por el programador
	//permite lanzar tu propia excepci�n y personalizar el mensaje
	
	public static void main(String[] args) {
		try {
			comprobarDiaMes(35);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static void comprobarDiaMes(int dia) throws Exception {
		if (dia > 31 || dia < 1) {
			Exception excepcion = new Exception("Numero de dia fuera de rango");
			throw excepcion;
		}
	}

}
