package ejerciciosexcepciones;

public class Ejercicio1SinExcepciones {

	public static void main(String[] args) {
		// todas las excepciones, heredan de Exception
		// checked
		// obligatorio controlarlas
		// ClassNotFoundExcepcion, IOException, InterruptedException ...

		// unchecked
		// no es obligatorio controlaras
		// NumberFormarException, NullPointerException ...

		int i;
		int valor;
		i = 3;
		valor = i / 0;
		System.out.println(valor);
	}

}
