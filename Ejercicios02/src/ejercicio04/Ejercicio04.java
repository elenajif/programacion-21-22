package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {

	public static void main(String[] args) {

		Scanner lector = new Scanner(System.in);

		System.out.println("Introduce el radio de la circunferencia");
		double radio=lector.nextDouble();
		
		double area=radio*radio*3.1416;
		double longitud=2*31416*radio;
		
		System.out.println("Longitud de la circunferencia "+longitud);
		System.out.println("Area de la circunferencia "+area);
		
		lector.close();
	}

}
