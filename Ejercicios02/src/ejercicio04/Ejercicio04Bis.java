package ejercicio04;

import java.util.Scanner;

public class Ejercicio04Bis {

	public static void main(String[] args) {

		Scanner lector = new Scanner(System.in);

		System.out.println("Introduce el radio de la circunferencia");
		double radio=lector.nextDouble();
		
		//Math.PI contiene el valor de PI
		double area=radio*radio*Math.PI;
		double longitud=2*Math.PI*radio;
		
		System.out.println("Longitud de la circunferencia "+longitud);
		System.out.println("Area de la circunferencia "+area);
		
		lector.close();
	}

}
