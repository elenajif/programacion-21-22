package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {

	public static void main(String[] args) {
		// creo objeto scanner
		Scanner escaner = new Scanner(System.in);
		
		//pido el dia indicando que es un numero
		System.out.println("Introduce el d�a (numero)");
		//creo la variable dia y la leo por teclado
		int dia=escaner.nextInt();
		
		//limpio el buffer (paso de numero a String)
		escaner.nextLine();
		
		//pido el mes indicando que es un texto
		System.out.println("Introduce el mes (texto)");
		//creo variable mes y la leo por teclado
		String mes=escaner.nextLine();
		
		//pido el a�o indicando que es un numero
		System.out.println("Introduce el a�o (numero)");
		//creo variable a�o y la leo por teclad
		int anno=escaner.nextInt();
		
		//muestro con la / de fechas
		System.out.println(dia+"/"+mes+"/"+anno);
		
		//cierro escaner
		escaner.close();
		
		
		

	}

}
