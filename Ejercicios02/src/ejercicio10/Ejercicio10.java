package ejercicio10;

import java.util.Scanner;

public class Ejercicio10 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce dia");
		int dia = input.nextInt();
		System.out.println("Introduce mes");
		int mes = input.nextInt();
		System.out.println("Introduce anno");
		int anno = input.nextInt();

		int suma = dia + mes + anno;
		int sumaTotal = 0;

		// saco el valor de cada cifra por separado
		sumaTotal = sumaTotal + (suma / 1000);
		sumaTotal = sumaTotal + (suma % 10);
		sumaTotal = sumaTotal + ((suma / 100) % 10);
		sumaTotal = sumaTotal + ((suma % 100) / 10);

		System.out.println("El numero de la suerte es " + sumaTotal);

		// muestro cifras (no es necesario) solo para comprobar
		System.out.println(suma / 1000);
		System.out.println(suma % 10);
		System.out.println((suma / 100) % 10);
		System.out.println((suma % 100) / 10);

		input.close();

	}

}
