public class Manzana extends Fruta
{
    public Manzana(){
        this.nombre="Manzana";
        this.caracteristicas="Piel comestible";
    }
    public void imprimirInfo(Manzana mn) {
        System.out.println("Manzana");
        System.out.println("Nombre "+mn.nombre);
        System.out.println("Caracteristicas "+mn.caracteristicas);
    }
    
}