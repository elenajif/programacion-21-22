public class Naranja extends Fruta
{
    public Naranja(){
        this.nombre="Naranja";
        this.caracteristicas="Piel no comestible";
    }
    public void imprimirInfo(Naranja nr) {
        System.out.println("Naranja");
        System.out.println("Nombre "+nr.nombre);
        System.out.println("Caracteristicas "+nr.caracteristicas);
    }
    
}