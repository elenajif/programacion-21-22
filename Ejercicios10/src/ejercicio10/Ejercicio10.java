package ejercicio10;

import java.time.LocalTime;
import java.util.ArrayList;

import ejercicios03040506.Avion;
import ejercicios03040506.Coche;
import ejercicios0709.Empleado;

public class Ejercicio10 {
	
	//tengo que recibir objetos
	//hasta ahora recibiamos Coches, Manzanas, Empleado, String ....
	//hoy incorporamos Object
	//permite recibir cualquier objeto

	public static void main(String[] args) {
		//ArrayList de objetos
		ArrayList<Object> lista = new ArrayList<>();
		
		System.out.println("A�ado un String");
		lista.add("hola");
		System.out.println("A�ado un LocalTime");
		lista.add(LocalTime.now());
		System.out.println("A�ado un coche");
		Coche miCoche = new Coche("FDE-1245","Peugeot",5,120.7);
		lista.add(miCoche);
		System.out.println("A�ado un avion");
		Avion miAvion = new Avion("1234EF","Boeing",2,2);
		lista.add(miAvion);
		System.out.println("A�ado un empleado");
		Empleado miEmpleado = new Empleado("Pepe");
		lista.add(miEmpleado);
		System.out.println("A�ado un booleano");
		lista.add(false);
		
		System.out.println("");
		System.out.println("Recorro la lista y la muestro");
		for (Object objeto: lista) {
			System.out.println(objeto);
		}
		
		System.out.println("");
		System.out.println("Matricula del coche "+miCoche.getMatricula());
		System.out.println("Marca del avion "+miAvion.getMarca());
		System.out.println("Nombre del empleado "+miEmpleado.getNombre());
	}

}
