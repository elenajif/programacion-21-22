package ejercicios0709;

//la clase Director no puede ser heredada por otra mas
//he a�adido final
public final class Director extends Empleado {
	public Director(String nombre) {
		super(nombre);
	}

	@Override
	public String toString() {
		return "[" + super.toString() + "] -> Director";
	}
	
	
}
