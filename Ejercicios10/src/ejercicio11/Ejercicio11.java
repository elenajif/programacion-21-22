package ejercicio11;

public class Ejercicio11 {

	public static void main(String[] args) {
		ListaPersonas miLista = new ListaPersonas();
		miLista.add(null);
		miLista.add(1, new Persona("Patricia", "1234"));
		miLista.add(1, new Persona("Francisco", "1234"));
		miLista.add(1, new Persona(null, null));
		miLista.add(1, new Persona("Silvia", "2346"));

		for (Persona persona : miLista) {
			System.out.println(persona);
		}

		System.out.println("Mostramos el toString tuneado");
		System.out.println(miLista);
	}

}
