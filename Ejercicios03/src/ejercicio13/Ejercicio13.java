package ejercicio13;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {
		Scanner escaner = new Scanner(System.in);

		System.out.println("Introduce una cadena de texto");
		String cadena1 = escaner.nextLine();

		System.out.println("Introduce otra cadena de texto");
		String cadena2 = escaner.nextLine();

		if (cadena1.equals(cadena2)) {
			System.out.println("Las cadenas son iguales, coincidencia completa");
		} else {
			if (cadena1.equalsIgnoreCase(cadena2)) {
				System.out.println("Mismas letras pero may�sculas y min�sculas");
			} else {
				System.out.println("No son iguales");
			}
		}

		escaner.close();

	}

}
