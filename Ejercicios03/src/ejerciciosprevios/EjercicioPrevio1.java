package ejerciciosprevios;

public class EjercicioPrevio1 {

	public static void main(String[] args) {
		int numero = 5;
		// sentencia if
		// solo hace algo si se cumple
		// sino se cumple no hace nada
		// if (condicion) {
		// lo que quieres hacer cuando es cierto
		// }
		if (numero > 3) {
			System.out.println("El numero es mayor de 3");
		}

		if (numero > 4 && numero < 6) {
			System.out.println("El numero esta entre 4 y 6");
		}

		if (numero > 24 || numero < 6) {
			System.out.println("El numero es mayor de 24 o menor de 6");
		}

		if (numero > 7 && numero < 10) {
			System.out.println("El numero est� entre 7 y 10");
		}
	}

}
