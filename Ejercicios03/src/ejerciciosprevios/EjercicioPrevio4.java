package ejerciciosprevios;

public class EjercicioPrevio4 {

	public static void main(String[] args) {
		// equals
		String cadena1 = "Hola caracola";
		String cadena2 = "Hola";
		System.out.println("cadena1 y cadena2 iguales? " + cadena1.equals(cadena2));
		// equalsIgnoreCase -> permite comprobar mayusculas y minusculas
		String cadena3 = "HOLA CARACOLA";
		System.out.println("cadena1 y cadena3 iguales? " + cadena1.equalsIgnoreCase(cadena3));
		//indexOf
		System.out.println(cadena1.indexOf("a"));
		//lastIndexOf
		System.out.println(cadena1.lastIndexOf("a"));

	}

}
