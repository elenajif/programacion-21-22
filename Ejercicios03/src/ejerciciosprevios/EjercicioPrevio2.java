package ejerciciosprevios;

public class EjercicioPrevio2 {

	public static void main(String[] args) {
		int numero = 5;
		// sentencia if-else
		// hace algo si se cumple
		// sino se cumple hace lo que esta en el else
		// if (condicion) {
		// lo que quieres hacer cuando es cierto
		// } else {
		// lo que quieres hacer cuando no es cierto
		//}
		if (numero > 3) {
			System.out.println("El numero es mayor de 3");
		} else {
			System.out.println("El numero no es mayor de 3");
		}

		if (numero > 4 && numero < 6) {
			System.out.println("El numero esta entre 4 y 6");
		} else {
			System.out.println("El numero no esta entre 4 y 6");
		}

		if (numero > 24 || numero < 6) {
			System.out.println("El numero es mayor de 24 o menor de 6");
		} else {
			System.out.println("El numero no es mayor de 24 ni menor de 6");
		}

		if (numero > 7 && numero < 10) {
			System.out.println("El numero est� entre 7 y 10");
		} else {
			System.out.println("El numero no est� entre 7 y 10");
		}
	}

}
