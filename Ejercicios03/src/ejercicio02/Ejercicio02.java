package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Dame un numero entero");
		int numero = input.nextInt();

		if (numero % 10 == 0) {
			System.out.println("El numero es m�ltiplo de 10");
		} else {
			System.out.println("El numero no es m�ltiplo de 10");
		}
		
		input.close();
	}

}
