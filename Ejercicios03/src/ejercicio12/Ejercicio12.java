package ejercicio12;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {
		Scanner escaner = new Scanner(System.in);

		System.out.println("Intoduce el numero de mes");
		int mes = escaner.nextInt();

		if (mes < 1 || mes > 12) {
			System.out.println("El mes no existe");
		} else {
			// el mes es correcto
			if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12) {
				System.out.println("El mes tiene 31 dias");
			} else if (mes == 4 || mes == 6 || mes == 9 || mes == 11) {
				System.out.println("El mes tiene 30 dias");
			} else {
				// ya no es ninguno de los anteriores
				// esta entre 1 y 12, es febrero
				System.out.println("El mes tiene 28 dias");
			}
		}

		escaner.close();

	}

}
