package ejercicio1;

import java.util.Scanner;

public class Ejercicio1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Introduce el n�mero de ventas");
		int numVentas = sc.nextInt();

		int sumaVentas = 0;
		for (int i = 0; i < numVentas; i++) {
			// indico el numero de venta
			System.out.println("Introduce el importe de la venta " + (i + 1));
			int venta = sc.nextInt();

			// Acumulamos el valor de la venta
			sumaVentas = sumaVentas + venta;
		}

		System.out.println("N�mero ventas "+numVentas);
		System.out.println("Total ventas "+sumaVentas+"�");
		System.out.println("IVA "+sumaVentas*0.21+"�");
		System.out.println("Total ventas con IVA "+sumaVentas*1.21+"�");
		sc.close();
	}

}
