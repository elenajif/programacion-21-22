package ejercicio3;

import java.util.Scanner;

public class Ejercicio3 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int opcion = 0;
		do {
			System.out.println("Men� de opciones");
			System.out.println("1.- Nota m�s baja");
			System.out.println("2.- Matricula correcta");
			System.out.println("3.- Tablas multiplicar");
			System.out.println("4.- Rect�ngulo");
			System.out.println("5.- Salir");
			System.out.println("Elegir opci�n");
			opcion = input.nextInt();
			input.nextLine();

			switch (opcion) {
			case 1:
				System.out.println("Introduce el total de notas a pedir");
				int cantidadNotas = input.nextInt();
				double menor = 0.0;
				for (int i = 0; i < cantidadNotas; i++) {
					System.out.println("Introduce una nota (" + (i + 1) + ")");
					double nota = input.nextDouble();
					if (i == 0) {
						menor = nota;
					} else if (nota < menor) {
						menor = nota;
					}
				}
				System.out.println("La nota m�s baja es " + menor);
				break;
			case 2:
				System.out.println("Dame una matricula");
				String matricula = input.nextLine().toLowerCase();
				boolean numeros = false;
				boolean letras = false;
				String matriculaNumeros = matricula.substring(0, 2);
				System.out.println("Numeros " + matriculaNumeros);
				String matriculaLetras = matricula.substring(2, 7);
				System.out.println("Letras " + matriculaLetras);
				for (int i = 0; i < matriculaNumeros.length(); i++) {
					if (matriculaNumeros.charAt(i) >= '0' && matriculaNumeros.charAt(i) <= '9') {
						numeros = true;
					}
				}
				for (int i = 2; i < matriculaLetras.length(); i++) {
					if (matriculaLetras.charAt(i) >= 'a' && matriculaLetras.charAt(i) <= 'z') {
						letras = true;
					}
				}
				if (numeros == true && letras == true) {
					System.out.println("Numeros " + numeros);
					System.out.println("Letras " + letras);
					System.out.println("Matricula correcta");
				} else {
					System.out.println("Numeros " + numeros);
					System.out.println("Letras " + letras);
					System.out.println("Matricula incorrecta");
				}
				break;
			case 3:
				for (int i = 1; i <= 10; i++) {
					System.out.println("\nTabla del " + i + "\n");
					for (int j = 1; j <= 10; j++) {
						System.out.println(i + " x " + j + " = " + (i * j));
					}
				}
				break;
			case 4:
				System.out.println("Introduce el ancho");
				int ancho = input.nextInt();
				System.out.println("Introduce el alto");
				int alto = input.nextInt();
				for(int i = 0; i < alto; i++){
					for(int j = 0; j < ancho; j++){
						System.out.print("* ");
					}
					System.out.println();
				}
				break;
			case 5:
				System.out.println("Salir");
				break;
			default:
				System.out.println("Opci�n incorrecta");
				break;
			}

		} while (opcion != 5);
		input.close();
	}

}
