
public class CalculoCuadrado
{
    public static void main(String[] args) {
        //creo objeto
        //clase nombreObjeto = new constructor
        //constructor por defecto cuando no lo he creado yo
        //nombreClase()
        SinMetodoMain smm = new SinMetodoMain();
        //cuando un metodo es static no necesita crear objeto
        //Clase.metodo
        int resultado=smm.calcularCuadrado(15);
        System.out.println(resultado);
        Mensaje mensajito = new Mensaje();
        mensajito.mostrarMensaje();
    }
}
