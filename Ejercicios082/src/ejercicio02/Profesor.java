package ejercicio02;

public class Profesor {
	//atributos
	private String nombre;
	private String apellidos;
	private int edad;
	private String ciclo;

	//constructores
	public Profesor(){
	}

	public Profesor(String nombre, String apellidos, int edad, String ciclo) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.edad = edad;
		this.ciclo = ciclo;
	}

	//setter y getter
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getCiclo() {
		return ciclo;
	}

	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}

	//toString
	//cuando en un metodo me indica delante @Override
	//java me esta indicando que el metodo ya existe
	//pero yo lo puedo sobreescribir
	//este metodo me permite mostrar los atributos de una clase
	//automatico -> boton derecho -> generate toString
	//este metodo devuelve con un return un String
	@Override
	public String toString() {
		return "Profesor [nombre=" + nombre + ", apellidos=" + apellidos + ", edad=" + edad + ", ciclo=" + ciclo + "]";
	}
	

	
}
