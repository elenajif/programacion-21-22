package ejercicio031;

public class Principal {

	public static void main(String[] args) {
		// creamos profesores
		Profesor profesor1 = new Profesor("Juan","Fernandez",33, "DAM");
		Profesor profesor2 = new Profesor("Jose","Garcia",38, "DAW");
		Profesor profesor3 = new Profesor("Carlota","Suarez",29, "DAW");
		Profesor profesor4 = new Profesor("Raquel","Lopez",25, "DAM");
		
		//mostramos profesores
		System.out.println("Profesor1");
		profesor1.mostrarDatosProfesor(profesor1);
		System.out.println("Profesor2");
		profesor2.mostrarDatosProfesor(profesor2);
		System.out.println("Profesor3");
		profesor3.mostrarDatosProfesor(profesor3);
		System.out.println("Profesor4");
		profesor4.mostrarDatosProfesor(profesor4);
	}

}
