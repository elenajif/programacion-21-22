package ejercicio03;

public class Profesor {
	//atributos
	private String nombre;
	private String apellidos;
	private int edad;
	private String ciclo;

	//constructores
	public Profesor(){
	}

	public Profesor(String nombre, String apellidos, int edad, String ciclo) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.edad = edad;
		this.ciclo = ciclo;
	}

	//setter y getter
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getCiclo() {
		return ciclo;
	}

	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}

	@Override
	public String toString() {
		return "Nombre=" + nombre + ", Apellidos=" + apellidos + "\nEdad=" + edad + "\nCiclo=" + ciclo;
	}
	
	//sino esta creado el metodo toString()
	//al imprimir el objeto 
	//syso(profesor2)
	//Profesor2
	//mostrar� algo como -> ejercicio03.Profesor@4e25154f
	

	
}
