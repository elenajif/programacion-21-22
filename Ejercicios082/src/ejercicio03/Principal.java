package ejercicio03;

public class Principal {

	public static void main(String[] args) {
		// creamos profesores
		Profesor profesor1 = new Profesor("Juan","Fernandez",33, "DAM");
		Profesor profesor2 = new Profesor("Jose","Garcia",38, "DAW");
		Profesor profesor3 = new Profesor("Carlota","Suarez",29, "DAW");
		Profesor profesor4 = new Profesor("Raquel","Lopez",25, "DAM");
		
		//muestro sus datos
		//para llamar al metodo toString de un String, no es necesario indicarlo
		//es lo mismo
		//System.out.println(profesor4.toString());
		//System.out.println(profesor4);
		System.out.println("Profesor1");
		System.out.println(profesor1);
		System.out.println("Profesor2");
		System.out.println(profesor2);
		System.out.println("Profesor3");
		System.out.println(profesor3);
		System.out.println("Profesor4");
		System.out.println(profesor4);
	}

}
