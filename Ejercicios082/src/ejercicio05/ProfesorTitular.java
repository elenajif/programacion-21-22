package ejercicio05;

public class ProfesorTitular extends Profesor {
	// atributos
	private int anyosCargo;

	// constructores
	public ProfesorTitular() {

	}

	public ProfesorTitular(String nombre, String apellidos, int edad, String ciclo, int anyosCargo) {
		super(nombre, apellidos, edad, ciclo);
		this.anyosCargo = anyosCargo;
	}

	// setter y getter
	public int getAnyosCargo() {
		return anyosCargo;
	}

	public void setAnyosCargo(int anyosCargo) {
		this.anyosCargo = anyosCargo;
	}

	@Override
	public String toString() {
		return "ProfesorTitular" + super.toString() + " \nanyosCargo=" + anyosCargo;
	}

	public double obtenerSalario() {
		double salario = 100 + this.anyosCargo * 63.25;
		return salario;
	}

}
