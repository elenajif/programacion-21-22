public class Principal
{
    public static void main(String[] args) {
        //Creo una fruta
        Fruta oFruta = new Fruta();
        System.out.println(oFruta.nombre);
        System.out.println(oFruta.caracteristicas);
        oFruta.imprimirInfo(oFruta);
        //Creo una manzana
        Manzana oManzana = new Manzana();
        System.out.println(oManzana.nombre);
        System.out.println(oManzana.caracteristicas);
        oManzana.imprimirInfo(oManzana);
        //Creo una naranja
        Naranja oNaranja = new Naranja();
        System.out.println(oNaranja.nombre);
        System.out.println(oNaranja.caracteristicas);
        oNaranja.imprimirInfo(oNaranja);
        
    }
}
