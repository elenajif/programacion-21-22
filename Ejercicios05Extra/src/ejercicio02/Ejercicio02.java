package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int seleccion;
		do {
			System.out.println("1.- N�meros de 1 al 100 con un while");
			System.out.println("2.- Positivos negativos o pares");
			System.out.println("3.- Salir");
			System.out.println("");
			System.out.println("Elija una opci�n");
			seleccion = input.nextInt();
			switch (seleccion) {
			case 1:
				int numero = 1;
				while (numero <= 100) {
					System.out.println(numero);
					numero++;
				}
				break;
			case 2:
				int numero1=0;
				do {
					System.out.println("introduce un numero");
					numero1=input.nextInt();
					if(numero1>0) {
						System.out.println("el numero es positivo");
					}else if(numero1<0) {
						System.out.println("el numero es negativo");
						
					}
					if(numero1%2==0) {
						System.out.println("el numero es par");
					}else {
						System.out.println("el numero es impar");
					}
						System.out.println();
				}while(numero1!=0);
				break;
			case 3:
				System.out.println("Fin del programa");
				break;
			default:
				System.out.println("Opci�n incorrecta");
				break;
			}

		} while (seleccion != 3);
	
		input.close();
	}

}
