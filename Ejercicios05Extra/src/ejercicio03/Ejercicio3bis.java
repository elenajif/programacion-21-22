package ejercicio03;

import java.util.Scanner;

public class Ejercicio3bis {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		boolean salir = false;
		do {
			System.out.println("____________________________________________________");
			System.out.println("1- Volumen esfera                                   |");
			System.out.println("2- Numero de tres cifras con cifras por separado    |");
			System.out.println("3- Letras minusculas                                |");
			System.out.println("4- Pares entre dos numeros                          |");
			System.out.println("5- Salir                                            |");
			System.out.println("____________________________________________________|");
			int menu = input.nextInt();
			switch (menu) {
			case 1:
				System.out.println("Dame el radio de la esfera");
				double radio = input.nextDouble();
				double volumen = (4 / 3) * Math.PI * Math.pow(radio, 3);
				System.out.println("El volumen es " + volumen);
				break;
			case 2:
				System.out.println("Introduzca el valor del numero");
				int numero = input.nextInt();
				System.out.println("Primera cifra " + (numero / 100));
				System.out.println("Segunda cifra " + ((numero / 10) % 10));
				System.out.println("Tercera cifra " + (numero % 10));
				break;
			case 3:
				input.nextLine();
				System.out.println("Escribe una letrica");
				char letra1 = input.nextLine().charAt(0);
				System.out.println("Escribe otra letrica");
				char letra2 = input.nextLine().charAt(0);
				boolean compara = true;
				if ((letra1 > 'z' || letra1 < 'a') || (letra2 > 'z' || letra2 < 'a')) {
					compara = false;
				}
				if (compara) {
					System.out.println("Ambas son minusculas");
				} else {
					System.out.println("No son minusculas las dos");
				}

				break;
			case 4:
				System.out.println("Da un n�mero");
				int numero1 = input.nextInt();
				System.out.println("Da un segundo n�mero (mayor que el anterior)");
				int numero2 = input.nextInt();
				for (int i = numero1; i < numero2; i++) {
					if (i % 2 == 0) {
						System.out.println(i);
					}
				}
				break;
			case 5:
				System.out.println("Fin del programa");
				salir = true;
				break;
			default:
				System.out.println(menu + " no es una opcion contemplada");
			}
		} while (!salir);

		input.close();
	}

}
