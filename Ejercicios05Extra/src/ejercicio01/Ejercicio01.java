package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		boolean fin = false;
		do {
			System.out.println("Menu de opciones");
			System.out.println("1.- Convertir a Binario");
			System.out.println("2.- Convertir a Hexadecimal");
			System.out.println("3.- Convertir a Octal");
			System.out.println("4.- Salir");
			System.out.println("Introduce una opci�n (1-4)");
			int opcion = input.nextInt();
			System.out.println("Introduce un numero entero en sistema decimal");
			int num = input.nextInt();
			switch (opcion) {
			case 1:
				System.out.println("Binario");
				System.out.println(Integer.toBinaryString(num));
				break;
			case 2:
				System.out.println("Hexadecimal");
				System.out.println(Integer.toHexString(num));
				break;
			case 3:
				System.out.println("Octal");
				System.out.println(Integer.toOctalString(num));
				break;
			case 4:
				System.out.println("Salir");
				fin = true;
				break;
			default:
				System.out.println("Introduce una opci�n del 1 al 4");
				break;
			}
		} while (fin == false);
		input.close();

	}

}
