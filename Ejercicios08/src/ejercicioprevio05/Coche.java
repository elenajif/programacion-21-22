package ejercicioprevio05;

public class Coche {
	// atributos
	String marca;
	String modelo;
	int velocidad;
	double tamRueda;

	// constructor
	public Coche() {
		marca = "opel";
		modelo = "corsa";
		velocidad = 234;
		tamRueda = 30.5;
	}

	// metodos para cambiar los atributos
	public void cambiarMarca(String marca1) {
		marca = marca1;
		System.out.println(marca);
	}

	public void cambiarModelo(String modelo1) {
		modelo = modelo1;
		System.out.println(modelo);
	}

	public void cambiarVelocidad(int velocidad1) {
		velocidad = velocidad1;
		System.out.println(velocidad);
	}

	public void cambiarTamRueda(double tamRueda1) {
		tamRueda = tamRueda1;
		System.out.println(tamRueda);
	}

}
