package ejercicioprevio05;

public class Principal {

	public static void main(String[] args) {
		Coche miCoche = new Coche();
		System.out.println("Marca " + miCoche.marca);
		System.out.println("Modelo " + miCoche.modelo);
		System.out.println("Velocidad " + miCoche.velocidad);
		System.out.println("Tama�o rueda " + miCoche.tamRueda);
		System.out.print("Cambio modelo ");
		miCoche.cambiarMarca("Seat");
		System.out.print("Cambio modelo ");
		miCoche.cambiarModelo("Seat Leon");
		System.out.print("Cambio velocidad ");
		miCoche.cambiarVelocidad(2345);
		System.out.print("Cambio tama�o rueda ");
		miCoche.cambiarTamRueda(2.5);
	}

}
