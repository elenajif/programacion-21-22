package ejercicio05;

import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		// pedir datos con Scanner
		Scanner input = new Scanner(System.in);

		System.out.println("Dame el modelo");
		String modelo = input.nextLine();
		System.out.println("Dame la marca");
		String marca = input.nextLine();
		System.out.println("Dame la autonomia");
		int autonomia = input.nextInt();
		System.out.println("Dame el kilometraje");
		float kilometraje = input.nextFloat();

		// crear vehiculo con datos introducidos por teclado
		Vehiculo vehiculo1 = new Vehiculo(modelo, marca, autonomia, kilometraje);

		// mostrar con getter
		System.out.println("Modelo "+vehiculo1.getModelo());
		System.out.println("Marca "+vehiculo1.getMarca());
		System.out.println("Autonomia "+vehiculo1.getAutonomia());
		System.out.println("Kilometraje "+vehiculo1.getKilometraje());

		// es seguro?
		System.out.println("Es seguro?");
		System.out.println(vehiculo1.esSeguro(vehiculo1.getKilometraje(), vehiculo1.getAutonomia()));
		
		input.close();
	}

}
