package ejercicio04;

public class Libro {
	//atributos
	private String titulo;
	private String autor;
	private int anyo;
	private String editorial;
	private float precio;

	//constructor sin parametros
	//boton derecho - source - constructor using fields
	public Libro() {
		this.titulo = "";
		this.autor = "";
		this.anyo = 0;
		this.editorial = "";
		this.precio = 0.0F;
	}

	//constructor con parametros
	public Libro(String titulo, String autor, int anyo, String editorial, float precio) {
		this.titulo = titulo;
		this.autor = autor;
		this.anyo = anyo;
		this.editorial = editorial;
		this.precio = precio;
	}

	//setter y getter	
	//boton derecho - source - setter y getter
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public int getAnyo() {
		return anyo;
	}

	public void setAnyo(int anyo) {
		this.anyo = anyo;
	}

	public String getEditorial() {
		return editorial;
	}

	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}

	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}
	
	//metodo
	static float precioConIva(float precio, float iva) {
		return (precio+(precio*iva/100));
	}
	
	//no hay metodo rellenar
	//no hay metodo visualizar
}
