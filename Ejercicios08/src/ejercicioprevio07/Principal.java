package ejercicioprevio07;

public class Principal {

	public static void main(String[] args) {
		Fruta oFruta = new Fruta("Frutita","madura");
		oFruta.imprimirInfo(oFruta);
		Naranja oNaranja = new Naranja("Naranjita","dulce");
		oNaranja.imprimirInfo(oNaranja);
		Manzana oManzana = new Manzana("Manzana","piel roja");
		oManzana.imprimirInfo(oManzana);
	}

}
