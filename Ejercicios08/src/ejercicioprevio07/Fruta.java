package ejercicioprevio07;

public class Fruta {
	String nombre;
	String caracteristicas;
	
	public Fruta(String nombre, String caracteristicas) {
		this.nombre = nombre;
		this.caracteristicas = caracteristicas;
	}
	
	public void imprimirInfo(Fruta fr) {
		System.out.println("Nombre "+fr.nombre);
		System.out.println("Caracteristicas "+fr.caracteristicas);
	}
	
}
