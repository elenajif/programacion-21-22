package ejercicioprevio06;

public class Coche {
	// atributos
	String marca;
	String modelo;
	int velocidad;
	double tamRueda;
	
	//constructor
	public Coche() {
		this.marca = "opel";
		this.modelo = "corsa";
		this.velocidad = 234;
		this.tamRueda = 30.5;
	}
	
	public Coche(String marca, String modelo, int velocidad, double tamRueda) {
		this.marca = marca;
		this.modelo = modelo;
		this.velocidad = velocidad;
		this.tamRueda = tamRueda;
	}
	
	//setter y getter
	public String getMarca() {
		return marca;
	}
	
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public int getVelocidad() {
		return velocidad;
	}
	public void setVelocidad(int velocidad) {
		this.velocidad = velocidad;
	}
	public double getTamRueda() {
		return tamRueda;
	}
	public void setTamRueda(double tamRueda) {
		this.tamRueda = tamRueda;
	}

	
}
