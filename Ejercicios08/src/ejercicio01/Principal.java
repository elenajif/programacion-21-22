package ejercicio01;

import java.util.Scanner;

public class Principal {

	static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args) {
		// creamos profesores
		//sino he creado un constructor, tengo uno por defecto Clase() -> Profesor()
		Profesor obProfe1=new Profesor();
		Profesor obProfe2=new Profesor();
		Profesor obProfe3=new Profesor();
		Profesor obProfe4=new Profesor();
		
		//rellenar profesor
		//el metodo es static, no requiere objeto
		//el metodo no es static, requiere objeto
		rellenarProfesor(obProfe1);
		rellenarProfesor(obProfe2);
		rellenarProfesor(obProfe3);
		rellenarProfesor(obProfe4);
		
		//muestro profesor
		System.out.println("Profe1");
		mostrarProfesor(obProfe1);
		System.out.println("Profe2");
		mostrarProfesor(obProfe2);
		System.out.println("Profe3");
		mostrarProfesor(obProfe3);
		System.out.println("Profe4");
		mostrarProfesor(obProfe4);
		
		input.close();
	}
	
	public static void rellenarProfesor(Profesor obProfesor) {
		//pedimos datos
		System.out.println("Dime el nombre");
		String nombre=input.nextLine();
		System.out.println("Dime los apellidos");
		String apellidos=input.nextLine();
		System.out.println("Dame el ciclo");
		String ciclo=input.nextLine();
		//guardamos los datos
		//necesitamos el set
		obProfesor.setNombre(nombre);
		obProfesor.setApellidos(apellidos);
		obProfesor.setCiclo(ciclo);
	}
	
	public static void mostrarProfesor(Profesor obProfesor) {
		//para mostrar los datos, usamos el get
		System.out.println("Su nombre es "+obProfesor.getNombre());
		System.out.println("Sus apellidos son "+obProfesor.getApellidos());
		System.out.println("El ciclo que imparte es "+obProfesor.getCiclo());
	}

}
