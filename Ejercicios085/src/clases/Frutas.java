package clases;

public class Frutas {
	
	//atributos
	private String codigoFruta;
	private String tipoFruta;
	private double precio;
	private String fruteria;
	

	public Frutas(String codigoFruta) {
		this.codigoFruta = codigoFruta;
	}
	
	//setter y getter

	public String getCodigoFruta() {
		return codigoFruta;
	}

	public void setNombreFruta(String nombreFruta) {
		this.codigoFruta = nombreFruta;
	}

	public String getTipoFruta() {
		return tipoFruta;
	}

	public void setTipoFruta(String TipoFruta) {
		this.tipoFruta = TipoFruta;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public String getFruteria() {
		return fruteria;
	}

	public void setFruteria(String fruteria) {
		this.fruteria = fruteria;
	}
	
	//metodo toString
	@Override
	public String toString(){
		return codigoFruta + " " + tipoFruta + " " + precio + " " + fruteria;
	}
	
	
}
