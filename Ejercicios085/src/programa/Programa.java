package programa;

import clases.Fruteria;

public class Programa {

	public static void main(String[] args) {		
		System.out.println("1- Crear una instancia de Fruteria llamada miFruteria con 4 frutas");
		int maxFrutas = 4;
		Fruteria miFruteria = new Fruteria(maxFrutas);
		System.out.println("Instancia creada");

		System.out.println("\n2- Dar de alta 4 frutas distintas.");
		System.out.println("Damos de alta 4 frutas");
		miFruteria.altaFruta("NNNN", "Naranja", 1.55, "Fruteria 1");
		miFruteria.altaFruta("CCCC", "Manzana", 1.20, "Fruteria 1");
		miFruteria.altaFruta("FFFF", "Pera", 0.75, "Fruteria 1");
		miFruteria.altaFruta("RRRR", "Melocot�n", 1.75, "Fruteria 2");
		
		System.out.println("\n3- Listar las frutas."); 
		miFruteria.listarFrutas();
		
		System.out.println("\n4- Buscar una fruta por su c�digo y mostrar sus datos por pantalla.");
		System.out.println("Buscamos fruta con c�digo NNNN");
		System.out.println(miFruteria.buscarFruta("NNNN"));
		
		System.out.println("\n5- Eliminar una fruta diferente al anterior.");
		System.out.println("Eliminamos la fruta CCCC");
		miFruteria.eliminarFruta("CCCC");
		miFruteria.listarFrutas();
		
		System.out.println("\n6- Almacenar una nueva fruta."); 
		System.out.println("Almacenamos Kiwi");
		miFruteria.altaFruta("WWWW", "Kiwi", 1.33, "Fruteria 3");
		miFruteria.listarFrutas();
		
		System.out.println("\n7- Modificar el nombre de un fruta.");
		System.out.println("Modificamos la fruta WWWW, ahora es una pera");
		miFruteria.cambiarNombreFruta("WWWW", "Pera");
		miFruteria.listarFrutas();

		System.out.println("\n8- Listar solo las frutas de una fruteria."); 
		System.out.println("Listamos las frutas del fruteria 1");
		miFruteria.listarFrutasPorFruteria("Fruteria 1");
	}
}
