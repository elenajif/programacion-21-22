package ejercicio9;

public class Ejercicio09 {

	public static void main(String[] args) {
		int numero = -3;
		boolean condicion = (numero % 3 == 0 || numero % 7 == 0) && numero >= 0;
		System.out.println(condicion?"Se cumplen las condiciones":"No se cumplen");

	}

}
