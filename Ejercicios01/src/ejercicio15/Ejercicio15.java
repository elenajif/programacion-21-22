package ejercicio15;

public class Ejercicio15 {
	
	static final String MENSAJE_INICIO="inicio del programa";
	static final String MENSAJE_FIN="fin del programa";

	public static void main(String[] args) {
		
		System.out.println(MENSAJE_INICIO);
		
		String cadenaLarga="Una cadena larga de m�s de 20 caracteres";
		System.out.println(cadenaLarga);
		String subcadena=cadenaLarga.substring(10, 17);
		System.out.println(subcadena);
		
		System.out.println(MENSAJE_FIN);

	}

}
