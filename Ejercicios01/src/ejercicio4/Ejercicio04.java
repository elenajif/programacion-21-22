package ejercicio4;

public class Ejercicio04 {

	public static void main(String[] args) {
		int varA=6;
		int varB=-45;
		int varC=500;
		int varD=-1234;
		
		System.out.println("El valor de la variable varA es "+varA);
		System.out.println("El valor de la variable varB es "+varB);
		System.out.println("El valor de la variable varC es "+varC);
		System.out.println("El valor de la variable varD es "+varD);
		
		//Para intercambiar el valor entre diferentes variables
		//necesitamos como minimo una variable mas
		//para no perder el valor
		
		int aux=varA;
		//Ahora ya he guardado el primer valor
		//Ya puedo cambiar los valores
		varA=varB;
		varB=varC;
		varC=varD;
		
		//como varA ya no contiene su valor original
		//lo tengo que traspasar usando aux
		varD=aux;
		
		System.out.println("");
		System.out.println("El valor de la variable varA es "+varA);
		System.out.println("El valor de la variable varB es "+varB);
		System.out.println("El valor de la variable varC es "+varC);
		System.out.println("El valor de la variable varD es "+varD);

	}

}
