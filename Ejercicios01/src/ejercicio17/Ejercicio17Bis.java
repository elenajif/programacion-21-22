package ejercicio17;

public class Ejercicio17Bis {

	public static void main(String[] args) {
		String cadena1="A";
		String cadena2="Otra cadena";
		
		//los String tienen un metodo que permite comparar cadenas
		//compareTo
		//el resultado de compareTo es un entero
		//primeraCadena.compareTo(segundaCadena) -> devuelve un entero
		
		int resultado=cadena1.compareTo(cadena2);
		
		//resultado>0 -> cadena1>cadena2
		//resultado<0 -> cadena1<cadena2
		//resultado=0 -> cadena1=cadena2
		//compara los codigos ascii
		
		System.out.println(resultado>0?"cadena1 es mayor":
			    (resultado==0?"cadena1 es igual a cadena2":"cadena1 es menor"));
		

	}

}
