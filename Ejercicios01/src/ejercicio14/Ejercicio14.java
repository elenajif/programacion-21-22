package ejercicio14;

public class Ejercicio14 {

	public static void main(String[] args) {
		String textoEntero="2345";
		String textoEnteroLargo="2654251321481425123158";
		String textoDecimal="1234.2255";
		String textoByte="123";
		
		//transformamos un texto que contiene un entero a un entero
		int entero=Integer.parseInt(textoEntero);
		//transformamos un texto que contiene un long a un long
		long enteroLargo=Long.parseLong(textoEnteroLargo);
		//transformamos un texto que contiene un double en un double
		double enteroDecimal=Double.parseDouble(textoDecimal);
		//transformamos un texto que contiene un byte en un byte
		byte numByte=Byte.parseByte(textoByte);
		
		System.out.println(entero);
		System.out.println(enteroLargo);
		System.out.println(enteroDecimal);
		System.out.println(numByte);
	}

}
