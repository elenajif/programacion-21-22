package ejercicio18;

import java.util.Scanner;

public class Ejercicio18 {

	public static void main(String[] args) {
		
		//creo un objeto clase Scanner
		Scanner teclado = new Scanner(System.in);
		
		//byte
		System.out.println("Introduce un byte");
		byte numByte = teclado.nextByte();
		System.out.println("El n�mero byte es "+numByte);
		
		//short
		System.out.println("Introduce un short");
		short numShort=teclado.nextShort();
		System.out.println("El n�mero short es "+numShort);
		
		//int
		System.out.println("Introduce un entero");
		int numInt=teclado.nextInt();
		System.out.println("El n�mero entero es "+numInt);
		
		//long
		System.out.println("Introduce un long");
		long numLong=teclado.nextLong();
		System.out.println("El n�mero long es "+numLong);
		
		//float
		System.out.println("Introduce un float");
		float numFloat=teclado.nextFloat();
		System.out.println("El n�mero float es "+numFloat);
		
		//double
		System.out.println("Introduce un double");
		double numDouble=teclado.nextDouble();
		System.out.println("El numero double es "+numDouble);
		
		//limpio buffer (he leido un numero y ahora quiero leer un caracter o un String)
		teclado.nextLine();
		
		//char
		//para leer un caracter se usa nextLine que lee una linea
		//no hay un nextChar se usa nextLine
		//para guardar solo el primer caracter se tiene que indicar con .charAt(0)
		System.out.println("Introduce un char");
		char caracter=teclado.nextLine().charAt(0);
		System.out.println("El caracter es "+caracter);
		
		//cierro Scanner
		teclado.close();
		
	}
	

}
