package ejerciciosprevios;

//linea para importar la clase Scanner de java.util
import java.util.Scanner;

public class EjercicioPrevioScanner {

	public static void main(String[] args) {
		
		//creo un objeto Scanner llamado input
		Scanner input = new Scanner(System.in);
		
		//vamos a usarlo, pido un dato por teclado
		System.out.println("Dame un numero entero");
		//el metodo nextInt() permite leer un entero
		int numInt=input.nextInt();
		//lo muestro
		System.out.println("El numero entero es "+numInt);
		
		//cierro el Scanner
		input.close();

	}

}
