package ejerciciosprevios;

public class EjercicioPrevioString {

	public static void main(String[] args) {
		int num1=3; //tipo primitivo
		System.out.println("El valor de num1 es "+num1);
		char letra='a'; //tipo primitivo
		System.out.println("La letra es "+letra);
		String cadena="hola caracola";//no es primitivo -> Mayusculas -> clase
		System.out.println("La cadena es "+cadena);
		//Clase String pertenece a java.lang
		//java.lang es paquete de java
		//viene por defecto, por eso no necesita import
		System.out.println("La longitud de la cadena es "+cadena.length());
		//declaro un String y lo muestro
		String textoEntero="1245";
		System.out.println(textoEntero);
		//declaro un entero y lo parseo a int usando una clase (Integer)
		int entero=Integer.parseInt(textoEntero);
		System.out.println(entero);
		entero--;
		System.out.println(entero);
		//Los String tienen muchas funcionalidades
		//Por ejemplo extraer un trozo de una cadena
		String frase="Esto es una frase completa";
		System.out.println(frase);
		//con substring le puedo indicar comienzo y final 
		String trozo=frase.substring(5, 11);
		System.out.println(trozo);

	}

}
