package ejercicio1;

public class Ejercicio01 {

	public static void main(String[] args) {
		// declaro 3 variables y las inicializo
		int entero=-24;
		double decimal=56.3;
		char caracter='Y';
		
		// muestro su valor por pantalla
		System.out.println(entero);
		System.out.println(decimal);
		System.out.println(caracter);
		
		// sumo el int con el double
		System.out.println(entero+decimal);
		//resto el int del double
		System.out.println(entero-entero);
		
		//muestro el valor num�rico del caracter
		System.out.println((int)caracter);

	}

}
