package ejercicio5;

public class Ejercicio05 {

	public static void main(String[] args) {
		// int con valores a = 5, b = 3, c = -12
		int a = 5;
		int b = 3;
		int c = -12;

		System.out.println(a > 3); //true
		System.out.println(c / b == -4);//true
		System.out.println((a + b == 8) && (a - b == 2));//true && true -> true
	}

}
