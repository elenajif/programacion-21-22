package ejercicio7;

public class Ejercicio07 {

	public static void main(String[] args) {
		// condicional doble -> positivo y par
		// positivo && par
		// positivo -> numero>=0
		// par -> el resto de la division por 2 es cero -> numero%2==0
		
		int numero=456;
		System.out.println((numero>=0)&&(numero%2==0)?"es positivo y par":"no cumple las condiciones");

	}

}
