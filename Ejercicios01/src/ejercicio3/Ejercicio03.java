package ejercicio3;

public class Ejercicio03 {

	public static void main(String[] args) {
		int num;
		num = -11;

		// incrementar en 77
		num = num + 77;
		// misma operación
		// num+=77;
		System.out.println(num);

		// decrementar en 3
		num = num - 3;
		// misma operación
		// num-=3;
		System.out.println(num);

		// duplicar su valor
		num = num * 2;
		// misma operación
		// num*=2;
		System.out.println(num);

	}

}
