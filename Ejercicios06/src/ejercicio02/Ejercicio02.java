package ejercicio02;

public class Ejercicio02 {

	public static void main(String[] args) {
		//en este caso no pido los datos por teclado
		//en este caso no me guardo el valor del maximo ni el del minimo
		//los muestro directamente
		System.out.println("El maximo es " + Metodos02.maximo(4, 5));
		System.out.println("El minimo es " + Metodos02.minimo(4, 5));
	}

}
