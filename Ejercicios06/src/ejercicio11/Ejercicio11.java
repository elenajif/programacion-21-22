package ejercicio11;

import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce una palabra");
		String palabra = input.nextLine().toLowerCase();

		String palabraResultado = cifrar(palabra, 26);
		System.out.println(palabraResultado);

		input.close();

	}

	static String cifrar(String palabra, int desfase) {
		String resultado = "";

		// si tengo un numero m�s grande de 26, permite trabajar menos
		// si es 5 avanza 5
		// si es 30 avanza 4 (a partir del 26)
		desfase = desfase % 26;
		for (int i = 0; i < palabra.length(); i++) {
			char caracterDesfasado = (char) (palabra.charAt(i) + desfase);
			//recordar, hemos pasado la palabra a minusculas
			if (caracterDesfasado > 'z') {
				caracterDesfasado = (char) ('a' + desfase - 1);
			}
			resultado += caracterDesfasado;
		}
		return resultado;
	}

}
