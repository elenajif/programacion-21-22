package ejerciciosprevios;

import java.util.Scanner;

public class EjercicioPrevio14 {

	public static void main(String[] args) {
		System.out.println("Estoy en el main");
		// llamada a metodo void
		System.out.println("Llamo a longitudNombre()");
		longitudNombre();
		//creo una variable y llamo al metodo int
		System.out.println("LLamo a calculaLongitud()");
		int longitudCadena = calculaLongitud();
		System.out.println(longitudCadena);
		//uso la variable que me ha dado el metodo calculaLongitud()
		System.out.println("Calculo si la longitud es mayor de 5");
		if (longitudCadena > 5) {
			System.out.println("Es mayor de 5");
		}
		//llamada a metodo que tenga un parametro
		System.out.println("LLamo a cambiaMayusculas con un parametro");
		String cadena="Esto es una frase";
		cambiaMayusculas(cadena);
		System.out.println("Vuelvo a llamar al metodo cambiaMayusculas");
		String otraCadena="Hola caracola";
		cambiaMayusculas(otraCadena);
	}

	// declaramos los metodos y les damos funcionalidad
	// public no es necesario si lo voy a usar en la misma clase
	// static lo usamos para no trabajar con POO
	// llamamos directamente al metodo partiendo de la clase
	// Clase.metodo -> EjercicioPrevio14.longitudNombre()
	// si el metodo esta en la misma clase no es necesario ponerlo
	// longitudNombre()
	// void cuando no devuelve nada, no sale nada fuera del "cajon" del metodo
	// nombre metodo -> misnusculasMayusculas (lowerCamelCase)
	public static void longitudNombre() {
		Scanner input = new Scanner(System.in);
		System.out.println("Dame tu nombre");
		String nombre = input.nextLine();
		System.out.println("La longitud del nombre es " + nombre.length());
		input.close();
	}

	// int me permite devolver un entero fuera del metodo
	public static int calculaLongitud() {
		String cadena = "Hola caracola";
		int longitud = cadena.length();
		return longitud;
	}
	
	//metodo que recibe una cadena y la pasa a mayusculas
	public static void cambiaMayusculas(String miCadena) {
		System.out.println(miCadena.toUpperCase());
	}

}
