package ejerciciosprevios;

public class EjercicioPrevio23 {

	public static void main(String[] args) {
		// Menu Navidad, Llamamos al menu
		menu();
		//LLamamos al primero
		primerPlato();
		//Llamamos al segundo
		segundoPlato();
		//Llamamos al tercero
		postre();
	}
	//metodo menu que muestra cabecera menu navidad
	static void menu() {
		System.out.println("********** MENU NAVIDAD **********");
	}
	//metodo que muestre el contenido del primero
	static void primerPlato() {
		System.out.println("1.- Crema de marisco");
	}	
	//metodo que muestre el contenido del segundo
	static void segundoPlato() {
		System.out.println("2.- Solomillo relleno al Pedro Ximenez");
	}
	//metodo que muestre el contenido del postre
	static void postre() {
		System.out.println("3.- Panacotta de fresa");
	}
	
}
