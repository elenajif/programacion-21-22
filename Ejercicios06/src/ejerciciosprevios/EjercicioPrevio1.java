package ejerciciosprevios;

//definicion de la clase
//public class
public class EjercicioPrevio1 {
	//public accesible desde cualquier clase
	//static no requiere objetos (POO)
	//void lo que devuelve (void nada, int entero, double decimal...)
	//nombre metodo suma()
	//notacion metodos lowerCamelCase
	//suma() sumaNumeros() sumaNumerosEnteros()
	//los paréntesis de suma() es lo que recibe -> en este caso nada
	public static void suma() {
		// suma dos numeros
		int numero1 = 3;
		int numero2 = 5;
		System.out.println("La suma es " + (numero1 + numero2));
	}

	public static void multiplica() {
		// multiplica dos numeros
		int numero1 = 4;
		int numero2 = 12;
		System.out.println("La multiplicación es " + (numero1 * numero2));
	}
	
	//definicion del metodo main
	//public static void main
		public static void main(String[] args) {
			// llamo al metodo multiplicacion
			multiplica();
			// llamo al metodo suma
			suma();

		}


}
