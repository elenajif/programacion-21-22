package ejerciciosprevios;

public class EjercicioPrevio8 {

	public static void main(String[] args) {
		// llamamos a los metodos
		sumaNumeros();
		restaNumeros();
		multiplicaNumeros();
		divideNumeros();
	}

	// metodo suma void
	public static void sumaNumeros() {
		int numero1 = 4;
		int numero2 = 5;
		System.out.println("La suma es " + (numero1 + numero2));
	}

	// metodo resta void
	public static void restaNumeros() {
		int numero1 = 4;
		int numero2 = 5;
		System.out.println("La resta es " + (numero1 - numero2));
	}

	// metodo multiplica void
	public static void multiplicaNumeros() {
		int numero1 = 4;
		int numero2 = 5;
		System.out.println("La multiplicacion es " + (numero1 * numero2));
	}

	// metodo divide void
	public static void divideNumeros() {
		int numero1 = 4;
		int numero2 = 5;
		double divide = ((double) numero1 /(double) numero2);
		System.out.println("La division es " + divide);
	}

}
