package ejerciciosprevios;

import java.util.Scanner;

public class EjercicioPrevio16 {

	public static void main(String[] args) {
		//pido un cadena
		Scanner input=new Scanner(System.in);
		System.out.println("Dame la cadena");
		String miCadena=input.nextLine();
		//llamo a metodo void cambiarMayusculas(cadena)
		cambiarMayusculas(miCadena);
		//llamo a metodo void cambiarMinusculas(cadena)
		cambiarMinusculas(miCadena);
		//llamo a metodo void longitud(cadena)
		longitud(miCadena);
		//llamo a metodo void primeraLetra(cadena)
		primeraLetra(miCadena);
		input.close();
	}
	
	//metodo cambiarMayusculas(cadena)
	static void cambiarMayusculas(String cadena) {
		System.out.println("La cadena en mayusculas es "+cadena.toUpperCase());
	}
	//metodo cambiarMinusculas(cadena)
	static void cambiarMinusculas(String cadena) {
		System.out.println("La cadena en minusculas es "+cadena.toLowerCase());
	}
	//metodo longitud(cadena)
	static void longitud(String cadena) {
		System.out.println("La cadena tiene la longitud "+cadena.length());
	}
	//metodo primeraLetra(cadena)
	static void primeraLetra(String cadena) {
		System.out.println("La primera letra de la cadena es "+cadena.charAt(0));
	}
}
