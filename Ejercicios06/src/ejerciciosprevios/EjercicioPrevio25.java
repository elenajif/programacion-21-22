package ejerciciosprevios;

public class EjercicioPrevio25 {

	public static void main(String[] args) {
		// factorial 5 = 5*4*3*2*1
		// factorial 4 = 4*3*2*1
		// factorial 3 = 3*2*1
		// factorial 2 = 2*1
		// factorial 0 = 1
		// factorial 5 = 5*factorial 4
		// factorial 4 = 4*factorial 3
		System.out.println("Factorial de 6 "+factorial(6));
		System.out.println("Factorial de 5 "+factorial(5));
		System.out.println("Factorial de 4 "+factorial(4));
		System.out.println("Factorial de 3 "+factorial(3));
	}

	//metodo recursivo -> recursividad
	//usar un metodo para llamarse a si mismo
	static int factorial(int entero) {
		if (entero == 0) {
			return 1;
		}
		return factorial(entero - 1) * entero;
	}
}
