package ejerciciosprevios;

import java.util.Scanner;

public class EjercicioPrevio22 {

	static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args) {
		// pido dos numeros usando un metodo
		System.out.println("Pido numeros");
		int numero1=dameNumero();
		int numero2=dameNumero();
		// hago operacion suma en un metodo
		System.out.println("Llamo a sumaNumeros");
		int sumando=sumaNumeros(numero1,numero2);
		// muestro resultado de la suma con un metodo
		System.out.println("LLamo a mostrar");
		mostrar(sumando);
		input.close();
	}

	// pido dos numeros usando un metodo
	static int dameNumero() {
		System.out.println("Dame un numero");
		int numero=input.nextInt();
		return numero;
	}

	// hago operacion suma en un metodo
	static int sumaNumeros(int num1, int num2) {
		int suma=num1+num2;
		return suma;
	}
	
	// muestro resultado de la suma con un metodo
	static void mostrar(int numerosSumados) {
		System.out.println("La suma es "+numerosSumados);
	}

}
