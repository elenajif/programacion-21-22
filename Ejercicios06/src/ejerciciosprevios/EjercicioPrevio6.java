package ejerciciosprevios;

import java.util.Scanner;

public class EjercicioPrevio6 {

	public static void main(String[] args) {
		muestraNombre();
	}

	public static void muestraNombre() {
		Scanner input = new Scanner(System.in);
		System.out.println("Dame un nombre");
		String nombre = input.nextLine();
		System.out.println("Tu nombre es " + nombre);
		input.close();
	}

}
