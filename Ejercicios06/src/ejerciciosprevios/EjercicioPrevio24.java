package ejerciciosprevios;

import java.util.Scanner;

public class EjercicioPrevio24 {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Menu Navidad, Llamamos al menu
		menu();
		// LLamamos al primero
		String primero = primerPlato();
		// Llamamos al segundo
		String segundo = segundoPlato();
		// Llamamos al tercero
		String postre = postre();
		// muestro todo
		System.out.println("Tu menu es");
		System.out.println(primero);
		System.out.println(segundo);
		System.out.println(postre);
		input.close();
	}

	// metodo menu que muestra cabecera menu navidad
	static void menu() {
		System.out.println("********** MENU NAVIDAD **********");
		System.out.println("********* Elige tu men� *********");
	}

	// metodo que muestre el contenido del primero
	static String primerPlato() {
		System.out.println("Primeros");
		System.out.println("1.- Crema de marisco");
		System.out.println("2.- Judias pintas con chipirones");
		System.out.println("3.- Rissoto de setas");
		System.out.println("Elige un primero");
		int opcion = input.nextInt();
		String primerPlato = "";
		switch (opcion) {
		case 1:
			primerPlato = "Crema de marisco";
			break;
		case 2:
			primerPlato = "Judias pintas con chipirones";
			break;
		case 3:
			primerPlato = "Rissoto de setas";
			break;
		}
		return primerPlato;
	}

	// metodo que muestre el contenido del segundo
	static String segundoPlato() {
		System.out.println("Segundos");
		System.out.println("1.- Solomillo relleno al Pedro Ximenez");
		System.out.println("2.- Pollo relleno en salsa de champagne");
		System.out.println("3.- Salmon en salsa de setas");
		System.out.println("Elige un segundo");
		int opcion = input.nextInt();
		String segundoPlato="";
		switch (opcion) {
		case 1:
			segundoPlato="Solomillo relleno al Pedro Ximenez";
			break;
		case 2:
			segundoPlato="Pollo relleno en salsa de champagne";
			break;
		case 3:
			segundoPlato="Salmon en salsa de seta";
			break;
		}
		return segundoPlato;
	}

	// metodo que muestre el contenido del postre
	static String postre() {
		System.out.println("Postres");
		System.out.println("1.- Panacotta de fresa");
		System.out.println("2.- Pastel de navidad");
		System.out.println("3.- Coulant de chocolate");
		System.out.println("Elige un postre");
		int opcion = input.nextInt();
		String postre="";
		switch (opcion) {
		case 1 :
			postre="Panacotta de fresa";
			break;
		case 2:
			postre="Pastel de navidad";
			break;
		case 3:
			postre="Coulant de chocolate";
			break;
		}
		return postre;
	}

}
