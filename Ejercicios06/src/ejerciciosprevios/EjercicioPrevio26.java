package ejerciciosprevios;

import java.util.Scanner;

// repaso general metodos static

public class EjercicioPrevio26 {

	// declaro scanner para varios metodos
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// llamo al metodo mostrarNombre
		mostrarNombre();
		// llamo al metodo calculaPrecio
		double precio = calculaPrecio();
		System.out.println("El precio total es " + precio);
		// llamo al metodo sumaDatos(double numero1, double numero2)
		double miSuma = sumaDatos(2, 3);
		System.out.println("La suma es " + miSuma);
		// llamo al metodo sumaDatos(double numero2, double numero2, double numero3)
		double miSuma1 = sumaDatos(2, 3, 4);
		System.out.println("La suma es " + miSuma1);
		input.close();
	}

	// metodo void
	static void mostrarNombre() {
		System.out.println("Dame tu nombre");
		String nombre = input.nextLine();
		System.out.println("Tu nombre es " + nombre);
	}

	// metodo double
	static double calculaPrecio() {
		System.out.println("Dame una cantidad");
		double cantidad = input.nextDouble();
		double total = cantidad * 1.21;
		return total;
	}

	// metodo double que recibe datos (parametros)
	static double sumaDatos(double numero1, double numero2) {
		return numero1 + numero2;
	}

	// metodo sobrecargado
	static double sumaDatos(double numero1, double numero2, double numero3) {
		return numero1 + numero2 + numero3;
	}

}
