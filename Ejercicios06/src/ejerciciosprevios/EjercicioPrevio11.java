package ejerciciosprevios;

public class EjercicioPrevio11 {

	public static void main(String[] args) {
		// llamamos a los metodos
		//guardo el valor para usarlo despues si lo necesito
		int suma=sumaNumeros();
		System.out.println(suma);
		//muestra sin guardar el valor
		System.out.println(restaNumeros());
		System.out.println(multiplicaNumeros());
		System.out.println(divideNumeros());
	}

	// metodo suma int
	public static int sumaNumeros() {
		int numero1 = 10;
		int numero2 = 21;
		int suma = numero1 + numero2;
		return suma;
	}

	// metodo resta int
	public static int restaNumeros() {
		int numero1 = 10;
		int numero2 = 21;
		int resta = numero1 - numero2;
		return resta;
	}

	// metodo multiplica double
	public static double multiplicaNumeros() {
		int numero1 = 10;
		int numero2 = 21;
		double multiplica = numero1 * numero2;
		return multiplica;
	}

	// metodo divide double
	public static double divideNumeros() {
		int numero1 = 10;
		int numero2 = 21;
		double divide = (double) numero1 / (double) numero2;
		return divide;
	}

}
