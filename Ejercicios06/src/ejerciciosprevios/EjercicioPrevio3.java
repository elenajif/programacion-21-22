package ejerciciosprevios;

//definicion de la clase
//public class
public class EjercicioPrevio3 {
//definicion del metodo main
//public static void main
	public static void main(String[] args) {
		//llamo al metodo suma
		suma();
		//llamo al metodo multiplicacion
		multiplica();
	}
	
	public static void suma() {
		//suma dos numeros
		int numero1=3;
		int numero2=5;
		System.out.println("La suma es "+(numero1+numero2));
	}
	public static void multiplica() {
		//multiplica dos numeros
		int numero1=4;
		int numero2=12;
		System.out.println("La multiplicación es "+(numero1*numero2));
	}

}
