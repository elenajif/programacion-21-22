package ejerciciosprevios;

public class EjercicioPrevio12 {

	public static void main(String[] args) {
		int numero1 = 10;
		int numero2 = 21;
		// llamamos a los metodos
		sumaNumeros(numero1, numero2);
		restaNumeros(numero1, numero2);
		double numero3 = 2.33;
		double numero4 = 3.45;
		multiplicaNumeros(numero3, numero4);
		divideNumeros(numero3, numero4);
	}

	// metodo suma void
	// lo que est� entre par�ntesis en un metodo se llama parametro
	// datos de entrada y no son los mismos datos que las variables
	public static void sumaNumeros(int num1, int num2) {
		int suma = num1 + num2;
		System.out.println(suma);
	}

	// metodo resta void
	public static void restaNumeros(int num1, int num2) {
		int resta = num1 - num2;
		System.out.println(resta);
	}

	// metodo multiplica void
	public static void multiplicaNumeros(double num1, double num2) {
		double multiplica = num1 * num2;
		System.out.println(multiplica);
	}

	// metodo divide void
	public static void divideNumeros(double num1, double num2) {
		double divide = num1 / num2;
		System.out.println(divide);
	}

}
