package ejerciciosprevios;

import java.util.Scanner;

public class EjercicioPrevio18 {

	public static void main(String[] args) {
		// pido un cadena
		Scanner input = new Scanner(System.in);
		System.out.println("Dame la cadena");
		String miCadena = input.nextLine();
		// menu
		menu();
		int option = input.nextInt();
		// limpio el buffer
		input.nextLine();
		switch (option) {
		case 1:
			cambiarMayusculas(miCadena);
			break;
		case 2:
			cambiarMinusculas(miCadena);
			break;
		case 3:
			longitud(miCadena);
			break;
		case 4:
			primeraLetra(miCadena);
			break;
		default:
			System.out.println("Opcion no contemplada.");
			break;
		}
		input.close();
	}

	// metodo menu
	static void menu() {
		System.out.println("=================================");
		System.out.println("1 - Cambia la frase a mayusculas.");
		System.out.println("2 - Cambia la frase a minusculas.");
		System.out.println("3 - Largura de la frase.");
		System.out.println("4 - Primera letra de la frase.");
		System.out.println("5 - Salir.");
		System.out.println("=================================");
	}

	// metodo cambiarMayusculas(cadena)
	static void cambiarMayusculas(String cadena) {
		System.out.println("La cadena en mayusculas es " + cadena.toUpperCase());
	}

	// metodo cambiarMinusculas(cadena)
	static void cambiarMinusculas(String cadena) {
		System.out.println("La cadena en minusculas es " + cadena.toLowerCase());
	}

	// metodo longitud(cadena)
	static void longitud(String cadena) {
		System.out.println("La cadena tiene la longitud " + cadena.length());
	}

	// metodo primeraLetra(cadena)
	static void primeraLetra(String cadena) {
		System.out.println("La primera letra de la cadena es " + cadena.charAt(0));
	}
}
