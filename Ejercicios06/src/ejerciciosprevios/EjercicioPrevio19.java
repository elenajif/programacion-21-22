package ejerciciosprevios;

import java.util.Scanner;

public class EjercicioPrevio19 {

	// uso de un solo Scanner
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// llamadas a los metodos
		// suma
		int numero1 = leerEntero();
		int numero2 = leerEntero();
		System.out.println("Suma numeros " + sumaNumeros(numero1, numero2));
		// resta
		System.out.println("Resta numeros " + restaNumeros(numero1, numero2));
		// multiplica
		double numero3 = leerDouble();
		double numero4 = leerDouble();
		System.out.println("Multiplica numeros " + multiplicaNumeros(numero3, numero4));
		// divide
		System.out.println("Divide numeros " + divideNumeros(numero3, numero4));
		input.close();
	}

	// metodo para pedir los datos enteros
	public static int leerEntero() {
		System.out.println("Dame un entero");
		int numero = input.nextInt();
		return numero;
	}

	// metodo para pedir los datos doubles
	public static double leerDouble() {
		System.out.println("Dame un double");
		double numero = input.nextDouble();
		return numero;
	}

	// metodo que recibe dos enteros y devuelve un entero que es la suma
	public static int sumaNumeros(int num1, int num2) {
		int suma = num1 + num2;
		return suma;
	}

	// metodo que recibe dos enteros y devuelve un entero que es la resta
	public static int restaNumeros(int num1, int num2) {
		int resta = num1 - num2;
		return resta;
	}

	// metodo que recibe dos doubles y devuelve un double que es la multiplicacion
	public static double multiplicaNumeros(double num1, double num2) {
		double multiplica = num1 * num2;
		return multiplica;
	}

	// metodo que recibe dos doubles y devuelve un double que es la division
	public static double divideNumeros(double num1, double num2) {
		double divide = num1 / num2;
		return divide;
	}

}
