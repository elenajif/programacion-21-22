package ejerciciosprevios;

public class EjercicioPrevio7 {

	public static void main(String[] args) {
		// suma void -> NO DEVUELVE
		sumaNumeros();
		// resta double -> DEVUELVE
		double miResta = restaNumeros();
		System.out.println(miResta);
		// multiplica int -> DEVUELVE
		int miMultiplicacion=multiplicaNumeros();
		System.out.println(miMultiplicacion);
	}
	public static void sumaNumeros() {
		int numero1 = 3;
		int numero2 = 5;
		System.out.println("La suma es " + (numero1 + numero2));
	}
	public static double restaNumeros() {
		int numero1 = 25;
		int numero2 = 10;
		double resta = numero1 - numero2;
		return resta;
	}
	public static int multiplicaNumeros() {
		int numero1 = 12;
		int numero2 = 4;
		int multiplica = numero1 * numero2;
		return multiplica;
	}

}
