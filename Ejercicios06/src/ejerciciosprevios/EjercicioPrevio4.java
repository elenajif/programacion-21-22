package ejerciciosprevios;

public class EjercicioPrevio4 {

	//ejercicio que llama a metodo ya construidos static de Java
	//Clase.metodo
	public static void main(String[] args) {
		// Llamo al metodo out.println de la clase System
		//metodo static
		System.out.println("Hola");
		// LLamo al metodo sqrt de la clase Math
		//metodo static
		System.out.println(Math.sqrt(2.55));
		//LLamo al metodo round de la clase Math
		//metodo static
		System.out.println(Math.round(2.33));
	}

}
