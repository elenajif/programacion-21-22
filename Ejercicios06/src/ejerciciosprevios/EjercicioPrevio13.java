package ejerciciosprevios;

public class EjercicioPrevio13 {

	public static void main(String[] args) {
		double numero1 = 10;
		double numero2 = 21;
		// llamamos a los metodos
		sumaNumeros(numero1, numero2);
		restaNumeros(numero1, numero2);
		multiplicaNumeros(numero1, numero2);
		divideNumeros(numero1, numero2);
	}

	// metodo suma void
	// lo que est� entre par�ntesis en un metodo se llama parametro
	// datos de entrada y no son los mismos datos que las variables
	public static void sumaNumeros(double num1, double num2) {
		double suma = num1 + num2;
		System.out.println(suma);
	}

	// metodo resta void
	public static void restaNumeros(double num1, double num2) {
		double resta = num1 - num2;
		System.out.println(resta);
	}

	// metodo multiplica void
	public static void multiplicaNumeros(double num1, double num2) {
		double multiplica = num1 * num2;
		System.out.println(multiplica);
	}

	// metodo divide void
	public static void divideNumeros(double num1, double num2) {
		double divide = num1 / num2;
		System.out.println(divide);
	}

}
