package ejerciciosprevios;

import java.util.Scanner;

public class EjercicioPrevio5 {

	//llamamos a metodos no static
	//solo los usados hasta ahora
	public static void main(String[] args) {
		//creado un objeto de la clase Scanner
		Scanner input = new Scanner(System.in);
		System.out.println("Dame un numero");
		//llamamos al metodo nextInt() usando input (objeto de la clase Scanner)
		int numero=input.nextInt();
		System.out.println(numero);
		input.close();

	}

}
