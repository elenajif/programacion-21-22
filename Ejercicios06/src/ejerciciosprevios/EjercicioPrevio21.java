package ejerciciosprevios;

public class EjercicioPrevio21 {

	public static void main(String[] args) {
		// sobrecarga -> puede haber varios metodos que se llamen igual
		//deben ser diferentes en el numero de parametros 
		//o en el tipo de parametros
		//no puede ser diferente solo en el valor de devolucion
		suma(2,3);
		suma(2.3,4.5);
		suma(2,4.5);
		suma(4.3,6);
		suma(2,3,4);
		suma(2,3.1,4.6);
		suma(2.2,3.1,4.6);
	}
	
	//metodo suma que recibe dos enteros
	static void suma(int num1, int num2) {
		System.out.println("Estoy en suma con dos enteros");
		System.out.println(num1+num2);
	}
	//metodo suma que recibe dos doubles
	static void suma(double num1, double num2) {
		System.out.println("Estoy en suma con dos doubles");
		System.out.println(num1+num2);
	}
	//metodo suma que recibe un entero y un double
		static void suma(int num1, double num2) {
			System.out.println("Estoy en suma de un entero y un double");
			System.out.println(num1+num2);
		}
	//metodo suma que recibe tres enteros
	static void suma(int num1, int num2, int num3) {
		System.out.println("Estoy en suma con tres enteros");
		System.out.println(num1+num2+num3);
	}
	//metodo suma que recibe tres double
		static void suma(double num1, double num2, double num3) {
			System.out.println("Estoy en suma con tres doubles");
			System.out.println(num1+num2+num3);
		}

}
