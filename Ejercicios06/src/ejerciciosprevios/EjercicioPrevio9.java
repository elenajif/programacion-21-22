package ejerciciosprevios;

public class EjercicioPrevio9 {

	public static void main(String[] args) {
		// llamamos a los metodos
		sumaNumeros();
		restaNumeros();
		multiplicaNumeros();
		divideNumeros();
	}

	//quito public a los metodos porque solo uso una clase
	
	// metodo suma void
	static void sumaNumeros() {
		int numero1 = 4;
		int numero2 = 5;
		System.out.println("La suma es " + (numero1 + numero2));
	}

	// metodo resta void
	static void restaNumeros() {
		int numero1 = 4;
		int numero2 = 5;
		System.out.println("La resta es " + (numero1 - numero2));
	}

	// metodo multiplica void
	static void multiplicaNumeros() {
		int numero1 = 4;
		int numero2 = 5;
		System.out.println("La multiplicacion es " + (numero1 * numero2));
	}

	// metodo divide void
	static void divideNumeros() {
		int numero1 = 4;
		int numero2 = 5;
		double divide = ((double) numero1 /(double) numero2);
		System.out.println("La division es " + divide);
	}

}
