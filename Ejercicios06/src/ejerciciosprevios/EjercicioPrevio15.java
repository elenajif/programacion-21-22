package ejerciciosprevios;

import java.util.Scanner;

public class EjercicioPrevio15 {

	public static void main(String[] args) {
		// Pedir un numero por teclado
		Scanner input = new Scanner(System.in);
		System.out.println("Dame un numero");
		int numero = input.nextInt();
		// llamar al metodo void divisibleDos(numero)
		divisibleDos(numero);
		// llamar al metodo void divisibleTres(numero)
		divisibleTres(numero);
		// llamar al metodo void divisibleCuatro(numero)
		divisibleCuatro(numero);
		input.close();
	}

	// metodo divisibleDos(numero)
	static void divisibleDos(int numero) {
		if (numero % 2 == 0) {
			System.out.println("Es divisible por dos");
		} else {
			System.out.println("No es divisible por dos");
		}
	}

	// metodo divisibleTres(numero)
	static void divisibleTres(int numero) {
		if (numero % 3 == 0) {
			System.out.println("Es divisible por tres");
		} else {
			System.out.println("No es divisible por tres");
		}
	}

	// metodo divisibleCuatro(numero)
	static void divisibleCuatro(int numero) {
		if (numero % 4 == 0) {
			System.out.println("Es divisible por cuatro");
		} else {
			System.out.println("No es divisible por cuatro");
		}
	}
}
