package ejercicio14;

import ejercicio10.Ejercicio10;

public class Ejercicio14 {

	public static void main(String[] args) {
		// Integer.parseInt("-234")
		int numero = deCadenaAEntero("31458");
		int numero2 = deCadenaAEntero("2-4");
		int numero3 = deCadenaAEntero("-31458");
		System.out.println(numero);
		System.out.println(numero2);
		System.out.println(numero3);
	}

	private static int deCadenaAEntero(String cadena) {
		int resultado = 0;
		if (Ejercicio10.esEntero(cadena)) {
			int exponente = 0;
			// convierto el String a int
			for (int i = cadena.length() - 1; i > 0; i--) {
				resultado = resultado + ((cadena.charAt(i) - 48) * (int) Math.pow(10, exponente));
				exponente++;
			}
			if (cadena.charAt(0)=='-') {
				resultado=-resultado;
			} else {
				resultado = resultado + ((cadena.charAt(0) - 48) * (int) Math.pow(10, exponente));
			}
		}
		return resultado;
	}

}
