package ejercicio04;

public class Metodos04 {
	// si encuentro un divisor no es primo
	static boolean esPrimo(int numero) {
		for (int i = 2; i < numero; i++) {
			if (numero % i == 0) {
				return false;
			}
		}
		return true;
	}
}
