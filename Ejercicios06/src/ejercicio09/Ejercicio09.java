package ejercicio09;

import ejercicio08.Ejercicio08;

public class Ejercicio09 {

	public static void main(String[] args) {
		String cadena = palabraAleatoria(700);
		System.out.println(cadena);
	}

	static String palabraAleatoria(int longitud) {
		String cadena = "";

		//puedo usar el metodo aleatorio porque es public
		//esta en otra clase y otro paquete
		//si el metodo aleatorio no llevara el public daria un error
		//el metodo palabraAleatoria no necesita el public
		//porque lo  usamos en la misma clase y en el mismo paquete
		for (int i = 0; i < longitud; i++) {
			cadena = cadena + (char) Ejercicio08.aleatorio(65, 90);
		}
		return cadena;
	}

}
