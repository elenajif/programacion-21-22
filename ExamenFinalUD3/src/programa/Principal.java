package programa;

import java.sql.SQLException;

import clases.Ikea;

public class Principal {

	public static void main(String[] args) {

		Ikea ikeaZaragoza = new Ikea();
		System.out.println("Creamos trabajadores");
		ikeaZaragoza.altaTrabajador("1111", "Maria", "muebles cocina");
		ikeaZaragoza.altaTrabajador("2222", "Carlos", "muebles cocina");
		ikeaZaragoza.altaTrabajador("3333", "Raquel", "muebles oficina");
		ikeaZaragoza.altaTrabajador("4444", "Felipe", "muebles oficina");
		ikeaZaragoza.altaTrabajador("5555", "Lorena", "menaje cocina");
		ikeaZaragoza.altaTrabajador("6666", "David", "menaje cocina");
		ikeaZaragoza.altaTrabajador("7777", "Andres", "menaje exterior");
		ikeaZaragoza.altaTrabajador("8888", "Ines", "menaje exterior");
		ikeaZaragoza.altaTrabajador("9999", "Andrea", "menaje cocina");
		System.out.println("Mostramos trabajadores");
		ikeaZaragoza.listarTrabajadores();
		System.out.println();

		System.out.println("Creamos jefes seccion");
		ikeaZaragoza.altaJefeSeccion("1111", "Maria", "muebles", 180);
		ikeaZaragoza.altaJefeSeccion("9999", "Andrea", "menaje", 250);
		System.out.println("Mostramos jefes seccion");
		ikeaZaragoza.listarJefesSeccion();
		System.out.println();

		System.out.println("Creamos departamentos");
		ikeaZaragoza.altaDepartamento("Departamento1", "menaje", "1111");
		ikeaZaragoza.altaDepartamento("Departamento2", "muebles", "9999");
		System.out.println("Mostramos departamentos");
		ikeaZaragoza.listarDepartamentos();
		System.out.println();

		System.out.println("Registramos trabajadores en departamento");
		ikeaZaragoza.registrarTrabajadorDepartamento("1111", "Departamento2");
		ikeaZaragoza.registrarTrabajadorDepartamento("2222", "Departamento2");
		ikeaZaragoza.registrarTrabajadorDepartamento("3333", "Departamento2");
		ikeaZaragoza.registrarTrabajadorDepartamento("4444", "Departamento2");
		ikeaZaragoza.registrarTrabajadorDepartamento("5555", "Departamento1");
		ikeaZaragoza.registrarTrabajadorDepartamento("6666", "Departamento1");
		ikeaZaragoza.registrarTrabajadorDepartamento("7777", "Departamento1");
		ikeaZaragoza.registrarTrabajadorDepartamento("8888", "Departamento1");
		ikeaZaragoza.registrarTrabajadorDepartamento("9999", "Departamento1");
		System.out.println("Mostramos departamentos");
		ikeaZaragoza.listarDepartamentos();
		System.out.println();

		System.out.println("Guardamos en fichero datos departamentos");
		ikeaZaragoza.guardarDatosDepartamentos();
		System.out.println("Guardamos en fichero datos personal");
		ikeaZaragoza.guardarDatosPersonal();
		System.out.println();

		try {
			System.out.println("Conectamos BBDD");
			ikeaZaragoza.conectarBBDD();
			System.out.println("Insertamos datos trabajadores en BBDD");
			ikeaZaragoza.insertarTrabajadoresBBDD();
			System.out.println("Mostramos trabajadores de BBDD");
			ikeaZaragoza.mostrarTrabajadoresBBDD();
			System.out.println();
			System.out.println("Insertamos datos jefes seccion en BBDD");
			ikeaZaragoza.insertarJefesSeccionBBDD();
			System.out.println("Mostramos jefes seccion");
			ikeaZaragoza.listarJefesSeccion();
			System.out.println();
			System.out.println("Eliminamos trabajador de BBDD");
			ikeaZaragoza.eliminarTrabajador("5555");
			System.out.println("Mostramos trabajadores de BBDD");
			ikeaZaragoza.mostrarTrabajadoresBBDD();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
