package programa;

import java.util.Scanner;

import clases.Colegio;

public class Programa {
	static Scanner scan = new Scanner(System.in);

	public static void main(String[] args) {

		System.out.println("Creamos colegio");
		Colegio colegio1 = new Colegio();

		System.out.println("Rellenamos");
		colegio1.rellenar();
		System.out.println("Visualizamos");
		colegio1.visualizar();
		System.out.println("Modificamos");
		colegio1.modificar();
		System.out.println("Ordenamos");
		colegio1.ordenar();
		System.out.println("Visualizamos");
		colegio1.visualizar();

	}
}


