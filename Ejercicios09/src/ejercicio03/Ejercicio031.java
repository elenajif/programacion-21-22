package ejercicio03;

import java.util.ArrayList;

public class Ejercicio031 {

	public static void main(String[] args) {
		ArrayList<Integer> lista = new ArrayList<>();

		// creamos los elementos
		for (int i = 0; i < 100; i++) {
			lista.add((int) (Math.random() * 20) + 1);
		}

		// mostramos los elementos
		for (int numero : lista) {
			System.out.print(numero + " ");
		}

		// eliminamos con un bucle while
		int contador = 0;
		while (contador < lista.size()) {
			if (lista.get(contador) >= 10 && lista.get(contador) <= 15) {
				lista.remove(contador);
			} else {
				contador++;
			}
		}

	}

}
