package ejercicio03;

import java.util.ArrayList;

public class Ejercicio03 {

	public static void main(String[] args) {
		ArrayList<Integer> lista = new ArrayList<>();

		// creamos los elementos
		for (int i = 0; i < 100; i++) {
			lista.add((int) (Math.random() * 20) + 1);
		}

		// mostramos los elementos
		for (int numero : lista) {
			System.out.print(numero + " ");
		}

		// eliminamos con un for en sentido descendente
		for (int i = lista.size()-1; i >= 0; i--) {
			if (lista.get(i) >= 10 && lista.get(i) <= 15) {
				lista.remove(i);
			}
		}

	}

}
