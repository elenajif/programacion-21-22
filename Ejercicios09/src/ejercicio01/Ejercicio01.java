package ejercicio01;

import java.util.ArrayList;
import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		ArrayList<String> lista = new ArrayList<>();

		System.out.println("Indica la cantidad de cadenas");
		int cantidad = input.nextInt();
		input.nextLine();
		
		for (int i = 0; i < cantidad; i++) {
			System.out.println("Introduce la cadena");
			String cadena = input.nextLine();
			lista.add(cadena);
		}

		for (String cadena : lista) {
			System.out.println(cadena);
		}
		
		System.out.println("Que numero de posicion quieres quitar");
		int posicion = input.nextInt();
		lista.remove(posicion);
		
		for (String cadena : lista) {
			System.out.println(cadena);
		}
		

		input.close();

	}

}
