package ejercicio05;

import java.util.ArrayList;
import java.util.Iterator;

public class Ejercicio05 {

	public static void main(String[] args) {
		ArrayList<String> cadenas = new ArrayList<>();
		cadenas.add("Hola");
		cadenas.add("Adios");
		cadenas.add("Hola");
		cadenas.add("Hola");
		cadenas.add("Adios");
		
		for (String cadena: cadenas) {
			System.out.println(cadena);
		}		
		System.out.println("Borramos");
		borrarCadenas("Hola",cadenas);
		for (String cadena: cadenas) {
			System.out.println(cadena);
		}
	}
	
	public static void borrarCadenas(String cadenaABorrar,ArrayList<String> cadenas) {
		Iterator<String> iterador = cadenas.iterator();
		
		while (iterador.hasNext()) {
			String cadena = iterador.next();
			if (cadena.equals(cadenaABorrar)) {
				iterador.remove();
			}
		}
	}

}
