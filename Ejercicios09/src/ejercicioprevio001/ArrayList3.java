package ejercicioprevio001;

import java.util.ArrayList;
import java.util.Iterator;

public class ArrayList3 {

	public static void main(String[] args) {
		// declaramos ArrayList de String
		ArrayList<String> lista = new ArrayList<String>();
		System.out.println("A�adimos elementos");
		lista.add("Elemento1");
		lista.add("Elemento2");
		lista.add("Elemento3");
		lista.add("Elemento4");
		lista.add("Elemento5");
		lista.add("Elemento1");

		System.out.println("Mostramos con un for");
		for (int i = 0; i < lista.size(); i++) {
			System.out.println(lista.get(i));
		}

		System.out.println("");
		System.out.println("Mostramos con iterator");
		// obtengo el iterador
		// importamos desde java.util
		Iterator<String> iterador = lista.iterator();
		while (iterador.hasNext()) {
			// iterador.next() permite acceder a cada elemento
			String elemento = iterador.next();
			System.out.println(elemento);
		}

		System.out.println("");
		System.out.println("Mostramos con foreach");
		for (String elemento : lista) {
			System.out.println(elemento);
		}

		System.out.println("");
		System.out.println("A�adimos un elemento en la posicion 1");
		lista.add(1, "Elemento insertado");
		for (String elemento : lista) {
			System.out.println(elemento);
		}

		System.out.println("");
		System.out.println("Devolvemos el numero de elementos del ArrayList");
		System.out.println(lista.size());

		System.out.println("");
		System.out.println("Devolvemos el elemento en la posicion 2 del ArrayList");
		System.out.println(lista.get(2));

		System.out.println("");
		System.out.println("Comprobamos si existe el elemnento 'Elemento1'");
		System.out.println(lista.contains("Elemento1"));

		System.out.println("");
		System.out.println("Devolvemos la posici�n de la primera ocurrencia de 'Elemento5'");
		System.out.println(lista.indexOf("Elemento5"));

		System.out.println("");
		System.out.println("Devolvemos ultima ocurrencia de 'Elemento1'");
		System.out.println(lista.lastIndexOf("Elemento1"));

		System.out.println("");
		System.out.println("Borramos el elemento de la posicion 5");
		lista.remove(5);
		for (String elemento : lista) {
			System.out.println(elemento);
		}

		System.out.println("");
		System.out.println("Borramos la primera ocurrencia de 'Elemento2'");
		lista.remove("Elemento2");
		for (String elemento : lista) {
			System.out.println(elemento);
		}

		System.out.println("");
		System.out.println("Copiamos el ArrayList");
		// vamos a tener que castearlo a ArrayList<String>
		// lista.clone estoy copiando un ArrayList
		ArrayList<String> arrayListCopia = (ArrayList<String>) lista.clone();
		for (String elemento : arrayListCopia) {
			System.out.println(elemento);
		}

		System.out.println("");
		System.out.println("Pasamos el ArrayList a un array");
		String[] array = new String[lista.size()];
		array = lista.toArray(array);
		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i]);
		}
		
		System.out.println("");
		System.out.println("Borra todos los elementos del ArrayList");
		lista.clear();
		for (String elemento : lista) {
			System.out.println(elemento);
		}
		
		System.out.println("");
		System.out.println("Comprobamos si el ArrayList esta vacio");
		System.out.println(lista.isEmpty());

	}

}
