package ejercicio02;

import java.util.ArrayList;
import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		ArrayList<String> lista = new ArrayList<>();

		String cadena;
		do {
			System.out.println("Introduce una cadena");
			cadena = input.nextLine();
			lista.add(cadena);
		} while (!cadena.equalsIgnoreCase("fin"));

		// llamo a listar que recibe el ArrayList
		// metodo static
		listar(lista);

		System.out.println("Dame la cadena a borrar");
		cadena = input.nextLine();
		borrar(cadena, lista);

		listar(lista);

		input.close();
	}

	public static void borrar(String cadena, ArrayList<String> lista) {
		lista.remove(cadena);
	}

	public static void listar(ArrayList<String> lista) {
		for (String s : lista) {
			System.out.println(s);
		}
	}

}
