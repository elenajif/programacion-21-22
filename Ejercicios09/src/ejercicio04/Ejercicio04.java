package ejercicio04;

import java.util.ArrayList;
import java.util.Iterator;

public class Ejercicio04 {

	public static void main(String[] args) {
		ArrayList<Integer> lista = new ArrayList<>();

		// creamos los elementos
		for (int i = 0; i < 100; i++) {
			lista.add((int) (Math.random() * 20) + 1);
		}

		// mostramos los elementos
		for (int numero : lista) {
			System.out.print(numero + " ");
		}
		System.out.println(" ");

		// eliminamos con la clase iterator
		Iterator<Integer> it = lista.iterator();
		while (it.hasNext()) {
			Integer numero = it.next();
			if (numero >= 10 && numero <= 15) {
				it.remove();
			}
		}
		
		//volver a listar con clase iterator
		Iterator<Integer> iterador=lista.iterator();
		while (iterador.hasNext()) {
			System.out.print(iterador.next()+" ");
		}
	}
}
