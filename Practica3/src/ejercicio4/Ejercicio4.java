package ejercicio4;

import java.util.Scanner;

public class Ejercicio4 {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		int[] habitantes = new int[10];

		leerHabitantes(habitantes);
		mediaHabitantes(habitantes);
		maximoHabitantes(habitantes);
		minimoHabitantes(habitantes);
		input.close();

	}

	public static void leerHabitantes(int[] habitantes) {
		for (int i = 0; i < 10; i++) {
			System.out.println("Introduce un n�mero de habitantes de un pueblo (" + (i + 1) + " de 10)");
			habitantes[i] = input.nextInt();
		}
	}

	public static void mediaHabitantes(int[] habitantes) {
		int suma = 0;
		for (int i = 0; i < habitantes.length; i++) {
			suma += habitantes[i];
		}
		double media = suma / 10;
		System.out.println("La suma de habitantes es: " + suma);
		System.out.println("La media de habitantes es: " + media);
	}

	public static void maximoHabitantes(int[] habitantes) {
		int maximo = habitantes[0];
		for (int i = 1; i < habitantes.length; i++) {
			if (habitantes[i] > maximo) {
				maximo = habitantes[i];
			}
		}
		System.out.println("El m�ximo de habitantes es: " + maximo);
	}

	public static void minimoHabitantes(int[] habitantes) {
		int minimo = habitantes[0];
		for (int i = 1; i < habitantes.length; i++) {
			if (habitantes[i] < minimo) {
				minimo = habitantes[i];
			}
		}
		System.out.println("El m�nimo de habitantes es: " + minimo);
	}
}
