package ejercicio3;

import java.util.Scanner;

public class Ejercicio3 {

	public static void main(String[] args) {
		String[] arrayPueblos = {"Belchite","Botorrita","Cadrete","Gelsa","Pedrola","Quinto","Zuera"};
		
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un pueblo");
		String pueblo = input.nextLine();
		
		if (encontrado(pueblo, arrayPueblos)) {
			System.out.println("El pueblo est� dentro del array");
		} else {
			System.out.println("El pueblo no existe dentro del array");
		}
		input.close();
	}
	
	public static boolean encontrado(String pueblo, String[] array) {
		for (int i = 0; i < array.length; i++) {
			if ( array[i].equals(pueblo) ) {
				return true;
			}
		}		
		return false;
	}

}
