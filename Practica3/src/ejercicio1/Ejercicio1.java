package ejercicio1;

import java.util.Scanner;

public class Ejercicio1 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		System.out.println("Dime un pueblo y te indicar� su comarca");
		String pueblo = input.nextLine();

		String comarca = mostrarComarca(pueblo);

		if (!comarca.equals("error")) {
			System.out.println("El pueblo " + pueblo + " pertenece a la comarca de " + comarca);
		} else {
			System.out.println("El pueblo introducido no es v�lido o no lo tenemos registrado");
		}
		input.close();
	}

	public static String mostrarComarca(String pueblo) {
		switch (pueblo) {
		case "Biota":
		case "Tauste":
			return "Ejea de los Caballeros";
		case "Fabara":
			return "Caspe";
		case "Anento":
		case "Gallocanta":
			return "Daroca";
		case "Calatorao":
		case "Codos":
			return "La Almunia de Do�a Godina";
		case "Gallur":
		case "Tarazona":
			return "Comarca Borja";
		case "Cadrete":
		case "Pedrola":
		case "Quinto":
		case "Zuera":
			return "Zaragoza";
		case "Ateca":
		case "Calatayud":
		case "Jaraba":
			return "Calatayud";
		default:
			return "error";
		}
	}
}
