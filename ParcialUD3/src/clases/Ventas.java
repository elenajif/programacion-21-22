package clases;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;

public class Ventas implements Serializable {

	private static final long serialVersionUID = 1L;

	private int codigoVenta;
	private LocalDate fechaVenta;
	private Clientes cliente;
	protected ArrayList<Productos> listaProductos;
	
	public Ventas(int codigoVenta, LocalDate fechaVenta, Clientes cliente) {
		this.codigoVenta = codigoVenta;
		this.fechaVenta = fechaVenta;
		this.cliente = cliente;
	}

	public Ventas(int codigoVenta, LocalDate fechaVenta, Clientes cliente, ArrayList<Productos> listaProductos) {
		this.codigoVenta = codigoVenta;
		this.fechaVenta = fechaVenta;
		this.cliente = cliente;
		this.listaProductos = new ArrayList<Productos>();
	}

	public int getCodigoVenta() {
		return codigoVenta;
	}

	public void setCodigoVenta(int codigoVenta) {
		this.codigoVenta = codigoVenta;
	}

	public LocalDate getFechaVenta() {
		return fechaVenta;
	}

	public void setFechaVenta(LocalDate fechaVenta) {
		this.fechaVenta = fechaVenta;
	}

	public Clientes getCliente() {
		return cliente;
	}

	public void setCliente(Clientes cliente) {
		this.cliente = cliente;
	}

	public ArrayList<Productos> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(ArrayList<Productos> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public double calcularPrecioTotal() {
		double precioTotal = 0;
		for (Productos p : listaProductos) {
			precioTotal += p.calcularPrecio();
		}
		return precioTotal;
	}
	
	//comprueba que no hay productos con ese nombre
	public boolean comprobarProducto(String nombreProducto) {
		for (Productos p:listaProductos) {
			if (p.getNombreProducto().equals(nombreProducto)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		return "Ventas [codigoVenta=" + codigoVenta + ", fechaVenta=" + fechaVenta + ", cliente=" + cliente
				+ ", listaProductos=" + listaProductos + "]";
	}

}
