package clases;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class Supermercado implements Serializable {

	private static final long serialVersionUID = 1L;

	private ArrayList<Productos> listaProductos;
	private ArrayList<Clientes> listaClientes;
	private ArrayList<Ventas> listaVentas;

	public Supermercado() {
		listaProductos = new ArrayList<Productos>();
		listaClientes = new ArrayList<Clientes>();
		listaVentas = new ArrayList<Ventas>();
	}

	// dar de alta cliente
	public void altaCliente(String nombre) {
		listaClientes.add(new Clientes(listaClientes.size() + 1, nombre, LocalDate.now()));
	}

	// dar de alta producto oferta
	public void altaProducto(String nombreProducto, double precio, String tipoOferta) {
		listaProductos.add(new ProductosOferta(nombreProducto, precio, tipoOferta));
		Collections.sort(listaProductos);
	}

	// dar de alta producto especial
	public void altaProducto(String nombreProducto, double precio, boolean entregaFragil) {
		listaProductos.add(new ProductosEspeciales(nombreProducto, precio, entregaFragil));
		Collections.sort(listaProductos);
	}

	// crear venta para un cliente
	public void crearVentaCliente(int codigoCliente) {
		if (clienteExiste(codigoCliente)) {
			listaVentas.add(new Ventas(listaVentas.size() + 1, LocalDate.now(), devuelveCliente(codigoCliente)));
		} else {
			System.out.println("El cliente no existe");
		}
	}

	// introducir productos en una venta
	public void introducirProductoVenta(int codigoVenta, String nombreProducto) {
		if (ventaExiste(codigoVenta)) {
			if (productoExiste(nombreProducto) && !devuelveVenta(codigoVenta).comprobarProducto(nombreProducto)) {
				devuelveVenta(codigoVenta).listaProductos.add(devuelveProducto(nombreProducto));
			} else {
				System.out.println("El producto no existe");
			}
		} else {
			System.out.println("La venta no existe");
		}
	}

	// mostrar ventas de un cliente concreto
	public void mostrarVentasCliente(int codigoCliente) {
		for (Ventas venta : listaVentas) {
			if (venta.getCliente().equals(devuelveCliente(codigoCliente))) {
				System.out.println(venta);
			}
		}
	}

	// listar ventas
	public void listarVentas() {
		for (Ventas venta : listaVentas) {
			System.out.println(venta);
		}
		System.out.println("Productos");
		for (Productos productos : listaProductos) {
			System.out.println(productos);
		}
	}

	// comprobar cliente existe
	public boolean clienteExiste(int codigoCliente) {
		for (Clientes cliente : listaClientes) {
			if (cliente.getCodigoCliente() == codigoCliente) {
				return true;
			}
		}
		return false;
	}

	// devuelve cliente
	public Clientes devuelveCliente(int codigoCliente) {
		for (Clientes cliente : listaClientes) {
			if (cliente.getCodigoCliente() == codigoCliente) {
				return cliente;
			}
		}
		return null;
	}

	// comprobar venta existe
	public boolean ventaExiste(int codigoVenta) {
		for (Ventas venta : listaVentas) {
			if (venta.getCodigoVenta() == codigoVenta) {
				return true;
			}
		}
		return false;
	}

	// devuelve venta
	public Ventas devuelveVenta(int codigoVenta) {
		for (Ventas venta : listaVentas) {
			if (venta.getCodigoVenta() == codigoVenta) {
				return venta;
			}
		}
		return null;
	}

	// comprobar producto existe
	public boolean productoExiste(String nombreProducto) {
		for (Productos producto : listaProductos) {
			if (producto.getNombreProducto().equals(nombreProducto)) {
				return true;
			}
		}
		return false;
	}

	// devuelve producto
	public Productos devuelveProducto(String nombreProducto) {
		for (Productos producto : listaProductos) {
			if (producto.getNombreProducto().equals(nombreProducto)) {
				return producto;
			}
		}
		return null;
	}

	// guardar datos
	public void guardarDatos() {
		try {
			ObjectOutputStream escritor = new ObjectOutputStream(new FileOutputStream(new File("src/datos.dat")));
			escritor.writeObject(listaVentas);
			escritor.writeObject(listaProductos);
			escritor.writeObject(listaClientes);
			escritor.close();
		} catch (IOException e) {
			System.out.println("Error de entrada salida de datos");
		}
	}

	// cargar datos
	@SuppressWarnings("unchecked")
	public void cargarDatos() {
		try {
			ObjectInputStream escritor = new ObjectInputStream(new FileInputStream(new File("src/datos.dat")));
			listaVentas=(ArrayList<Ventas>) escritor.readObject();
			listaProductos=(ArrayList<Productos>) escritor.readObject();
			listaClientes=(ArrayList<Clientes>) escritor.readObject();
			escritor.close();
		} catch (IOException e) {
			System.out.println("Error de entrada salida de datos");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}


}
