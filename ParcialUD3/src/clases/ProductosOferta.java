package clases;

import java.io.Serializable;

public class ProductosOferta extends Productos implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final double OFERTA_SEMANA = 0.10;
	public static final double OFERTA_DIA = 0.15;

	private String tipoOferta;

	public ProductosOferta(String nombreProducto, double precio, String tipoOferta) {
		super(nombreProducto, precio);
		this.tipoOferta = tipoOferta;
	}

	public String getTipoOferta() {
		return tipoOferta;
	}

	public void setTipoOferta(String tipoOferta) {
		this.tipoOferta = tipoOferta;
	}

	@Override
	public int compareTo(Productos o) {
		return getNombreProducto().compareTo(o.getNombreProducto());
	}

	@Override
	public double calcularPrecio() {
		double precioTotal = 0;
		if (this.getTipoOferta().equals("semanal")) {
			precioTotal = this.getPrecio() - this.getPrecio() * OFERTA_SEMANA;
		} else if (this.getTipoOferta().equals("dia")) {
			precioTotal = this.getPrecio() - this.getPrecio() * OFERTA_DIA;
		}
		return precioTotal;
	}

	@Override
	public String toString() {
		return "ProductosOferta [tipoOferta=" + tipoOferta + ", toString()=" + super.toString() + "]";
	}

}
