package clases;

import java.io.Serializable;

public class ProductosEspeciales extends Productos implements Serializable{

	private static final long serialVersionUID = 1L;
	public static final double SUPLEMENTO=10;
	
	private boolean entregaFragil;

	public ProductosEspeciales(String nombreProducto, double precio, boolean entregaFragil) {
		super(nombreProducto, precio);
		this.entregaFragil=entregaFragil;
	}

	public boolean isEntregaFragil() {
		return entregaFragil;
	}

	public void setEntregaFragil(boolean entregaFragil) {
		this.entregaFragil = entregaFragil;
	}

	@Override
	public int compareTo(Productos o) {
		return getNombreProducto().compareTo(o.getNombreProducto());
	}

	@Override
	public double calcularPrecio() {
		double precioTotal = 0;
		if (this.isEntregaFragil()) {
			precioTotal = this.getPrecio() +SUPLEMENTO;
		} 
		return precioTotal;
	}

	@Override
	public String toString() {
		return "ProductosEspeciales [entregaFragil=" + entregaFragil + ", toString()=" + super.toString() + "]";
	}


}
