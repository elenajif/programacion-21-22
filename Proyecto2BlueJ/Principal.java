public class Principal
{
    public static void main(String[] args)
    {
        //uso metodo crearInteger de clase ClaseInteger
        //clase nombreObjeto = new constructor
        ClaseInteger objeto = new ClaseInteger();
        objeto.crearInteger();
        
        //metodo static
        //para llamarlo
        //Clase.metodo
        
        //metodo no static
        //creo objeto (usando constructor)
        //para llamarlo
        //objeto.metodo
        
        //Scanner input = new Scanner(System.in)
        //Clase  objeto = new constructor
        //era correcto
        //Scanner entrada = new Scanner(System.in)
        
    }
}
