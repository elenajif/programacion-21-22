
public class ClaseInteger
{
    public void crearInteger()
    {
        System.out.println("Creo un numero int y lo muestro");
        int x=3;
        System.out.println("El numero es "+x);
        System.out.println("Creo un numero String y lo muestro");
        String cadena="Hola";
        System.out.println("La cadena es "+cadena);
        System.out.println("Creo un Integer y lo muestro");
        //clase objeto = new constructor
        Integer miInteger=new Integer(3);
        //uso el constructor de la clase Integer
        //Integer objeto = new Integer(numero)
        System.out.println("El integer es "+miInteger.intValue());
    }
}
