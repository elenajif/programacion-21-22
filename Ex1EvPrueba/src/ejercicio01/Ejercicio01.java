package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		double resultado = 0;
		double numero;
		for (int i = 0; i < 10; i++) {
			System.out.println("Introduce un decimal");
			numero = input.nextDouble();
			resultado = resultado + numero;
		}
		
		System.out.println("La suma es "+resultado);

		input.close();

	}

}
