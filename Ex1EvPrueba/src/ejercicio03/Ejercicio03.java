package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {

	static final double DESCUENTO_COMPRA = 0.15;

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int opcion = 0;
		do {
			System.out.println("Menu");
			System.out.println("1.- calcular importe");
			System.out.println("2.- contraseņa correcta");
			System.out.println("3.- salir");
			System.out.println("Elegir opcion");
			opcion = input.nextInt();
			input.nextLine();

			switch (opcion) {
			case 1:
				double compra;
				double descuento;
				double total1;
				double total2;
				String dia;
				System.out.println("Introduce el total de la compra");
				compra = input.nextDouble();
				input.nextLine();
				System.out.println("Introduce el dia de la semana");
				dia = input.nextLine();

				descuento = DESCUENTO_COMPRA * compra;
				total1 = compra - descuento;
				total2 = compra;
				if (dia.equals("martes") || dia.equals("jueves")) {
					System.out.println("El descuento es " + descuento);
					System.out.println("El total es " + total1);
				} else {
					System.out.println("El total es " + total2);
				}
				break;
			case 2:
				System.out.println("Dame una contraseņa");
				String password=input.nextLine().toLowerCase();
				boolean vocales=false;
				boolean letras=false;
				for (int i=0;i<password.length();i++) {
					if (password.charAt(i)=='a' || password.charAt(i)=='e'
							||password.charAt(i)=='i'||password.charAt(i)=='o'
							||password.charAt(i)=='u') {
						vocales=true;
					}
					if (password.charAt(i)>='a' && password.charAt(i)<='z') {
						letras=true;
					}
					if (password.charAt(i)>='0' && password.charAt(i)<='9') {
						letras=false;
						break;
					}
				}
				if (vocales==true && letras==true) {
					System.out.println("la contraseņa es correcta");
				} else {
					System.out.println("la contraseņa es incorrecta");
				}
				break;
			case 3:
				System.out.println("salir");
				break;
			default:
				System.out.println("opcion incorrecta");
				break;
			}
		} while (opcion != 3);
		input.close();
	}

}
