package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Dime el numero de veces que se van a introducir numeros");
		int veces = input.nextInt();

		double suma = 0;
		for (int i = 0; i < veces; i++) {
			System.out.println("Dame un numero");
			int numero = input.nextInt();
			if (numero % 2 == 0) {
				System.out.println("El numero es par");
			} else {
				System.out.println("El numero es impar");
			}
			suma = suma + numero;
		}
		double media = suma / veces;
		System.out.println("La media es " + media);

		input.close();

	}

}
