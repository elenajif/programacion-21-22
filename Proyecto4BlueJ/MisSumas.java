public class MisSumas
{
    //atributos
    int numInt1;
    int numInt2;
    double numDouble1;
    double numDouble2;
    
    //constructor
    //es un metodo
    //se llama como la clase
    //no tiene valor de devolucion
    //un constructor me permite crear objetos
    //sino hay, por defecto usaremos nombreClase()
    //puedo crearlo yo y darle los valores que yo quiera
    public MisSumas() {
        numInt1=2;
        numInt2=3;
        numDouble1=2.3;
        numDouble2=4.5;
    }
    
    //metodos
    //metodos sobrecargados
    //se llaman igual pero difieren en el numero de parametros o tipo de datos
    public int suma(int x,int y) {
        return x+y;
    }
    public double suma(double x, double y) {
        return x+y;
    }
}
