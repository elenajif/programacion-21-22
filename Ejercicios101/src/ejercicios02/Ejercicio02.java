package ejercicios02;

public class Ejercicio02 {

	public static void main(String[] args) {
		SuperClase padre = new SuperClase("Pepe");
		SubClase hijo1 = new SubClase("Maria");
		SubClase hijo2 = new SubClase("Pepe",5);
		
		System.out.println(padre);
		System.out.println(hijo1);
		System.out.println(hijo2);
		
		padre.muestraHora();


	}

}
