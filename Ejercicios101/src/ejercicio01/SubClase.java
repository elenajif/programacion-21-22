package ejercicio01;

public class SubClase extends SuperClase{
	private int cantidad;
	
	public SubClase(int cantidad) {
		super();
		this.cantidad=cantidad;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	@Override
	public String toString() {
		return "SubClase [cantidad=" + cantidad + ", toString()=" + super.toString() + "]";
	}

	

}
