package principal;

public class PrincipalStringBuilder {

	public static void main(String[] args) {
		// StringBuilder es din�mica
		// a�adir, insertar, transformar desde un String, invertir...
		// pesa m�s
		StringBuilder texto = new StringBuilder("una prueba");
		System.out.println("Texto: "+texto);
		texto.append(" mas");
		System.out.println("A�adimos: "+texto);
		texto.insert(2, "xxx");
		System.out.println("Insertamos: "+texto);
		texto.reverse();
		System.out.println("Invertimos: "+texto);
		System.out.println("En may�sculas: "+texto.toString().toUpperCase());

	}

}
