package comparableserializable;

import java.io.Serializable;

public class Persona implements Comparable<Persona>, Serializable {

	private static final long serialVersionUID = 1L;
	private String nombre;
	private int edad;
	private int altura;

	// Constructor
	public Persona(String nombre, int altura, int edad) {
		this.nombre = nombre;
		this.altura = altura;
		this.edad = edad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public int getAltura() {
		return altura;
	}

	public void setAltura(int altura) {
		this.altura = altura;
	}

	public String toString() {
		return "Nombre:" + nombre + "\n Altura: " + altura + "\n edad:  " + edad;
	}

	// Comparar por edad y altura
	// Se le pasa la variable cadena para ejecutar la comparacion por edad o
	// altura
	public int compareTo(Persona o, String cadena) {
		int resultado = 0;
		if (cadena.equals("Edad")) {
			if (this.edad < o.edad) {
				resultado = 1;
			} // Cambio el valor de resultado para no crear otro metodo de
				// ordenacion
			else if (this.edad > o.edad) {
				resultado = -1;
			} else {
				resultado = 0;
			}
		}
		if (cadena.equals("Altura")) {
			if (this.altura < o.altura) {
				resultado = -1;
			} else if (this.altura > o.altura) {
				resultado = 1;
			} else {
				resultado = 0;
			}
		}
		return resultado;
	}

	@Override
	public int compareTo(Persona arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
}
