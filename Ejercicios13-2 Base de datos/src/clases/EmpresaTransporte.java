package clases;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;


public class EmpresaTransporte {

	private ArrayList<Transporte> listaTransportes;
	private ArrayList<Personal> listaPersonal;
	private Connection conexion;
	
	public EmpresaTransporte() {
		this.listaTransportes = new ArrayList<Transporte>();
		this.listaPersonal = new ArrayList<Personal>();
	}

	public ArrayList<Transporte> getListaTransportes() {
		return listaTransportes;
	}

	public void setListaTransportes(ArrayList<Transporte> listaTransportes) {
		this.listaTransportes = listaTransportes;
	}

	public ArrayList<Personal> getListaPersonal() {
		return listaPersonal;
	}

	public void setListaPersonal(ArrayList<Personal> listaPersonal) {
		this.listaPersonal = listaPersonal;
	}
	
	//ALTA CONDUCTOR
	public void altaConductor(String dni, String nombre, String carnet) {
		listaPersonal.add(new Conductor(dni, nombre, carnet));
		Collections.sort(listaPersonal);
	}
	//ALTA CARRETILLERO
	public void altaCarretillero(String dni, String nombre, float costeHora) {
		listaPersonal.add(new Carretillero(dni, nombre, costeHora));
		Collections.sort(listaPersonal);
	}
	
	//ALTA TRANSPORTE
	public void altaTransporte(String codigo, String nombre, String fechaTransporte, String dniConductor) {
		if (compruebaTransporte(codigo) == false && compruebaConductor(dniConductor)) {
			listaTransportes.add(new Transporte(codigo, nombre, fechaTransporte, devuelveConductor(dniConductor)));
			Collections.sort(listaTransportes);
		}else {
			System.out.println("Ha ocurrido un error");
		}
	}
	
	//REGISTAR CARRETILLEROS EN TRANSPORTE
	public void registarCarretilleroTransporte(String codigoTransporte, String dniCarretillero) {
		if (compruebaTransporte(codigoTransporte) && compruebaCarretillero(dniCarretillero)) {
			devuelveTransporte(codigoTransporte).getListaCarretilleros().add(devuelveCarretillero(dniCarretillero));
		}else {
			System.out.println("Ha ocurrido un error");
		}
	}
	
	//LISTAR CONDUCTORES
	public void listarConductores() {
		for(Personal personal : listaPersonal) {
			if (personal instanceof Conductor) {
				System.out.println(personal);
			}
		}
	}
	
	//LISTAR CARRETILLEROS
	public void listarCarretilleros() {
		for(Personal personal : listaPersonal) {
			if (personal instanceof Carretillero) {
				System.out.println(personal);
			}
		}
	}
	
	//GUARDAR DATOS
	public void guardarDatos() {
		try {
			ObjectOutputStream escritor = new ObjectOutputStream(new FileOutputStream(new File("src/datos.dat")));
			escritor.writeObject(listaTransportes);
			escritor.writeObject(listaPersonal);
			escritor.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//CARGAR DATOS
	@SuppressWarnings("unchecked")
	public void cargarDatos() {
		try {
			ObjectInputStream cargar = new ObjectInputStream(new FileInputStream(new File("src/datos.dat")));
			listaTransportes = (ArrayList<Transporte>) cargar.readObject();
			listaPersonal = (ArrayList<Personal>) cargar.readObject();
			cargar.close();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	
	//CONECTAR BASE DE DATOS
	public void conectarBBDD() {
		String servidor = "jdbc:mysql://localhost:3306/transporte3ev";
		try {
			conexion = DriverManager.getConnection(servidor, "root", "");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	//GUARDAR CONDUCTORES
	public void guardarConductoresBBDD(String carnet) {
		String query = "INSERT INTO conductores(dni, nombre,carnet) VALUES(?, ?, ?)";
		try {
			PreparedStatement sentencia = conexion.prepareStatement(query);
			
			for (Personal personal : listaPersonal) {
				if (personal instanceof Conductor && ((Conductor) personal).getCarnet().equals(carnet)) {
					sentencia.setString(1, personal.getDni());
					sentencia.setString(2, personal.getNombre());
					sentencia.setString(3, ((Conductor) personal).getCarnet());
					sentencia.executeUpdate();
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	//CARGAR CONDUCTORES
	public void cargarConductoresBBDD() throws SQLException {
		String query = "SELECT * FROM conductores";
		PreparedStatement sentencia = conexion.prepareStatement(query);
		ResultSet resultado = sentencia.executeQuery();
		while (resultado.next()) {
				listaPersonal.add(new Conductor(resultado.getString(1), 
						resultado.getString(2), 
						resultado.getString(3)
						));
		}
	}
	
	//MIOS
	//COMPROBAR TRANSPORTE 
	public boolean compruebaTransporte(String codigo) {
		boolean existe = false;
		for(Transporte transporte : listaTransportes) {
			if(transporte.getCodigo().equals(codigo)) {
				existe = true;
			}
		}
		return existe;
	}
	
	//COMPROBAR CONDUCTOR 
	public boolean compruebaConductor(String dni) {
		boolean existe = false;
		for(Personal personal : listaPersonal) {
			if (personal instanceof Conductor) {
				if(personal.getDni().equals(dni)) {
					existe = true;
				}
			}
		}
		return existe;
	}
	
	//COMPROBAR CARRETILLERO 
	public boolean compruebaCarretillero(String dni) {
		boolean existe = false;
		for(Personal personal : listaPersonal) {
			if (personal instanceof Carretillero) {
				if(personal.getDni().equals(dni)) {
					existe = true;
				}
			}
		}
		return existe;
	}
	
	//DEVUELVE CONDUCTOR
	public Conductor devuelveConductor(String dni) {
		for(Personal personal : listaPersonal) {
			if (personal instanceof Conductor) {
				if (personal.getDni().equals(dni)) {
					return  (Conductor) personal;
				}	
			}
		}
		return null;
	}
	
	//DEVUELVE CARRETILLERO
	public Carretillero devuelveCarretillero(String dni) {
		for(Personal personal : listaPersonal) {
			if (personal instanceof Carretillero) {
				if (personal.getDni().equals(dni)) {
					return (Carretillero) personal;
				}
			}
		}
		return null;
	}
	
	//DEVUELVE TRANSPORTES
	public Transporte devuelveTransporte(String codigo) {
		for(Transporte transporte : listaTransportes) {
			if (transporte.getCodigo().equals(codigo)) {
				return transporte;
			}
		}
		return null;
	}
}
