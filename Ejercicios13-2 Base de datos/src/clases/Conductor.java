package clases;

public class Conductor extends Personal {


	private static final long serialVersionUID = -3287096738226583558L;
	private String carnet;
	
	public Conductor(String dni, String nombre, String carnet) {
		super(dni, nombre);
		this.carnet = carnet;
	}

	public String getCarnet() {
		return carnet;
	}

	public void setCarnet(String carnet) {
		this.carnet = carnet;
	}

	@Override
	public int compareTo(Personal arg0) {
		return getDni().compareTo(arg0.getDni());
	}

	@Override
	public String toString() {
		return super.toString() + carnet;
	}

	
	
}
